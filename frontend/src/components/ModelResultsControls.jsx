import React from "react";
import Select from "react-select";
import { Row, InputGroup, FormControl, Button } from "react-bootstrap";
import { CustomSelectStyles } from "../data/sortingFilteringOptions";
import pageStyles from "../styles/Pages.module.css";

class ModelResultsControls extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchParam: "",
    };
  }

  render() {
    return (
      <React.Fragment>
        <Row className="ml-1 mr-1">
          <div style={{ width: "30%" }} className="pl-1">
            <h1 className={pageStyles.mediumText}>Sort By</h1>
            <Select
              styles={CustomSelectStyles}
              options={this.props.sortingOptions}
              defaultValue={{ label: "Default" }}
              onChange={(selectedOption) =>
                this.props.setSortingOption(selectedOption.value)
              }
            />
          </div>
          {this.props.showSearchBar === true ? (
            <div style={{ width: "60%" }} className="ml-auto pr-1">
              <h1 className={pageStyles.mediumText}>Search</h1>
              <InputGroup className="ml-auto">
                <FormControl
                  type="text"
                  placeholder="Search..."
                  className="ml-auto"
                  onKeyPress={(event) => {
                    if (event.key === "Enter") {
                      event.preventDefault();
                      this.props.setSearchParam(this.state.searchParam);
                    }
                  }}
                  onChange={(event) => {
                    this.setState({ searchParam: event.target.value });
                  }}
                />
                <InputGroup.Append>
                  <Button
                    variant="outline-success"
                    onClick={() =>
                      this.props.setSearchParam(this.state.searchParam)
                    }
                  >
                    Go
                  </Button>
                </InputGroup.Append>
              </InputGroup>
            </div>
          ) : null}
        </Row>
        <br />
        <Row className="ml-1 mr-1">
          {this.props.filteringOptions.map((optionSet) => (
            <div
              style={{ width: "20%" }}
              className="pl-1 pr-1"
              key={optionSet.name + "_div"}
            >
              <h1 className={pageStyles.mediumText}>{optionSet.name}</h1>
              <Select
                styles={CustomSelectStyles}
                options={optionSet.options}
                isMulti={true}
                onChange={(selectedOptions) =>
                  this.props.updateFilteringOptions(
                    optionSet.param,
                    selectedOptions.map((option) => option.value)
                  )
                }
                key={optionSet.name}
                id={optionSet.param}
              />
            </div>
          ))}
        </Row>
      </React.Fragment>
    );
  }
}

export default ModelResultsControls;
