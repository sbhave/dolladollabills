# DollaDollaBills

Phase 1 Git SHA: 4188cfda32a680684d5c2e831450bc85f37fc8e9  
Phase 2 Git SHA: 7acefa1107caa59d602087361dc68a2e6a1bc5d1  
Phase 3 Git SHA: e499c194a1be0c47bd2113c3c192a44b6da3ef5a  
Phase 4 Git SHA: 916ea5397f827d3ddf7196c0446bd9e3f7c1da20

## The Team

| Name             | EID     | GitLabID     |
| ---------------- | ------- | ------------ |
| Siddhi Bhave     | SMB5733 | @sbhave      |
| Saumyaa Krishnan | SK46826 | @saumyaa2000 |
| Reagan Lasswell  | RCL2253 | @reagancl    |
| Ian Thorne       | IET222  | @ian-thorne  |
| William Wang     | WW7964  | @wwang00     |

**Team Leader:**
William Wang

## Website

[DollaDollaBills](https://www.dolladollabills.me)

## Our Presentation

[Phase 4 Presentation](https://www.youtube.com/watch?v=Gd1p9PqEArM)  
You can also find our presentation at the bottom of our About page!

## Phase One Estimated Completion Time

| Name             | Estimate | Actual   |
| ---------------- | -------- | -------- |
| Siddhi Bhave     | 10 hours | 12 hours |
| Saumyaa Krishnan | 8 hours  | 12 hours |
| Reagan Lasswell  | 12 hours | 12 hours |
| Ian Thorne       | 8 hours  | 14 hours |
| William Wang     | 8 hours  | 12 hours |

## Phase Two Estimated Completion Time

| Name             | Estimate | Actual   |
| ---------------- | -------- | -------- |
| Siddhi Bhave     | 20 hours | 30 hours |
| Saumyaa Krishnan | 18 hours | 30 hours |
| Reagan Lasswell  | 20 hours | 25 hours |
| Ian Thorne       | 20 hours | 35 hours |
| William Wang     | 20 hours | 25 hours |

## Phase Three Estimated Completion Time

| Name             | Estimate | Actual   |
| ---------------- | -------- | -------- |
| Siddhi Bhave     | 10 hours | 12 hours |
| Saumyaa Krishnan | 12 hours | 15 hours |
| Reagan Lasswell  | 15 hours | 16 hours |
| Ian Thorne       | 15 hours | 22 hours |
| William Wang     | 8 hours  | 16 hours |

## Phase Four Estimated Completion Time

| Name             | Estimate | Actual   |
| ---------------- | -------- | -------- |
| Siddhi Bhave     | 8 hours  | 8 hours  |
| Saumyaa Krishnan | 8 hours  | 8 hours  |
| Reagan Lasswell  | 8 hours  | 8 hours  |
| Ian Thorne       | 12 hours | 12 hours |
| William Wang     | 8 hours  | 10 hours |

## RESTful API Documentation

[Documenation](https://documenter.getpostman.com/view/7971575/TzRNFqMq)

## GitLab Pipelines

[Pipelines](https://gitlab.com/sbhave/dolladollabills/-/pipelines)

## Notes

Some of our tests are failing because a bug in a library we are using. So we removed from the pipeline. If you run the tests locally they work. It's just the acceptance tests of querying the API that don't work, if you run it locally it will work. Something just goes wrong when you run them on the pipeline.

Our GUI tests with Splinter are having some inconsistent results. We've found that they pass fairly consistently when only one test is run at a time, but they fail much more frequently when many tests are run at a time. It seems like there's some sort of timing issue that prevents Splinter from seeing the state of the page after the actions it performs. When performing those same tests on our website manually, we always see the expected results, so it must be something we're missing with regards to Splinter. (This issue only happens on pages that are dynamic, the splash and about page tests pass consistently.)
