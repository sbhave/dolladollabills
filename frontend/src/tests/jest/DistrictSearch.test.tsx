import ReactDOM from "react-dom";
import DistrictSearch from "../../pages/Districts/DistrictSearch";
import { act } from "react-dom/test-utils";
import { unmountComponentAtNode } from "react-dom";
import "@testing-library/jest-dom/extend-expect";
import { BrowserRouter as Router } from "react-router-dom";

it("Test Rendering for district page", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <Router>
      <DistrictSearch></DistrictSearch>
    </Router>,
    div
  );
});
