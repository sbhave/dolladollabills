import argparse
from splinter import Browser
from selenium.webdriver import ChromeOptions
from unittest import main, TestCase

parser = argparse.ArgumentParser()
parser.add_argument("path")
args = parser.parse_args()
browser_executable = args.path

POLITICIANS_URL = "https://www.dolladollabills.me/politicians"
BILLS_URL = "https://www.dolladollabills.me/bills"
DISTRICTS_URL = "https://www.dolladollabills.me/districts"
STATES_URL = "https://www.dolladollabills.me/states"
DEFAULT_WAIT = 20

class TestSorting(TestCase):

    # Construct a browser to use in the tests
  @classmethod
  def setUpClass(self):
    command_line_options = ChromeOptions()
    command_line_options.add_argument("--headless")
    command_line_options.add_argument("--no-sandbox")
    command_line_options.add_argument("--disable-dev-shm-usage")
    self.browser = Browser("chrome", executable_path=browser_executable, options=command_line_options)

  # Class up the browser after running the tests
  @classmethod
  def tearDownClass(self):
    self.browser.quit()

  def test_politicians_sort_age_dec(self):
    self.browser.visit(POLITICIANS_URL)
    self.assertTrue(self.browser.is_text_present("of the 535", wait_time=DEFAULT_WAIT))
    self.browser.find_by_text("Default").click()
    self.assertTrue(self.browser.is_text_present("Age (decreasing)", wait_time=DEFAULT_WAIT))
    self.browser.find_by_text("Age (decreasing)").click()
    self.assertTrue(self.browser.is_text_present("Dianne", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Feinstein", wait_time=DEFAULT_WAIT))

  def test_bills_sort_year_inc(self):
    self.browser.visit(BILLS_URL)
    self.assertTrue(self.browser.is_text_present("of the 1042", wait_time=DEFAULT_WAIT))
    self.browser.find_by_text("Default").click()
    self.assertTrue(self.browser.is_text_present("Year (increasing)", wait_time=DEFAULT_WAIT))
    self.browser.find_by_text("Year (increasing)").click()
    self.assertTrue(self.browser.is_text_present("H.R.1", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Democrat", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("California", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("House", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("House Bill", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("2009", wait_time=DEFAULT_WAIT))

  def test_districts_sort_population_inc(self):
    self.browser.visit(DISTRICTS_URL)
    self.assertTrue(self.browser.is_text_present("of the 435", wait_time=DEFAULT_WAIT))
    self.browser.find_by_text("Default").click()
    self.assertTrue(self.browser.is_text_present("Population (increasing)", wait_time=DEFAULT_WAIT))
    self.browser.find_by_text("Population (increasing)").click()
    self.assertTrue(self.browser.is_text_present("Rhode Island", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Congressional District 2", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Democrat", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("529,295", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("40.8 years", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("$35,623", wait_time=DEFAULT_WAIT))

  def test_states_sort_name_za(self):
    self.browser.visit(STATES_URL)
    self.assertTrue(self.browser.is_text_present("of the 50", wait_time=DEFAULT_WAIT))
    self.browser.find_by_text("Default").click()
    self.assertTrue(self.browser.is_text_present("Name (z-a)", wait_time=DEFAULT_WAIT))
    self.browser.find_by_text("Name (z-a)").click()
    self.assertTrue(self.browser.is_text_present("Wyoming", wait_time=DEFAULT_WAIT))


if __name__ == "__main__":
  main(argv=['first-arg-is-ignored'])