from models import db, Politician, Bill, District, State

from sqlalchemy import or_


def query_bill(queries):
    q = (
        db.session.query(Bill)
        .join(State, State.id == Bill.state_id)
        .join(Politician, Politician.id == Bill.politician_id)
    )

    # search

    if "search" in queries:
        token = queries["search"][0]
        token_conds = []
        token_conds.append(Bill.name.ilike(f"%{token}%"))
        token_conds.append(Bill.name.ilike(f"%{token}%"))
        token_conds.append(Bill.number.ilike(f"%{token}%"))
        token_conds.append(Bill.subject.ilike(f"%{token}%"))
        token_conds.append(Bill.summary.ilike(f"%{token}%"))
        token_conds.append(Bill.sponsor.ilike(f"%{token}%"))
        token_conds.append(Bill.bill_type_str.ilike(f"%{token}%"))
        token_conds.append(Bill.chamber.ilike(f"%{token}%"))
        token_conds.append(Bill.party_str.ilike(f"%{token}%"))
        token_conds.append(Bill.state_name.ilike(f"%{token}%"))
        q = q.filter(or_(*token_conds))

    # filter

    if "party" in queries:
        party_conds = []
        for party in queries["party"]:
            party_conds.append(Bill.party == party)
        q = q.filter(or_(*party_conds))

    if "chamber" in queries:
        chamber_conds = []
        for chamber in queries["chamber"]:
            is_senate_bool = chamber == "senate"
            chamber_conds.append(Bill.is_senate == is_senate_bool)
        q = q.filter(or_(*chamber_conds))

    if "bill_type" in queries:
        bill_type_conds = []
        for bill_type in queries["bill_type"]:
            bill_type_conds.append(Bill.bill_type == bill_type)
        q = q.filter(or_(*bill_type_conds))

    if "year" in queries:
        year_conds = []
        for year in queries["year"]:
            year_conds.append(Bill.year == int(year))
        q = q.filter(or_(*year_conds))

    if "subject" in queries:
        subject_conds = []
        for subject in queries["subject"]:
            subject_conds.append(Bill.subject == subject)
        q = q.filter(or_(*subject_conds))

    if "state" in queries:
        state_conds = []
        for state in queries["state"]:
            state_conds.append(State.name == state)
        q = q.filter(or_(*state_conds))

    # sort

    if "sort" not in queries:
        q = q.order_by(Bill.year.desc())
    elif queries["sort"][0] == "year-inc":
        q = q.order_by(Bill.year)
    elif queries["sort"][0] == "year-dec":
        q = q.order_by(Bill.year.desc())
    elif queries["sort"][0] == "state-inc":
        q = q.order_by(State.name)
    elif queries["sort"][0] == "state-dec":
        q = q.order_by(State.name.desc())

    return q.all()
