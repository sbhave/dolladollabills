import ReactDOM from "react-dom";
import BillSearch from "../../pages/Bills/BillSearch";
import { act } from "react-dom/test-utils";
import { unmountComponentAtNode } from "react-dom";
import "@testing-library/jest-dom/extend-expect";
import { BrowserRouter as Router } from "react-router-dom";

it("Test Rendering for bill search page", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <Router>
      <BillSearch></BillSearch>
    </Router>,
    div
  );
});
