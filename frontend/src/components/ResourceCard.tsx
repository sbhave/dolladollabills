import React from "react";
import { Card } from "react-bootstrap";
import cardStyles from "../styles/Cards.module.css";

class ResourceCard extends React.Component<
  IResourceCardProps,
  IResourceCardState
> {
  constructor(props: any) {
    super(props);
    this.state = {
      hovering: false,
    };
  }

  GetCardClasses = () => {
    if (
      this.props.link != null &&
      this.props.link.length > 0 &&
      this.state.hovering === true
    ) {
      return cardStyles.smallCard + " " + cardStyles.cardShadow;
    } else {
      return cardStyles.smallCard;
    }
  };

  ClickableLink = () => {
    if (this.props.link != null && this.props.link.length > 0) {
      return (
        <a href={this.props.link}>
          <span className={cardStyles.linkableCard} />
        </a>
      );
    } else {
      return null;
    }
  };

  BodyText = () => {
    if (this.props.bodyText != null && this.props.bodyText.length > 0) {
      return (
        <div>
          <hr className={"mt-2 " + cardStyles.lineSeparator} />
          <Card.Text className={"mt-2 " + cardStyles.cardText}>
            {this.props.bodyText}
          </Card.Text>
        </div>
      );
    } else {
      return null;
    }
  };

  render() {
    return (
      <Card
        className={"bg-white rounded " + this.GetCardClasses()}
        onMouseEnter={() => this.setState({ hovering: true })}
        onMouseLeave={() => this.setState({ hovering: false })}
        id={this.props.id}
      >
        <this.ClickableLink />
        <div className="mt-5 ml-5 mr-5 mb-4">
          <Card.Img variant="top" src={this.props.imgUrl} />
        </div>
        <Card.Body className={"text-center d-flex flex-column"}>
          <Card.Title className={"mt-0 mb-0 " + cardStyles.cardTitle}>
            {this.props.name}
          </Card.Title>
          <this.BodyText />
        </Card.Body>
      </Card>
    );
  }
}

export default ResourceCard;

interface IResourceCardState {
  hovering: Boolean;
}

interface IResourceCardProps {
  imgUrl: string;
  name: string;
  bodyText?: string; // nullable
  link?: string; //nullable
  id?: string;
}
