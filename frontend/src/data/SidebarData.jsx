import React, {useState} from 'react'
import * as FaIcons from "react-icons/fa"
import * as AiIcons from "react-icons/ai"
import * as IoIcons from "react-icons/io"

export const SidebarData = [
  {
    title: 'Home',
    path: '/',
    icon: <AiIcons.AiFillHome />,
    cName: 'nav-tex'
  },
  {
    title: 'Bills',
    path: '/bills',
    icon: <IoIcons.IoIosPaper />,
    cName: 'nav-tex'
  },
  {
    title: 'Politicians',
    path: '/politicians',
    icon: <FaIcons.FaCartPlus />,
    cName: 'nav-tex'
  },
  {
    title: 'Districts',
    path: '/districts',
    icon: <IoIcons.IoMdPeople />,
    cName: 'nav-tex'
  },
  {
    title: 'States',
    path: '/StateCard',
    icon: <IoIcons.IoMdPeople />,
    cName: 'nav-tex'
  },
  {
    title: 'About',
    path: '/About',
    icon: <IoIcons.IoMdPeople />,
    cName: 'nav-tex'
  }
]
