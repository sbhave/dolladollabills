import argparse
from splinter import Browser
from selenium.webdriver import ChromeOptions
from unittest import main, TestCase

parser = argparse.ArgumentParser()
parser.add_argument("path")
args = parser.parse_args()
browser_executable = args.path

POLITICIANS_URL = "https://www.dolladollabills.me/politicians"
BILLS_URL = "https://www.dolladollabills.me/bills"
DISTRICTS_URL = "https://www.dolladollabills.me/districts"
STATES_URL = "https://www.dolladollabills.me/states"
DEFAULT_WAIT = 20

class TestPagination(TestCase):

  # Construct a browser to use in the tests
  @classmethod
  def setUpClass(self):
    command_line_options = ChromeOptions()
    command_line_options.add_argument("--headless")
    command_line_options.add_argument("--no-sandbox")
    command_line_options.add_argument("--disable-dev-shm-usage")
    self.browser = Browser("chrome", executable_path=browser_executable, options=command_line_options)

  # Clean up the browser after running the tests
  @classmethod
  def tearDownClass(self):
    self.browser.quit()

  def test_politicians_next_prev(self):
    self.browser.visit(POLITICIANS_URL)
    self.assertTrue(self.browser.is_text_present("1 through 10", wait_time=DEFAULT_WAIT))
    self.browser.find_by_id("pagination_next").click()
    self.browser.find_by_id("pagination_next").click()
    self.assertTrue(self.browser.is_text_present("21 through 30", wait_time=DEFAULT_WAIT))
    self.browser.find_by_id("pagination_prev").click()
    self.assertTrue(self.browser.is_text_present("11 through 20", wait_time=DEFAULT_WAIT))

  def test_politicians_page_size(self):
    self.browser.visit(POLITICIANS_URL)
    self.assertTrue(self.browser.is_text_present("1 through 10" , wait_time=DEFAULT_WAIT))
    self.browser.find_by_id("pagination_next").click()
    self.assertTrue(self.browser.is_text_present("11 through 20", wait_time=DEFAULT_WAIT))
    self.browser.find_by_id("pagination_dropup").click()
    self.assertTrue(self.browser.is_text_present("11 through 20", wait_time=DEFAULT_WAIT))
    self.browser.find_by_id("pagination_30").click()
    self.assertTrue(self.browser.is_text_present("1 through 30", wait_time=DEFAULT_WAIT))

  def test_bills_next_prev(self):
    self.browser.visit(BILLS_URL)
    self.assertTrue(self.browser.is_text_present("1 through 20", wait_time=DEFAULT_WAIT))
    self.browser.find_by_id("pagination_next").click()
    self.browser.find_by_id("pagination_next").click()
    self.browser.find_by_id("pagination_next").click()
    self.assertTrue(self.browser.is_text_present("61 through 80", wait_time=DEFAULT_WAIT))
    self.browser.find_by_id("pagination_prev").click()
    self.assertTrue(self.browser.is_text_present("41 through 60", wait_time=DEFAULT_WAIT))

  def test_bills_page_size(self):
    self.browser.visit(BILLS_URL)
    self.assertTrue(self.browser.is_text_present("1 through 20", wait_time=DEFAULT_WAIT))
    self.browser.find_by_id("pagination_dropup").click()
    self.assertTrue(self.browser.is_text_present("1 through 20", wait_time=DEFAULT_WAIT))
    self.browser.find_by_id("pagination_30").click()
    self.assertTrue(self.browser.is_text_present("1 through 30", wait_time=DEFAULT_WAIT))

  def test_districts_next_prev(self):
    self.browser.visit(DISTRICTS_URL)
    self.assertTrue(self.browser.is_text_present("1 through 20", wait_time=DEFAULT_WAIT))
    self.browser.find_by_id("pagination_next").click()
    self.browser.find_by_id("pagination_next").click()
    self.browser.find_by_id("pagination_next").click()
    self.browser.find_by_id("pagination_next").click()
    self.assertTrue(self.browser.is_text_present("81 through 100", wait_time=DEFAULT_WAIT))
    self.browser.find_by_id("pagination_prev").click()
    self.browser.find_by_id("pagination_prev").click()
    self.browser.find_by_id("pagination_prev").click()
    self.assertTrue(self.browser.is_text_present("21 through 40", wait_time=DEFAULT_WAIT))

  def test_districts_page_size(self):
    self.browser.visit(DISTRICTS_URL)
    self.assertTrue(self.browser.is_text_present("1 through 20", wait_time=DEFAULT_WAIT))
    self.browser.find_by_id("pagination_dropup").click()
    self.assertTrue(self.browser.is_text_present("1 through 20", wait_time=DEFAULT_WAIT))
    self.browser.find_by_id("pagination_10").click()
    self.assertTrue(self.browser.is_text_present("1 through 10", wait_time=DEFAULT_WAIT))

  def test_states_next_prev(self):
    self.browser.visit(STATES_URL)
    self.assertTrue(self.browser.is_text_present("1 through 10", wait_time=DEFAULT_WAIT))
    self.browser.find_by_id("pagination_next").click()
    self.assertTrue(self.browser.is_text_present("11 through 20", wait_time=DEFAULT_WAIT))
    self.browser.find_by_id("pagination_prev").click()
    self.assertTrue(self.browser.is_text_present("1 through 10", wait_time=DEFAULT_WAIT))

  def test_states_page_size(self):
    self.browser.visit(STATES_URL)
    self.assertTrue(self.browser.is_text_present("1 through 10", wait_time=DEFAULT_WAIT))
    self.browser.find_by_id("pagination_dropup").click()
    self.assertTrue(self.browser.is_text_present("50", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("1 through 10", wait_time=DEFAULT_WAIT))
    self.browser.find_by_id("pagination_20").click()
    self.assertTrue(self.browser.is_text_present("1 through 20", wait_time=DEFAULT_WAIT))


if __name__ == "__main__":
  main(argv=['first-arg-is-ignored'])