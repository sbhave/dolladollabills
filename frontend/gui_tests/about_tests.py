import argparse
from splinter import Browser
from selenium.webdriver import ChromeOptions
from unittest import main, TestCase

parser = argparse.ArgumentParser()
parser.add_argument("path")
args = parser.parse_args()
browser_executable = args.path

ABOUT_URL = "https://www.dolladollabills.me/about"
LINKEDIN_URL = "https://www.linkedin.com/in/"
SIDDHI_LI = "siddhi-bhave"
SAUMYAA_LI = "saumyaa-krishnan-95109016b"
REAGAN_LI = "reagan-lasswell-5a2264161"
IAN_LI = "ian-thorne526"
WILLIAM_LI = "wwang00"

class TestAbout(TestCase):

  # Construct a browser to use in the tests
  @classmethod
  def setUpClass(self):
    command_line_options = ChromeOptions()
    command_line_options.add_argument("--headless")
    command_line_options.add_argument("--no-sandbox")
    command_line_options.add_argument("--disable-dev-shm-usage")
    self.browser = Browser("chrome", executable_path=browser_executable, options=command_line_options)

  # Clean up the browser after running the tests
  @classmethod
  def tearDownClass(self):
    self.browser.quit()

  def test_headers(self):
    self.browser.visit(ABOUT_URL)
    self.assertTrue(self.browser.is_text_present("About DollaDollaBills"))
    self.assertTrue(self.browser.is_text_present("Our Team"))
    self.assertTrue(self.browser.is_text_present("Cumulative Stats"))
    self.assertTrue(self.browser.is_text_present("Our Resources"))
    self.assertTrue(self.browser.is_text_present("Tools"))
    self.assertTrue(self.browser.is_text_present("APIs"))
    self.assertTrue(self.browser.is_text_present("Our Gitlab Repo and Postman API"))

  def test_siddhi_linkedin(self):
    self.browser.visit(ABOUT_URL)
    siddhi_li = self.browser.find_by_id("Siddhi Bhave LI")
    siddhi_li.click()
    self.assertTrue("siddhi-bhave" in self.browser.url)
    # self.assertTrue((self.browser.url == LINKEDIN_URL + SIDDHI_LI) or (self.browser.url == LINKEDIN_URL + SIDDHI_LI +"/"))

  def test_saumyaa_linkedin(self):
    self.browser.visit(ABOUT_URL)
    saumyaa_li = self.browser.find_by_id("Saumyaa Krishnan LI")
    saumyaa_li.click()
    self.assertTrue("saumyaa-krishnan" in self.browser.url)
    # self.assertTrue((self.browser.url == LINKEDIN_URL + SAUMYAA_LI) or (self.browser.url == LINKEDIN_URL + SAUMYAA_LI +"/"))

  def test_reagan_linkedin(self):
    self.browser.visit(ABOUT_URL)
    reagan_li = self.browser.find_by_id("Reagan Lasswell LI")
    reagan_li.click()
    self.assertTrue("reagan-lasswell" in self.browser.url)
    # self.assertTrue((self.browser.url == LINKEDIN_URL + REAGAN_LI) or (self.browser.url == LINKEDIN_URL + REAGAN_LI +"/"))

  def test_ian_linkedin(self):
    self.browser.visit(ABOUT_URL)
    ian_li = self.browser.find_by_id("Ian Thorne LI")
    ian_li.click()
    self.assertTrue("ian-thorne" in self.browser.url)
    # self.assertTrue((self.browser.url == LINKEDIN_URL + IAN_LI) or (self.browser.url == LINKEDIN_URL + IAN_LI +"/"))

  def test_william_linkedin(self):
    self.browser.visit(ABOUT_URL)
    will_li = self.browser.find_by_id("William Wang LI")
    will_li.click()
    # self.assertTrue((self.browser.url == LINKEDIN_URL + WILLIAM_LI) or (self.browser.url == LINKEDIN_URL + WILLIAM_LI +"/"))
    self.assertTrue("wwang00" in self.browser.url)

  def test_gitlab_card(self):
    self.browser.visit(ABOUT_URL)
    gitlab_card = self.browser.find_by_id("gitlab")
    gitlab_card.click()
    self.assertEqual(self.browser.url, "https://gitlab.com/sbhave/dolladollabills")

  def test_postman_card(self):
    self.browser.visit(ABOUT_URL)
    postman_card = self.browser.find_by_id("postman")
    postman_card.click()
    self.assertEqual(self.browser.url, "https://documenter.getpostman.com/view/7971575/TzRNFqMq")

if __name__ == "__main__":
  main(argv=['first-arg-is-ignored'])