import React, { useState, useEffect } from "react";
import axios from "axios";
import Select from "react-select";
import {
  Container,
  Jumbotron,
  Row,
  FormControl,
  InputGroup,
  Button,
} from "react-bootstrap";
import DistrictResult from "../../components/DistrictResult";
import {
  DistrictFilteringOptions,
  CustomSelectStyles,
} from "../../data/sortingFilteringOptions";
import pageStyles from "../../styles/Pages.module.css";

let emptyFilterParams = {};
DistrictFilteringOptions.forEach((optionSet) => {
  emptyFilterParams[optionSet.param] = [];
});

const Districts = () => {
  const [filterParams, setFilterParams] = useState(emptyFilterParams);
  const [sortParams, setSortParams] = useState("");
  const [districtData, setDistrictData] = useState([]);

  const params = new URLSearchParams(window.location.search);
  let queryParam = "";
  if (params.has("q")) {
    queryParam = params.get("q");
  }
  const query = React.useRef();
  function searchOnClick() {
    window.location.assign("/districtSearch?q=" + query.current.value);
  }

  const options = [
    { value: "age-inc", label: "Age (increasing)" },
    { value: "age-dec", label: "Age (decreasing)" },
    { value: "income-inc", label: "Income (increasing)" },
    { value: "income-dec", label: "Income (decreasing)" },
    { value: "pop-inc", label: "Population (increasing)" },
    { value: "pop-dec", label: "Population decreasing)" },
    { value: "", label: "Default" },
  ];

  useEffect(() => {
    async function getDistrictData() {
      const address = "https://www.dolladollabills.me/api/districts?";
      const pageNumberParamString = "page=1";
      const perPageParamString = "&per-page=100";
      let searchString = "";
      let filterParamsString = "";
      const sortParamsString = "&sort=" + sortParams;
      for (const paramName in filterParams) {
        for (const paramValue of filterParams[paramName]) {
          filterParamsString += "&" + paramName + "=" + paramValue;
        }
      }
      if (queryParam) {
        searchString = "&search=" + queryParam;
      }

      axios
        .get(
          address +
            pageNumberParamString +
            perPageParamString +
            searchString +
            filterParamsString +
            sortParamsString
        )
        .then((response) => {
          setDistrictData(response.data["data"]);
        });
    }
    getDistrictData();
  }, [filterParams, sortParams, queryParam]);

  return (
    <div className={pageStyles.leftJustifyText}>
      <Jumbotron fluid={true} className={pageStyles.jumbotronBanner}>
        <h1 className={pageStyles.title}>District Search</h1>
      </Jumbotron>
      <Container className={pageStyles.core}>
        <br />
        <Row className="ml-1 mr-1">
          <div style={{ width: "30%" }}>
            <h1 className={pageStyles.mediumText}>Sort By</h1>
            <Select
              styles={CustomSelectStyles}
              options={options}
              defaultValue={{ label: "Default" }}
              onChange={(selectedOptions) => {
                setSortParams(selectedOptions.value);
              }}
            ></Select>
          </div>
          <div style={{ width: "30%" }} className="ml-auto">
            <h1 className={pageStyles.mediumText}>Search</h1>
            <InputGroup className="ml-auto">
              <FormControl
                type="text"
                placeholder="Search..."
                className="ml-auto"
                ref={query}
                onKeyPress={(event) => {
                  if (event.key === "Enter") {
                    event.preventDefault();
                    searchOnClick();
                  }
                }}
              />
              <InputGroup.Append>
                <Button
                  variant="outline-success"
                  onClick={() => searchOnClick()}
                >
                  Go
                </Button>
              </InputGroup.Append>
            </InputGroup>
          </div>
        </Row>
        <br />
        <Row className="ml-1 mr-1">
          {DistrictFilteringOptions.map((optionSet) => (
            <div style={{ width: "20%" }}>
              <h1 className={pageStyles.mediumText}>{optionSet.name}</h1>
              <Select
                styles={CustomSelectStyles}
                options={optionSet.options}
                isMulti={true}
                onChange={(selectedOptions) => {
                  let newFilterParams = { ...filterParams };
                  newFilterParams[optionSet.param] = selectedOptions.map(
                    (option) => option.value
                  );
                  setFilterParams(newFilterParams);
                }}
                key={optionSet.name}
                id={optionSet.name}
              />
            </div>
          ))}
        </Row>
        <br />
        <Row className="ml-1 mr-1">
          <Container>
            {districtData?.map((data) => {
              return (
                <DistrictResult key={data.id} data={data} query={queryParam} />
              );
            })}
          </Container>
        </Row>
        <br />
      </Container>
    </div>
  );
};

export default Districts;
