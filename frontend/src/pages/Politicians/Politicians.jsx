import "../../App.css";
import React, { useState, useEffect } from "react";
import { Container, CardDeck, Jumbotron } from "react-bootstrap";
import { Tabs, Tab } from "react-bootstrap-tabs";
import PoliticianCard from "../../components/PoliticianCard";
import PoliticianResult from "../../components/PoliticianResult";
import ModelResultsControls from "../../components/ModelResultsControls";
import PaginationControls from "../../components/PaginationControls";
import LoadingSpinner from "../../components/LoadingSpinner";
import {
  PoliticianSortingOptions,
  PoliticianFilteringOptions,
} from "../../data/sortingFilteringOptions";
import { makeAPIRequest } from "../../utilities/APIRequests";
import "bootstrap/dist/css/bootstrap.min.css";
import pageStyles from "../../styles/Pages.module.css";
import cardStyles from "../../styles/Cards.module.css";
import "../../styles/Tabs.css";

function Politicians() {
  const [pageNumber, setPageNumber] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [currNumPoliticians, setCurrNumPoliticians] = useState(0);
  const [searchParam, setSearchParam] = useState("");
  const [sortOption, setSortOption] = useState("");
  const [filterParams, setFilterParams] = useState(
    Object.fromEntries(
      PoliticianFilteringOptions.map((optionSet) => [optionSet.param, ""])
    )
  );
  const [politicianData, setPoliticianData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [onSearchTab, setOnSearchTab] = useState(false);

  useEffect(() => {
    async function getPoliticianData() {
      const requestParams = {
        page: pageNumber,
        "per-page": perPage,
        search: searchParam,
        sort: sortOption,
        ...filterParams,
      };
      setLoading(true);
      const { count, data } = await makeAPIRequest(
        "politicians",
        requestParams
      );
      setPoliticianData(data);
      setCurrNumPoliticians(count);
      setLoading(false);
    }
    getPoliticianData();
  }, [pageNumber, perPage, searchParam, filterParams, sortOption]);

  let numPages =
    currNumPoliticians % perPage === 0
      ? currNumPoliticians / perPage
      : Math.floor(currNumPoliticians / perPage) + 1;
  let pageEnd = (pageNumber - 1) * perPage + perPage;

  return (
    <div className={pageStyles.leftJustifyText}>
      <Jumbotron fluid={true} className={pageStyles.jumbotronBanner}>
        <h1 className={pageStyles.title}>Politician Information</h1>
      </Jumbotron>
      <Container className={pageStyles.core}>
        <br />
        <Tabs
          onSelect={(index, label) => {
            if (label === "View All") {
              setSearchParam("");
              setOnSearchTab(false);
            } else if (label === "Search") {
              setOnSearchTab(true);
            }
          }}
        >
          <Tab label="View All" id="view_all" />
          <Tab label="Search" id="search" />
        </Tabs>
        <br />
        <p className={pageStyles.pageText}>
          On this page, you can see cards for each politician{" "}
          {(pageNumber - 1) * perPage + 1} through{" "}
          {Math.min(pageEnd, currNumPoliticians)} of the {currNumPoliticians}{" "}
          house representatives and senators we have data on that match your
          current filters. Click on a card to learn more about that politician!
        </p>
        <ModelResultsControls
          sortingOptions={PoliticianSortingOptions}
          setSortingOption={setSortOption}
          filteringOptions={PoliticianFilteringOptions}
          updateFilteringOptions={(param, selectedFilterOptions) => {
            let newFilterParams = { ...filterParams };
            newFilterParams[param] = selectedFilterOptions;
            setFilterParams(newFilterParams);
          }}
          showSearchBar={onSearchTab}
          setSearchParam={setSearchParam}
        />
        <br />
        {loading ? (
          <LoadingSpinner />
        ) : onSearchTab ? (
          <React.Fragment>
            <Container>
              {politicianData?.map((data) => (
                <PoliticianResult
                  key={data.id}
                  data={data}
                  query={searchParam}
                />
              ))}
            </Container>
            <PaginationControls
              pageNumber={pageNumber}
              numPages={numPages}
              perPage={perPage}
              onSetPage={setPageNumber}
              onSetPerPage={setPerPage}
            />
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Container>
              <CardDeck className={cardStyles.cardDeck}>
                {politicianData?.map((data) => {
                  return <PoliticianCard key={data.id} data={data} />;
                })}
              </CardDeck>
            </Container>
            <PaginationControls
              pageNumber={pageNumber}
              numPages={numPages}
              perPage={perPage}
              onSetPage={setPageNumber}
              onSetPerPage={setPerPage}
            />
          </React.Fragment>
        )}
        <br />
      </Container>
    </div>
  );
}

export default Politicians;
