import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Container, Jumbotron, Row, Col } from "react-bootstrap";
import {
  TwitterTimelineEmbed,
  TwitterMentionButton,
} from "react-twitter-embed";
import BillTable from "../../components/BillTable";
import DistrictTable from "../../components/DistrictTable";
import StateCard from "../../components/StateCard";
import LoadingSpinner from "../../components/LoadingSpinner";
import { makeAPIRequest } from "../../utilities/APIRequests";
import pageStyles from "../../styles/Pages.module.css";
import politicianStyles from "../../styles/Politicians.module.css";

function PoliticianInstance() {
  const [politicianInfo, setPoliticianInfo] = useState(null);
  const [stateInfo, setStateInfo] = useState(null);
  const [districtInfo, setDistrictInfo] = useState(null);
  const { id } = useParams();

  useEffect(() => {
    async function getPoliticianInfo() {
      const requestParams = { id: id };
      const response = await makeAPIRequest("politician", requestParams);
      setPoliticianInfo(response);
    }
    getPoliticianInfo();
  }, [id]);

  useEffect(() => {
    async function getStateInfo() {
      const requestParams = { id: politicianInfo.state_id };
      const response = await makeAPIRequest("state", requestParams);
      setStateInfo(response);
      // only get the district info if the politician isn't a senator, if they
      // are, we'll just use all the districts in the state response
      if (politicianInfo.is_senator === false) {
        // since we get the whole state's information, we don't have to go back
        // to the API to get a particular district's information, since it will
        // be included in the state response
        setDistrictInfo(
          response.districts.filter(
            (district) => district.politician.id === id
          )[0]
        );
      }
    }
    if (politicianInfo !== null) {
      getStateInfo();
    }
  }, [politicianInfo, id]);

  function PoliticianPicture() {
    return (
      <React.Fragment>
        <img
          className={politicianStyles.picture}
          src={politicianInfo.image_url}
          alt="not available"
        />
      </React.Fragment>
    );
  }

  if (politicianInfo === null) {
    return (
      <div className={politicianStyles.politiciansPage}>
        <Jumbotron fluid={true} className={pageStyles.jumbotronBanner}>
          <h1 className={pageStyles.title}>Loading...</h1>
        </Jumbotron>
        <Container className={pageStyles.core}>
          <br />
          <LoadingSpinner />
          <br />
        </Container>
      </div>
    );
  } else {
    return (
      <div>
        <div className={politicianStyles.politiciansPage}>
          <Jumbotron fluid={true} className={pageStyles.jumbotronBanner}>
            <h1 className={"mb-0 " + pageStyles.title}>
              {politicianInfo.first_name} {politicianInfo.last_name}
            </h1>
            {politicianInfo.is_senator ? (
              <h3 className={"mb-3 " + pageStyles.bigText}>Senator</h3>
            ) : (
              <h3 className={"mb-3 " + pageStyles.bigText}>
                House Representative
              </h3>
            )}
          </Jumbotron>
          <Container className={pageStyles.core}>
            <br />
            <h1 className={pageStyles.subtitle}>General Information</h1>
            <PoliticianPicture />
            <h3 className={"mt-1 mb-3 " + pageStyles.bigText}>
              {politicianInfo.first_name} is part of the{" "}
              {politicianInfo.party === "D"
                ? "Democratic"
                : politicianInfo.party === "R"
                  ? "Republican"
                  : "Independent"}{" "}
              Party.
            </h3>
            <h3 className={"mb-3 " + pageStyles.bigText}>
              {politicianInfo.first_name} is {politicianInfo.age} years of age.
            </h3>
            <h3 className={"mb-3 " + pageStyles.bigText}>
              Gender: {politicianInfo.gender === "M" ? "Male" : "Female"}
            </h3>
            <h3 className={"mb-3 " + pageStyles.bigText}>
              Office Address: {politicianInfo.office}
            </h3>
            <h3 className={"mb-3 " + pageStyles.bigText}>
              Leadership Role: {politicianInfo.leadership}
            </h3>
            <h3 className={"mb-3 " + pageStyles.bigText}>
              Twitter Handle: {politicianInfo.twitter}
            </h3>
            <br />
            <TwitterMentionButton
              screenName={politicianInfo.twitter}
              options={{ height: 100, width: 100 }}
            />
            <TwitterTimelineEmbed
              sourceType="profile"
              screenName={politicianInfo.twitter}
              options={{ height: 500, width: 800 }}
            />
            <br />
            <h1 className={pageStyles.subtitle}>
              Learn More About this Politician
            </h1>
            <br />
            <Row>
              <Col>
                <h3 className={"mb-2 " + pageStyles.bigText}>
                  This Politician's State
                </h3>
                {stateInfo !== null ? (
                  <StateCard className="ml-auto mr-auto" data={stateInfo} />
                ) : null}
              </Col>
            </Row>
            <br />
            <Row>
              {politicianInfo.bills.length === 0 ? (
                <Col>
                  <h3 className={"mb-2 " + pageStyles.bigText}>
                    {politicianInfo.first_name} {politicianInfo.last_name}{" "}
                    hasn't been involved with any bills.
                  </h3>
                </Col>
              ) : (
                <Col>
                  <h3 className={"mb-2 " + pageStyles.bigText}>
                    This Politician's Bills
                  </h3>
                  <BillTable billData={politicianInfo.bills} showState={true} />
                </Col>
              )}
            </Row>
            <br />
            <Row>
              {politicianInfo.is_senator ? (
                <Col>
                  <h3 className={"mb-2 " + pageStyles.bigText}>
                    This Politician's Districts
                  </h3>
                  <h3 className={"mb-2 " + pageStyles.mediumText}>
                    Since {politicianInfo.first_name} {politicianInfo.last_name}{" "}
                    is a senator, they aren't associated with a congressional
                    district. However, these are all the congressional districts
                    in the state they represent:
                  </h3>
                  {stateInfo !== null ? (
                    <DistrictTable
                      className="ml-auto mr-auto"
                      data={stateInfo.districts}
                      showState={true}
                    />
                  ) : null}
                </Col>
              ) : (
                <Col>
                  <h3 className={"mb-2 " + pageStyles.bigText}>
                    This Politician's District
                  </h3>
                  {districtInfo !== null ? (
                    <DistrictTable
                      className="ml-auto mr-auto"
                      // wrap districtInfo in an array so DistrictTable can call map on it
                      data={[districtInfo]}
                    />
                  ) : null}
                </Col>
              )}
            </Row>
          </Container>
        </div>
      </div>
    );
  }
}

export default PoliticianInstance;
