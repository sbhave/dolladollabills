from app import db, State

import sys

abbreviation = sys.argv[1]
name = sys.argv[2]

state = db.session.query(State).filter(State.abbreviation == abbreviation).one()
state.flower = f'{{"name":"{name}","path":"/images/States/Flowers/{abbreviation}.jpg"}}'
db.session.commit()
