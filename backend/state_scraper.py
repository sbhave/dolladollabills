from models import db, State

import pandas as pd

states = [
    (1, "Alabama", "AL", 'Kay Ivey', 'R', 'East South Central', 'Audemus jura nostra defendere'),
    (2, "Alaska", "AK", 'Mike Dunleavy', 'R', 'Pacific', 'North to the Future'),
    (4, "Arizona", "AZ", 'Doug Ducey', 'R', 'Mountain', 'Ditat Deus'),
    (5, "Arkansas", "AR", 'Asa Hutchinson', 'R', 'West South Central', 'Regnat populus'),
    (6, "California", "CA", 'Gavin Newsom', 'D', 'Pacific', 'Eureka'),
    (8, "Colorado", "CO", 'Jared Polis', 'D', 'Mountain', 'Nil sine Numine'),
    (9, "Connecticut", "CT", 'Ned Lamont', 'D', 'New England', 'Qui transtulit sustinet'),
    (10, "Delaware", "DE", 'John Carney', 'D', 'South Atlantic', 'Liberty and Independence'),
    (12, "Florida", "FL", 'Ron DeSantis', 'R', 'South Atlantic', 'In God we trust'),
    (13, "Georgia", "GA", 'Brian Kemp', 'R', 'South Atlantic', 'Wisdom, justice, and moderation'),
    (15, "Hawaii", "HI", 'David Ige', 'D', 'Pacific', 'Ua Mau Ke Ea O Ka Aina I Ka Pono'),
    (16, "Idaho", "ID", 'Brad Little', 'R', 'Mountain', 'Esto perpetua'),
    (17, "Illinois", "IL", 'J.B. Pritzker', 'D', 'East North Central', 'State sovereignty, national union'),
    (18, "Indiana", "IN", 'Eroc Holcomb', 'R', 'East North Central', 'The Crossroads of America'),
    (19, "Iowa", "IA", 'Kim Reynolds', 'R', 'West North Central', 'Our liberties we prize and our rights we will maintain'),
    (20, "Kansas", "KS", 'Laura Kelly', 'D', 'West North Central', 'Ad astra per aspera'),
    (21, "Kentucky", "KY", 'Andy Beshear', 'D', 'East South Central', 'United we stand, divided we fall'),
    (22, "Louisiana", "LA", 'John Bel Edwards', 'D', 'West South Central', 'Union, justice, and confidence'),
    (23, "Maine", "ME", 'Janet Mills', 'D', 'New England', 'Dirigo'),
    (24, "Maryland", "MD", 'Larry Hogan', 'R', 'South Atlantic', 'Fatti maschii, parole femine'),
    (25, "Massachusetts", "MA", 'Charlie Baker', 'R', 'New England', 'Ense petit placidam sub libertate quietem'),
    (26, "Michigan", "MI", 'Gretchen Whitmer', 'D', 'East North Central', 'Si quaeris peninsulam amoenam circumspice'),
    (27, "Minnesota", "MN", 'Tim Walz', 'D', 'West North Central', "L'toile du Nord"),
    (28, "Mississippi", "MS", 'Tate Reeves', 'R', 'East South Central', 'Virtute et armis'),
    (29, "Missouri", "MO", 'Mike Parson', 'R', 'West North Central', 'Salus populi suprema lex esto'),
    (30, "Montana", "MT", 'Greg Gianforte', 'R', 'Mountain', 'Oro y plata'),
    (31, "Nebraska", "NE", 'Pete Ricketts', 'R', 'West North Central', 'Equality before the law'),
    (32, "Nevada", "NV", 'Steve Sisolak', 'D', 'Mountain', 'All for Our Country'),
    (33, "New Hampshire", "NH", 'Chris Sununu', 'R', 'New England', 'Live Free or Die'),
    (34, "New Jersey", "NJ", 'Phil Murphy', 'D', 'Mid-Atlantic', 'Liberty and prosperity'),
    (35, "New Mexico", "NM", 'Michelle Lujan Grisham', 'D', 'Mountain', 'Crescit eundo'),
    (36, "New York", "NY", 'Andrew Cuomo', 'D', 'Mid-Atlantic', 'Excelsior'),
    (37, "North Carolina", "NC", 'Roy Cooper', 'D', 'South Atlantic', 'Esse quam videri'),
    (38, "North Dakota", "ND", 'Doug Burgum', 'R', 'West North Central', 'Liberty and union, now and forever, one and inseparable'),
    (39, "Ohio", "OH", 'Mike DeWine', 'R', 'East North Central', 'With God all things are possible'),
    (40, "Oklahoma", "OK", 'Kevin Stitt', 'R', 'West South Central', 'Labor omnia vincit'),
    (41, "Oregon", "OR", 'Kate Brown', 'D', 'Pacific', 'Alis volat Propriis'),
    (42, "Pennsylvania", "PA", 'Tom Wolf', 'D', 'Mid-Atlantic', 'Virtue, liberty, and independence'),
    (44, "Rhode Island", "RI", 'Daniel McKee', 'D', 'New England', 'Hope'),
    (45, "South Carolina", "SC", 'Henry McMaster', 'R', 'South Atlantic', 'Dum spiro spero'),
    (46, "South Dakota", "SD", 'Kristi Noem', 'R', 'West North Central', 'Under God the people rule'),
    (47, "Tennessee", "TN", 'Bill Lee', 'R', 'East South Central', 'Agriculture and Commerce'),
    (48, "Texas", "TX", 'Greg Abbott', 'R', 'West South Central', 'Friendship'),
    (49, "Utah", "UT", 'Spencer Cox', 'R', 'Mountain', 'Industry'),
    (50, "Vermont", "VT", 'Phil Scott', 'R', 'New England', 'Freedom and Unity'),
    (51, "Virginia", "VA", 'Ralph Northam', 'D', 'South Atlantic', 'Sic semper tyrannis'),
    (53, "Washington", "WA", 'Jay Inslee', 'D', 'Pacific', 'Al-Ki'),
    (54, "West Virginia", "WV", 'Jim Justice', 'R', 'South Atlantic', 'Montani semper liberi'),
    (55, "Wisconsin", "WI", 'Tony Evers', 'D', 'East North Central', 'Forward'),
    (56, "Wyoming", "WY", 'Mark Gordon', 'R', 'Mountain', 'Equal rights'),
]

df_pop = pd.read_csv('state_population.csv')
df_area = pd.read_csv('state_area.csv')

for id, name, abbreviation, governor, party, region, motto in states:
    population = df_pop.loc[df_pop['NAME'] == name]['POPESTIMATE2019'].iloc[0]
    land_area = df_area.loc[df_area['state'] == name]['area'].iloc[0]
    db_state = State(
        id=id,
        name=name,
        abbreviation=abbreviation,
        governor=governor,
        party=party,
        region=region,
        motto=motto,
        population=population,
        land_area=land_area
    )
    db.session.add(db_state)

db.session.commit()
