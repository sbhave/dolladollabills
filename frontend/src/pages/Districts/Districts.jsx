import React, { useState, useEffect } from "react";
import { Container, Jumbotron } from "react-bootstrap";
import { Tabs, Tab } from "react-bootstrap-tabs";
import DistrictTable from "../../components/DistrictTable";
import DistrictResult from "../../components/DistrictResult";
import ModelResultsControls from "../../components/ModelResultsControls";
import PaginationControls from "../../components/PaginationControls";
import LoadingSpinner from "../../components/LoadingSpinner";
import {
  DistrictSortingOptions,
  DistrictFilteringOptions,
} from "../../data/sortingFilteringOptions";
import { makeAPIRequest } from "../../utilities/APIRequests";
import pageStyles from "../../styles/Pages.module.css";
import "../../styles/Tabs.css";

const Districts = () => {
  const [pageNumber, setPageNumber] = useState(1);
  const [perPage, setPerPage] = useState(20);
  const [currNumDistricts, setCurrNumDistricts] = useState(0);
  const [searchParam, setSearchParam] = useState("");
  const [sortOption, setSortOption] = useState("");
  const [filterParams, setFilterParams] = useState(
    Object.fromEntries(
      DistrictFilteringOptions.map((optionSet) => [optionSet.param, ""])
    )
  );
  const [districtData, setDistrictData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [onSearchTab, setOnSearchTab] = useState(false);

  useEffect(() => {
    async function getDistrictData() {
      const requestParams = {
        page: pageNumber,
        "per-page": perPage,
        search: searchParam,
        sort: sortOption,
        ...filterParams,
      };
      setLoading(true);
      const { count, data } = await makeAPIRequest("districts", requestParams);
      setDistrictData(data);
      setCurrNumDistricts(count);
      setLoading(false);
    }
    getDistrictData();
  }, [pageNumber, perPage, searchParam, filterParams, sortOption]);

  let numPages =
    currNumDistricts % perPage === 0
      ? currNumDistricts / perPage
      : Math.floor(currNumDistricts / perPage) + 1;
  let pageEnd = Math.min(
    (pageNumber - 1) * perPage + perPage,
    currNumDistricts
  );

  return (
    <div className={pageStyles.leftJustifyText}>
      <Jumbotron fluid={true} className={pageStyles.jumbotronBanner}>
        <h1 className={pageStyles.title}>
          United States Congressional Districts
        </h1>
      </Jumbotron>
      <Container className={pageStyles.core}>
        <br />
        <Tabs
          onSelect={(index, label) => {
            if (label === "View All") {
              setSearchParam("");
              setOnSearchTab(false);
            } else if (label === "Search") {
              setOnSearchTab(true);
            }
          }}
        >
          <Tab label="View All" id="view_all" />
          <Tab label="Search" id="search" />
        </Tabs>
        <br />
        <p className={pageStyles.pageText}>
          Below, you'll find a table of congressional districts in the United
          States. You're currently looking at districts{" "}
          {(pageNumber - 1) * perPage + 1} through {pageEnd} of the{" "}
          {currNumDistricts} districts in the country that match your current
          filters. Click a table row to learn more about that district!
        </p>
        <ModelResultsControls
          sortingOptions={DistrictSortingOptions}
          setSortingOption={setSortOption}
          filteringOptions={DistrictFilteringOptions}
          updateFilteringOptions={(param, selectedFilterOptions) => {
            let newFilterParams = { ...filterParams };
            newFilterParams[param] = selectedFilterOptions;
            setFilterParams(newFilterParams);
          }}
          showSearchBar={onSearchTab}
          setSearchParam={setSearchParam}
        />
        <br />
        {loading ? (
          <LoadingSpinner />
        ) : onSearchTab ? (
          <React.Fragment>
            <Container>
              {districtData?.map((data) => {
                return (
                  <DistrictResult
                    key={data.id}
                    data={data}
                    query={searchParam}
                  />
                );
              })}
            </Container>
            <PaginationControls
              pageNumber={pageNumber}
              numPages={numPages}
              perPage={perPage}
              onSetPage={setPageNumber}
              onSetPerPage={setPerPage}
            />
          </React.Fragment>
        ) : (
          <React.Fragment>
            <DistrictTable data={districtData} showState={true} />
            <PaginationControls
              pageNumber={pageNumber}
              numPages={numPages}
              perPage={perPage}
              onSetPage={setPageNumber}
              onSetPerPage={setPerPage}
            />
          </React.Fragment>
        )}
        <br />
      </Container>
    </div>
  );
};

export default Districts;
