export const toolData = [
  {
    imgUrl: "./images/Logos/aws.jpeg",
    name: "Amazon Web Services",
    bodyText:
      "Amazon Web Services is a subsidiary of Amazon providing on-demand cloud computing platforms and APIs to individuals, companies, and governments, on a metered pay-as-you-go basis.",
    link: "https://aws.amazon.com/"
  },
  {
    imgUrl: "./images/Logos/bootstrap.png",
    name: "Bootstrap",
    bodyText:
      "Quickly design and customize responsive mobile-first sites with Bootstrap, the world’s most popular front-end open source toolkit, featuring Sass variables and mixins, responsive grid system, extensive prebuilt components, and powerful JavaScript plugins.",
    link: "https://getbootstrap.com/"
  },
  {
    imgUrl: "./images/Logos/docker.jpeg",
    name: "Docker",
    bodyText:
      "Docker is a set of platform as a service products that use OS-level virtualization to deliver software in packages called containers. Containers are isolated from one another and bundle their own software, libraries and configuration files; they can communicate with each other through well-defined channels.",
    link: "https://www.docker.com/"
  },
  {
    imgUrl: "./images/Logos/flask.png",
    name: "Flask",
    bodyText:
      "Flask is a micro web framework written in Python. It is classified as a microframework because it does not require particular tools or libraries. It has no database abstraction layer, form validation, or any other components where pre-existing third-party libraries provide common functions.",
    link: "https://flask.palletsprojects.com/en/1.1.x/"
  },
  {
    imgUrl: "./images/Logos/teams.png",
    name: "Microsoft Teams",
    bodyText:
      "The communication platform that we've used to make sure we're all on the same page.",
    link: "https://www.microsoft.com/en-us/microsoft-teams/log-in"
  },
  {
    imgUrl: "./images/Logos/namecheap.png",
    name: "Namecheap",
    bodyText:
      "The domain name registrar that got our website hosted at the address you're currenly at!",
    link: "https://www.namecheap.com"
  },
  {
    imgUrl: "./images/Logos/prettier.png",
    name: "Prettier VSCode Extension",
    bodyText:
      "Prettier is an opinionated code formatter. It enforces a consistent style by parsing your code and re-printing it with its own rules that take the maximum line length into account, wrapping code when necessary.",
    link:
      "https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode"
  },
  {
    imgUrl: "./images/Logos/react.png",
    name: "React",
    bodyText:
      "A JavaScript library for building and composing reusable user interfaces.",
    link: "https://reactjs.org/"
  },
  {
    imgUrl: "./images/Logos/sqlalchemy.jpeg",
    name: "SQLAlchemy",
    bodyText:
      "SQLAlchemy is the Python SQL toolkit and Object Relational Mapper that gives application developers the full power and flexibility of SQL.",
    link: "https://www.sqlalchemy.org/"
  },
  {
    imgUrl: "./images/Logos/splinter.png",
    name: "Splinter",
    bodyText:
      "Splinter is an open source tool for testing web applications using Python. It lets you automate browser actions, such as visiting URLs and interacting with their items.",
    link: "https://splinter.readthedocs.io/en/latest/#"
  },
  {
    imgUrl: "./images/Logos/jest.png",
    name: "Jest",
    bodyText:
      "Jest is a delightful JavaScript Testing Framework with a focus on simplicity. It works on projects using Babel, Typescript, Node, React, Angular, Vue, and more!",
    link: "https://jestjs.io/"
  }
];
