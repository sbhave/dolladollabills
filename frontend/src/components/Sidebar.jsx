import React, { useState } from "react";
import * as FaIcons from "react-icons/fa";
import * as AiIcons from "react-icons/ai";
import { Link } from "react-router-dom";
import { SidebarData } from "../data/SidebarData";
import "../styles/Sidebar.css";
import { IconContext } from "react-icons";

function Sidebar() {
  // usestate is something is changing
  const [sidebar, setSidebar] = useState(false);
  const showSidebar = () => setSidebar(!sidebar);
  return (
    <div>
      {/* making a class called sidebar */}
      <div className="sidebar">
        {/* add and install icon */}
        <Link to="#" className="menu-bars">
          <FaIcons.FaBars onClick={showSidebar} />
        </Link>
      </div>
      {/* terniary operator, if sidebar showing then want it to be active otherwise hidden */}
      <nav className={sidebar ? "side-menu active" : "side-menu"}>
        <ul className="side-menu-items" onClick={showSidebar}>
          <li className="sidebar-toggle">
            <Link to="#" className="menu-bars">
              <AiIcons.AiOutlineClose />
            </Link>
          </li>
          {SidebarData.map((item, index) => {
            return (
              <li key={index} classname={item.cName}>
                <Link to={item.path}>
                  {item.icon}
                  <span className="sidebar-span">{item.title}</span>
                </Link>
              </li>
            );
          })}
        </ul>
      </nav>
    </div>
  );
}

export default Sidebar;
