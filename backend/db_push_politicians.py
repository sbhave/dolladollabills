from models import db
from models import Politician, State, District

import json
from collections import defaultdict
from typing import Set, Tuple, DefaultDict

import requests
import sys
import os

# all possible states and their ids
states = {
    state[1]: state[0]
    for state in db.session.query(State.id, State.abbreviation).distinct()
}

# process politician json file and insert into database
def insert_politicians(file, is_senator):
    with open(file) as fd:
        data = json.load(fd)

        # iterate over all senators
        for politician in data["results"][0]["members"]:
            state = politician["state"]

            # make sure state is valid
            if state not in states:
                continue
            state_id = states[state]

            # calculate district id for house
            if is_senator:
                district_id = None
            else:
                district_id = 100 * state_id
                try:
                    district_id += int(politician["district"])
                except ValueError:
                    pass

            # change empty leadership roles to "None"
            leadership_role = politician["leadership_role"]
            if leadership_role is None or len(leadership_role) == 0:
                leadership_role = "None"

            # add to database
            db_politician = Politician(
                id=politician["id"],
                state_id=state_id,
                district_id=district_id,
                first_name=politician["first_name"],
                last_name=politician["last_name"],
                leadership_role=leadership_role,
                twitter=politician["twitter_account"],
                office=politician["office"],
                party=politician["party"],
                is_senator=is_senator,
                gender=politician["gender"],
                age=(2021 - int(politician["date_of_birth"][:4])),
            )
            db.session.add(db_politician)
        db.session.commit()

def add_senate_images():
    url = "https://rapidapi.p.rapidapi.com/api/Search/ImageSearchAPI"
    headers = {
    'x-rapidapi-host': "contextualwebsearch-websearch-v1.p.rapidapi.com",
    'x-rapidapi-key': "6ee6915340msh8c843b80c4d2b5dp1ccc60jsn47948af8a4f7"
    }
    count = 0
    dir_name = "data/Politicians/Pics"
    for file_name in os.listdir(dir_name):
        count += 1
        print("count ", count)
        print("file ", file_name)
        with open(f"data/Politicians/Pics/{file_name}") as fd:
            data = json.load(fd)
            name = data["value"][0]["politician_name"].split(" ", 1)
            first_name = name[0]
            last_name = name[1]
            print(name)
            thumbnail = data["value"][0]["thumbnail"]
            p = Politician.query.filter(Politician.first_name == first_name, Politician.last_name == last_name)
            p.first().image_url = thumbnail
            db.session.commit()
            print(p.first().image_url)

def add_rep_images():

    url = "https://rapidapi.p.rapidapi.com/api/Search/ImageSearchAPI"
    headers = {
    'x-rapidapi-host': "contextualwebsearch-websearch-v1.p.rapidapi.com",
    'x-rapidapi-key': "02bc2ec430msh5c324fbbea0f525p18bc72jsn32f58f8d8c4c"
    }
    senate_names = get_senate_names()
    count = 0
    for r in range(67, 100):
        name = senate_names[r]
        print("count ", count)
        count += 1
        name_arr = senate_names[r].split(" ", 1)
        params = {
            "q": name,
            "pageNumber": 1,
            "pageSize": 10,
        }
        print("name ", name)
        response = requests.get(url, headers=headers, params=params).json()
        thumbnail = response["value"][0]["thumbnail"]
        p = Politician.query.filter(Politician.first_name == name_arr[0], Politician.last_name == name_arr[1])

        p.first().image_url = thumbnail
        db.session.commit()
        print(p.first().image_url)

# insert_politicians("data/Politicians/house_members.json", False)
# insert_politicians("data/Politicians/senate_members.json", True)

def get_senate_names():
    senate_names = []
    senators = "data/Politicians/senate_members.json"
    with open(senators) as fd:
        data = json.load(fd)
        for politician in data["results"][0]["members"]:
            name = politician["first_name"] + " " + politician["last_name"]
            senate_names.append(name)
    return senate_names

def get_rep_names():
    rep_names = []
    senate_names = []
    reps = "data/Politicians/house_members.json"
    with open(reps) as fd:
        data = json.load(fd)
        for politician in data["results"][0]["members"]:
            name = politician["first_name"] + " " + politician["last_name"]
            rep_names.append(name)
    sorted(rep_names, key=lambda x: x.split(" ")[-1])
    return rep_names

def scrape_senate_photos():
   
    
    senate_names = get_senate_names()
    
    url = "https://rapidapi.p.rapidapi.com/api/Search/ImageSearchAPI"
    headers = {
    'x-rapidapi-host': "contextualwebsearch-websearch-v1.p.rapidapi.com",
    'x-rapidapi-key': "02bc2ec430msh5c324fbbea0f525p18bc72jsn32f58f8d8c4c"
    }
   
    original_stdout = sys.stdout
    count = 0
    res_count = 0
    for s in senate_names:
        print("count ", count)
        count += 1
        output_file_name = "data/Politicians/Pics/" + s + ".json"
        with open(output_file_name, "w") as f:
            params = {
               "q": s,
               "pageNumber": 1,
               "pageSize": 10,
            }
            res_count += 1
            print("res count ", res_count)
            response = requests.get(url, headers=headers, params=params).json()
            sys.stdout = f
            
            response["value"][0]["politician_name"] = s
            print(json.dumps(response, indent=4))
        sys.stdout = original_stdout
    
#scrape_senate_photos()
add_rep_images()

