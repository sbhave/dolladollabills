import React from "react";
import { withRouter } from "react-router-dom";
import { Card } from "react-bootstrap";
import cardStyles from "../styles/Cards.module.css";
import stateStyles from "../styles/States.module.css";

class StateCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hovering: false,
    };
  }

  GetCardClasses = () => {
    if (this.state.hovering === true) {
      return (
        this.props.className +
        " " +
        stateStyles.stateCard +
        " " +
        cardStyles.cardShadow
      );
    } else {
      return this.props.className + " " + stateStyles.stateCard;
    }
  };

  render() {
    return (
      <React.Fragment>
        <Card
          className={this.GetCardClasses()}
          onMouseEnter={() => this.setState({ hovering: true })}
          onMouseLeave={() => this.setState({ hovering: false })}
          onClick={() => {
            this.props.history.push("/state/" + this.props.data.id);
          }}
        >
          <Card.Header
            className={"text-center pt-2 pb-1 " + cardStyles.cardHeader}
          >
            <Card.Title className={"mb-0 " + cardStyles.cardTitle}>
              {this.props.data.name}
            </Card.Title>
          </Card.Header>
          <div
            className={
              this.props.data.party === "D"
                ? stateStyles.cornerBadgeDem
                : this.props.data.party === "R"
                ? stateStyles.cornerBadgeRep
                : stateStyles.cornerBadgeIndep
            }
          >
            {this.props.data.party}
          </div>
          <Card.Img
            variant="left"
            className={stateStyles.stateCardPicture}
            src={this.props.data.flag_path}
          />
          <Card.Body
            className={"d-flex flex-column p-2 " + cardStyles.leftCardText}
          >
            {/* <Card.Text className="mb-2">
              <b>Governor:</b> {this.props.data.governor}
            </Card.Text> */}
            <Card.Text className="mb-2">
              <b>House Representatives:</b>{" "}
              {this.props.data.politicians.length - 2}
            </Card.Text>
            <Card.Text className="mb-2">
              <b>Population:</b> {this.props.data.population.toLocaleString()}
            </Card.Text>
            <Card.Text className="mb-2">
              <b>Land Area:</b> {this.props.data.land_area.toLocaleString()}{" "}
              miles<sup>2</sup>
            </Card.Text>
            <Card.Text className="mb-2">
              <b>Region:</b> {this.props.data.region}
            </Card.Text>
          </Card.Body>
        </Card>
      </React.Fragment>
    );
  }
}

export default withRouter(StateCard);
