import React from "react";
import { Card } from "react-bootstrap";
import aboutStyles from "../styles/About.module.css";
import cardStyles from "../styles/Cards.module.css";

class MemberCard extends React.Component<IMemberCardProps> {
  render() {
    return (
      <Card className={"bg-white rounded " + cardStyles.card}>
        <div>
          <a href={this.props.info.linkedin}>
            <span
              className={aboutStyles.linkedinOverlay}
              id={this.props.info.name + " LI"}
            >
              <img
                src="./images/Logos/linkedin.png"
                className={aboutStyles.linkedinIcon}
                alt="not available"
              />
            </span>
          </a>
          <Card.Img
            variant="top"
            src={this.props.info.image}
            className={aboutStyles.memberCardPicture}
          />
        </div>
        <Card.Body
          className={
            "text-center d-flex flex-column p-3 pr-4 pl-4 " +
            cardStyles.cardText
          }
        >
          <Card.Title className={"mt-0 mb-0 " + cardStyles.cardTitle}>
            {this.props.info.name}
          </Card.Title>
          <Card.Subtitle className={"mt-0 mb-2 " + cardStyles.cardSubtitle}>
            {this.props.info.job}
          </Card.Subtitle>
          <hr className={cardStyles.lineSeparator} />
          <Card.Text className="mt-2">{this.props.info.blurb}</Card.Text>
        </Card.Body>
        <Card.Footer
          className={
            "text-center " + cardStyles.cardText + " " + cardStyles.cardFooter
          }
        >
          <Card.Text>
            Commits: {this.props.info.commits} | Issues:{" "}
            {this.props.info.issues} | Tests: {this.props.info.tests}
          </Card.Text>
        </Card.Footer>
      </Card>
    );
  }
}

export default MemberCard;

//------
// types
//------

interface MemberData {
  name: string;
  gitlabNames: string[];
  commits: number;
  issues: number;
  tests: number;
  job: string;
  linkedin: string;
  image: string;
  blurb: string;
}

export type { MemberData };

interface IMemberCardProps {
  info: MemberData;
}
