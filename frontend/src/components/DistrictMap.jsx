import ReactMapboxGl, { Layer, Source } from "react-mapbox-gl";

const Map = ReactMapboxGl({
  accessToken:
    "pk.eyJ1IjoiaWFudGhvcm5lOTkiLCJhIjoiY2tsd3Rxd201MDU2ZDJ2cGtndWdmYXFlayJ9.i12Ve5stwZmbyKh0WcVgRw"
});

const DISTRICT_BOUNDS_SOURCE = {
  type: "vector",
  url: "mapbox://ianthorne99.121mit9v"
};

export function DistrictMap(props) {
  return (
    <Map
      style={`mapbox://styles/mapbox/streets-v11`}
      fitBounds={[
        [props.longitude + props.bBoxSize, props.latitude + props.bBoxSize],
        [props.longitude - props.bBoxSize, props.latitude - props.bBoxSize]
      ]}
      fitBoundsOptions={{ padding: 80 }}
      movingMethod="easeTo"
      containerStyle={{ height: "400px", width: "400px" }}
    >
      <div>
        <Source
          id="district_bounds_source"
          tileJsonSource={DISTRICT_BOUNDS_SOURCE}
        />
        <Layer
          id="district_bounds_layer"
          type="line"
          sourceId="district_bounds_source"
          sourceLayer="cb_2019_us_cd116_20m-0kthsa"
        />
        <Layer
          id="district_bounds_layer_fill"
          type="fill"
          sourceId="district_bounds_source"
          sourceLayer="cb_2019_us_cd116_20m-0kthsa"
          paint={{ "fill-color": "#dee6e0", "fill-opacity": 0.125 }}
        />
      </div>
    </Map>
  );
}
