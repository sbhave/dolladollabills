export const apiData = [
  {
    imgUrl: "./images/Logos/census_api.jpeg",
    name: "Census Bureau",
    bodyText:
      "Provides the most comprehensive and precise measure of U.S. state and local governments' economic activity",
    link: "https://api.geocod.io/v1.6/",
  },
  {
    imgUrl: "./images/Logos/geocodio.png",
    name: "Geocod.io",
    bodyText:
      "Used to convert addresses or city/state into latitude and longitude or turn latitude and longitude into addresses",
    link: "https://api.geocod.io/v1.6/",
  },
  {
    imgUrl: "./images/Logos/mapbox.png",
    name: "Mapbox",
    bodyText:
      "The Mapbox Maps Service includes several APIs for creating and requesting maps, either by interacting with an API directly or using an SDK. This includes services for requesting map tiles, requesting static images, uploading data to your Mapbox account, querying data in a tileset, and more.",
    link: "https://www.mapbox.com/",
  },
  {
    imgUrl: "./images/Logos/propublica.jpeg",
    name: "ProPublica Congress",
    bodyText:
      "Using the Congress API, you can retrieve legislative data from the House of Representatives, the Senate and the Library of Congress. The API includes details about members, votes, bills, nominations and other aspects of congressional activity.",
    link: "https://projects.propublica.org/api-docs/congress-api/",
  },
  {
    imgUrl: "./images/Logos/rapidapi.jpeg",
    name: "RapidAPI",
    bodyText: "Billions of webpages, images and news with a single API call.",
    link: "https://rapidapi.com/marketplace",
  },
];
