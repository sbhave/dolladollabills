import ReactDOM from "react-dom";
import StateInstance from "../../pages/States/StateInstance";
import { act } from "react-dom/test-utils";
import { unmountComponentAtNode } from "react-dom";
import "@testing-library/jest-dom/extend-expect";
import { BrowserRouter as Router } from "react-router-dom";

it("Test Rendering for state instance", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <Router>
      <StateInstance></StateInstance>
    </Router>,
    div
  );
});
