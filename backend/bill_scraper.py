from models import db        
from models import Politician, State, Bill, District
import json
import requests
import sys
import os

# starting at 100th congress, 1987
# too much data so only want to look at passed
def populate_bills():
    file = "data/Bills/bill_data.json"
    with open(file) as f:
        bill_data = json.load(f)
        push_bills(bill_data)
    db.session.commit()
    print('bills added')


def populate_bills():
    dir_name = "data/Bills"
    print('starting')
    # Iterate through all JSONs in politicians data folder
    for file_name in os.listdir(dir_name):
        print('next file')
        full_path = "%s/%s" % (dir_name, file_name)
        with open(full_path, "r") as f:
            bill_json = json.load(f)
            push_bills(bill_json)
    print('done')
    db.session.commit()


def push_bills(data):
    for d in data['results']:
        entry = dict()
        bills = d['bills']
        for info in bills:
            entry['name'] = info['short_title']
            summary = info['summary']
            if summary.__contains__("amends"):
                start = summary.index('amends')
                substring = summary[start:]
                amends = ""
                for c in substring:
                    if c != '.':
                        amends += c
                    else:
                        break
                entry['amendments'] = amends
            else:
                entry['amendments'] = "None"
            entry['sponsor'] = info['sponsor_name']
            entry['bill_number'] = info['number']
            entry['subject'] = infor['primary_subject']
            entry['bill_summary'] = info['summary']
            latest_action_date = info['latest_major_action_date']
            latest_action_year = int(latest_action_date[0:4])
            entry['latest_action_year'] = latest_action_year
            entry['bill_url'] = info['bill_uri']
            entry['state'] = info['sponsor_state']
            entry['party'] = info["sponsor_party"]
            if info["bill_type"] == "s"
                entry["is_senate"] = True
            else:
                entry["is_senate"] = False
            instance = Bill(**entry)                                  # what does this do
            db.session.add(instance)

def getId(data):
    ids = []
    for d in data['results']:
        members = d['members']
        for info in members:
            ids.append(info['id'])
    return ids

def scrapeData():
    file = "data/Politicians/senate_members.json"
    s_list = []
    with open(file) as f:
        senators_data = json.load(f)
        s_list = getId(senators_data)
    file = "data/Politicians/house_members.json"
    r_list = []
    with open(file) as f:
        reps_data = json.load(f)
        r_list = getId(reps_data)

    url = 'https://api.propublica.org/congress/v1/members/'
    headers = {'X-API-Key': 'sgca2A6sWNvVImXJkmCsrhXv6BM0ifsqTC3dhnVu'}

    original_stdout = sys.stdout
    for l in r_list:
        file_name = 'data/Bills/' + l + '.json'
        with open(file_name, 'w') as f:
            response = requests.get("https://api.propublica.org/congress/v1/members/"+ l + "/bills/passed.json", headers=headers).json()
            sys.stdout = f 
            print(json.dumps(response, indent=4))
        sys.stdout = original_stdout
    

if __name__ == "__main__":
    # scrapeData()
    populate_bills()

    