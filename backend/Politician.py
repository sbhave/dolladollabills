from models import db, Politician, Bill, District, State

from sqlalchemy import or_


def query_politician(queries):
    q = db.session.query(Politician).join(State, State.id == Politician.state_id)

    # search

    if "search" in queries:
        token = queries["search"][0]
        token_conds = []
        token_conds.append(Politician.first_name.ilike(f"%{token}%"))
        token_conds.append(Politician.last_name.ilike(f"%{token}%"))
        token_conds.append(Politician.leadership_role.ilike(f"%{token}%"))
        token_conds.append(Politician.twitter.ilike(f"%{token}%"))
        token_conds.append(Politician.office.ilike(f"%{token}%"))
        token_conds.append(Politician.party_str.ilike(f"%{token}%"))
        token_conds.append(Politician.chamber.ilike(f"%{token}%"))
        token_conds.append(Politician.gender_str.ilike(f"%{token}%"))
        token_conds.append(Politician.state_name.ilike(f"%{token}%"))
        q = q.filter(or_(*token_conds))

    # filter

    if "party" in queries:
        party_conds = []
        for party in queries["party"]:
            party_conds.append(Politician.party == party)
        q = q.filter(or_(*party_conds))

    if "is_senator" in queries:
        is_senator_conds = []
        for is_senator in queries["is_senator"]:
            is_senator_bool = is_senator == "True"
            is_senator_conds.append(Politician.is_senator == is_senator_bool)
        q = q.filter(or_(*is_senator_conds))

    if "gender" in queries:
        gender_conds = []
        for gender in queries["gender"]:
            gender_conds.append(Politician.gender == gender)
        q = q.filter(or_(*gender_conds))

    if "age" in queries:
        age_conds = []
        for age in queries["age"]:
            age_min, age_max = tuple(map(int, age.split(",")))
            age_conds.append(Politician.age.between(age_min, age_max))
        q = q.filter(or_(*age_conds))

    if "state" in queries:
        state_conds = []
        for state in queries["state"]:
            state_conds.append(State.name == state)
        q = q.filter(or_(*state_conds))

    # sort

    if "sort" not in queries:
        q = q.order_by(Politician.last_name)
    elif queries["sort"][0] == "age-inc":
        q = q.order_by(Politician.age)
    elif queries["sort"][0] == "age-dec":
        q = q.order_by(Politician.age.desc())
    elif queries["sort"][0] == "alpha-inc":
        q = q.order_by(Politician.last_name)
    elif queries["sort"][0] == "alpha-dec":
        q = q.order_by(Politician.last_name.desc())
    elif queries["sort"][0] == "state-inc":
        q = q.order_by(State.name)
    elif queries["sort"][0] == "state-dec":
        q = q.order_by(State.name.desc())

    return q.all()
