import ReactDOM from "react-dom";
import PoliticianSearch from "../../pages/Politicians/PoliticianSearch";
import { act } from "react-dom/test-utils";
import { unmountComponentAtNode } from "react-dom";
import "@testing-library/jest-dom/extend-expect";
import { BrowserRouter as Router } from "react-router-dom";

it("Test Rendering for politician search page", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <Router>
      <PoliticianSearch></PoliticianSearch>
    </Router>,
    div
  );
});
