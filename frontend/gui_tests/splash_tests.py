import argparse
from splinter import Browser
from selenium.webdriver import ChromeOptions
from unittest import main, TestCase

parser = argparse.ArgumentParser()
parser.add_argument("path")
args = parser.parse_args()
browser_executable = args.path

MAIN_URL = "https://www.dolladollabills.me/"

class TestSplash(TestCase):

  # Construct a browser to use in the tests
  @classmethod
  def setUpClass(self):
    command_line_options = ChromeOptions()
    command_line_options.add_argument("--headless")
    command_line_options.add_argument("--no-sandbox")
    command_line_options.add_argument("--disable-dev-shm-usage")
    self.browser = Browser("chrome", executable_path=browser_executable, options=command_line_options)

  # Clean up the browser after running the tests
  @classmethod
  def tearDownClass(self):
    self.browser.quit()

  # these first two tests may not be super useful
  def test_title(self):
    self.browser.visit(MAIN_URL)
    self.assertTrue(self.browser.is_text_present("Dolla Dolla Bills"))

  def test_welcome_message(self):
    self.browser.visit(MAIN_URL)
    self.assertTrue(self.browser.is_text_present("We're a website driven to bring more awareness to US politics and the bills supported by our representatives."))
    self.assertTrue(self.browser.is_text_present("Have a look around, we hope you enjoy what we have to offer!"))
    
  def test_buttons(self):
    self.browser.visit(MAIN_URL)
    self.assertTrue(self.browser.is_element_present_by_tag("button"))
    # there should be two buttons, the "Learn More About Us" and the button
    # in the navbar that shows up when it doesn't have enough horizontal space
    # for the page titles
    all_buttons = self.browser.find_by_tag("button")
    self.assertFalse(all_buttons.is_empty())
    self.assertEqual(len(all_buttons), 3)
    self.assertEqual(all_buttons[0].text, "")
    self.assertEqual(all_buttons[2].text, "Learn More About Us")

  def test_about_button(self):
    self.browser.visit(MAIN_URL)
    self.assertTrue(self.browser.is_element_present_by_tag("button"))
    # clicking the "Learn More About Us" button should take us to the About page
    all_buttons = self.browser.find_by_tag("button")
    all_buttons[2].click()
    self.assertEqual(self.browser.url, MAIN_URL + "About")


if __name__ == "__main__":
  main(argv=['first-arg-is-ignored'])