from models import db        
from models import Politician, State, Bill, District
# import os
import json
# import csv



def populate_senators():
    file = "data/Politicians/senate_members.json"
    state_entry = dict()
    state_entry['name'] = "california"
    state_entry['abbreviation'] = "CA"
    state_entry['motto'] = "eureka"
    state_entry['governor'] = "governor_name"

    state_entry['region'] = "region"
    state_entry['land_area'] = 1000
    state_entry['population'] = 150000
    state_entry['party'] = "R"
    state_instance = State(**state_entry)
    db.session.add(state_instance)
    db.session.commit()

    with open(file) as f:
        senators_data = json.load(f)
        push_politicians(senators_data)
    db.session.commit()
    print('senators added')


def populate_reps():
    file = "data/Politicians/house_members.json"
    state_entry = dict()
    state_entry['name'] = "california"
    state_entry['abbreviation'] = "CA"
    state_entry['motto'] = "eureka"
    state_entry['governor'] = "governor_name"

    state_entry['region'] = "region"
    state_entry['land_area'] = 1000
    state_entry['population'] = 150000
    state_entry['party'] = "R"
    state_instance = State(**state_entry)
    db.session.add(state_instance)
    db.session.commit()

    with open(file) as f:
        house_data = json.load(f)
        push_politicians(house_data)
    db.session.commit()
    print('reps added')

def push_politicians(data):
    # need to calculate the age from year
    count = 0
    for d in data['results']:
        entry = dict()
        members = d['members']
        for info in members:
            if info["title"] != "Delegate":
                count += 1
                entry["first_name"] = info['first_name'] 
                entry["last_name"] = info['last_name']
                if info["leadership_role"] is not None:
                    entry["leadership_role"] = info["leadership_role"]
                else:
                    entry["leadership_role"] = "None"
                entry["twitter"] = info['twitter_account']
                entry["office"] = info['office']
                entry["full_name"] = info["first_name"] + " " + info["last_name"]

                birth_date = info['date_of_birth']
                year = int(birth_date[0:4])
                age = 2021 - year
                entry["age"] = age
                if info['title'] == "Representative":
                    entry["is_senator"] = False
                else:
                    entry["is_senator"] = True
                # entry["state"] = info['state']
                entry["party"] = info['party']
                # find right json
                # entry["image_url"]
                
                entry["gender"] = info['gender']            

                s = db.session.query(State).first()

                entry['state_id'] = s.id
        
        
                instance = Politician(**entry)                                  # what does this do
                db.session.add(instance)
    print("count ", count)

    

# def test():
#     file = "data/Politicians/senate_members.json"
#     count = 0
#     with open(file) as json_file:
#         data = json.load(json_file)
#         for p in data['results']:
#             members = p['members']
#             for i in members:
#                 count += 1
#                 print(i['first_name']) 
#                 print(' ')
    
if __name__ == "__main__":
    print('main')
    # test()
    populate_senators()
    populate_reps()


