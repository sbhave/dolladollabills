import ReactDOM from "react-dom";
import BillSearch from "../../pages/Bills/BillSearch";
import DDBNavbar from "../../components/DDBNavbar";
import { act } from "react-dom/test-utils";
import { unmountComponentAtNode } from "react-dom";
import "@testing-library/jest-dom/extend-expect";
import { BrowserRouter as Router } from "react-router-dom";

it("Test Rendering for navbar", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <Router>
      <DDBNavbar></DDBNavbar>
    </Router>,
    div
  );
});
