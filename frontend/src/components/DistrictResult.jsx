import React from "react";
import { withRouter } from "react-router-dom";
import { Card } from "react-bootstrap";
import cardStyles from "../styles/Cards.module.css";
import Highlighter from "react-highlight-words";

class DistrictResult extends React.Component {
  MAX_FIELD_LENGTH = 500;

  render() {
    let {
      name,
      cities,
      counties,
      zip_codes,
      population,
      income,
      age,
      representative,
      party_str,
      state_name,
    } = this.props.data;
    if (cities.length > this.MAX_FIELD_LENGTH) {
      cities = cities.substr(0, this.MAX_FIELD_LENGTH) + "...";
    }
    if (counties.length > this.MAX_FIELD_LENGTH) {
      counties = counties.substr(0, this.MAX_FIELD_LENGTH) + "...";
    }
    if (zip_codes.length > this.MAX_FIELD_LENGTH) {
      zip_codes = zip_codes.substr(0, this.MAX_FIELD_LENGTH) + "...";
    }
    const searchWords = this.props.query.split(" ");
    return (
      <React.Fragment>
        <Card
          className="mb-3"
          onClick={() => {
            this.props.history.push("/district/" + this.props.data.id);
          }}
        >
          <Card.Body>
            <Card.Title>
              <Highlighter
                unhighlightClassName={cardStyles.cardTitle}
                highlightClassName={cardStyles.cardTitleHighlight}
                searchWords={searchWords}
                textToHighlight={name}
              />
            </Card.Title>
            <Card.Text>
              <b>State: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={state_name}
              />
            </Card.Text>
            <Card.Text>
              <b>Representative: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={representative}
              />
            </Card.Text>
            <Card.Text>
              <b>Party: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={party_str}
              />
            </Card.Text>
            <Card.Text>
              <b>Population: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={String(population)}
              />
            </Card.Text>
            <Card.Text>
              <b>Average Age: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={String(age)}
              />
            </Card.Text>
            <Card.Text>
              <b>Median Household Income: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={String(income)}
              />
            </Card.Text>
            <Card.Text>
              <b>Cities: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={cities}
              />
            </Card.Text>
            <Card.Text>
              <b>Counties: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={counties}
              />
            </Card.Text>
            <Card.Text>
              <b>Zip Codes: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={zip_codes}
              />
            </Card.Text>
          </Card.Body>
        </Card>
      </React.Fragment>
    );
  }
}

export default withRouter(DistrictResult);
