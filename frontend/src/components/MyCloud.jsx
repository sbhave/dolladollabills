import React from "react";
import { TagCloud } from "react-tagcloud";

class MyCloud extends React.Component {
  render() {
    const data = this.props.summary
      .replace(/[.,/#!$%^&*;:{}=\-_`~()]/g, "")
      .split(" ")
      .map((word) => word.toLowerCase());
    let tags = [];
    data.forEach((word) => {
      let tagArray = tags.filter((item) => item.value === word);
      if (tagArray.length === 0) {
        tags = tags.concat({ value: word, count: 1 });
      } else {
        let tag = tagArray[0];
        tag.count = tag.count + 1;
      }
    });
    return (
      <TagCloud
        minSize={10}
        maxSize={50}
        tags={tags}
        style={{ paddingLeft: "10rem", paddingRight: "10rem" }}
      />
    );
  }
}

export default MyCloud;
