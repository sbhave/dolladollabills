import "../styles/DDBNavbar.css";
import {
  Navbar,
  Nav,
  Form,
  FormControl,
  Button,
  InputGroup,
} from "react-bootstrap";
import React from "react";

function DDBNavbar() {
  const query = React.useRef();
  function searchOnClick() {
    window.location.assign("/search?q=" + query.current.value);
  }
  return (
    // Add sticky="top" to the line below to make the navbar stay at the top of the screen at all times
    <Navbar expand="lg" variant="dark" className="ddb-navbar">
      <Navbar.Brand
        href="/"
        className="ddb-navbar-link"
        style={{ fontSize: 27 + "px" }}
      >
        <b>DollaDollaBills</b>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav" className="ml-auto">
        <Nav className="ml-auto">
          <Nav.Link href="/bills" className="ddb-navbar-link mr-1">
            Bills
          </Nav.Link>
          <Nav.Link href="/politicians" className="ddb-navbar-link mr-1">
            Politicians
          </Nav.Link>
          <Nav.Link href="/districts" className="ddb-navbar-link mr-1">
            Districts
          </Nav.Link>
          <Nav.Link href="/states" className="ddb-navbar-link mr-1">
            States
          </Nav.Link>
          <Nav.Link href="/visualizations" className="ddb-navbar-link mr-1">
            Visualizations
          </Nav.Link>
          <Nav.Link href="/about" className="ddb-navbar-link mr-1">
            About
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
      <InputGroup className="ml-auto">
        <Form inline className="ml-auto">
          <FormControl
            type="text"
            placeholder="Site Search..."
            className="ml-auto"
            ref={query}
            onKeyPress={(event) => {
              if (event.key === "Enter") {
                event.preventDefault();
                searchOnClick(event);
              }
            }}
          />
          <InputGroup.Append>
            <Button variant="outline-success" onClick={() => searchOnClick()}>
              Go
            </Button>
          </InputGroup.Append>
        </Form>
      </InputGroup>
    </Navbar>
  );
}

export default DDBNavbar;
