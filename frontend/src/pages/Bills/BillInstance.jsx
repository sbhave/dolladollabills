import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Container, Jumbotron, Row, Col, CardDeck } from "react-bootstrap";
import StateCard from "../../components/StateCard";
import PoliticianCard from "../../components/PoliticianCard";
import MyCloud from "../../components/MyCloud";
import LoadingSpinner from "../../components/LoadingSpinner";
import { makeAPIRequest } from "../../utilities/APIRequests";
import pageStyles from "../../styles/Pages.module.css";
import cardStyles from "../../styles/Cards.module.css";
import billStyles from "../../styles/Bills.module.css";
import stateStyles from "../../styles/States.module.css";

function BillInstance() {
  const [billInfo, setBillInfo] = useState(null);
  const [stateInfo, setStateInfo] = useState(null);
  const [politicianInfo, setPoliticianInfo] = useState(null);
  const { id } = useParams();

  useEffect(() => {
    async function getBillInfo() {
      const requestParams = { id: id };
      const response = await makeAPIRequest("bill", requestParams);
      setBillInfo(response);
    }
    getBillInfo();
  }, [id]);

  useEffect(() => {
    async function getStateInfo() {
      const requestParams = { id: billInfo.state_id };
      const response = await makeAPIRequest("state", requestParams);
      setStateInfo(response);
      // since we get the whole state's information, we don't have to go back to
      // the API to get a particular politician's info, since it will be
      // included in the state response
      setPoliticianInfo(
        response.politicians.filter(
          (politician) => politician.id === billInfo.politician_id
        )[0]
      );
    }
    if (billInfo !== null) {
      getStateInfo();
    }
  }, [billInfo]);

  if (billInfo === null) {
    return (
      <div className={billStyles.billsPage}>
        <Jumbotron fluid={true} className={pageStyles.jumbotronBanner}>
          <h1 className={pageStyles.title}>Loading...</h1>
        </Jumbotron>
        <Container className={pageStyles.core}>
          <br />
          <LoadingSpinner />
          <br />
        </Container>
      </div>
    );
  } else {
    return (
      <div>
        <Jumbotron fluid={true} className={pageStyles.jumbotronBanner}>
          <h1 className={"mb-0 " + pageStyles.title}>{billInfo.number}</h1>
          <h1 className={"mb-0 " + pageStyles.bigText}>{billInfo.name}</h1>
        </Jumbotron>
        <Container
          className={pageStyles.leftJustifyText + " " + pageStyles.core}
        >
          <br />
          <h1 className={pageStyles.subtitle}>Bill Information:</h1>
          <h3
            className={"mb-3 " + pageStyles.bigText}
            style={{ textAlign: "left" }}
          >
            Bill Subject: {billInfo.subject}{" "}
          </h3>
          <h3
            className={"mb-1 " + pageStyles.bigText}
            style={{ textAlign: "left" }}
          >
            This bill states the following (summarized):
          </h3>
          <h3 className={"mb-3 " + pageStyles.pageText}>{billInfo.summary}</h3>
          <h3
            className={"mb-3 " + pageStyles.bigText}
            style={{ textAlign: "left" }}
          >
            Year Introduced: {billInfo.year}
          </h3>
          <h3
            className={"mb-3 " + pageStyles.bigText}
            style={{ textAlign: "left" }}
          >
            Party:{" "}
            {billInfo.party === "D"
              ? "Democratic"
              : billInfo.party === "R"
              ? "Republican"
              : "Independent"}
          </h3>
          <br />
          <h1 className={pageStyles.subtitle}>Word Cloud of Summary:</h1>
          <MyCloud summary={billInfo.summary} />
          <br />
          <div>
            <h1 className={pageStyles.subtitle}>Politicians Involved:</h1>
            <br />
            <CardDeck className={cardStyles.cardDeck}>
              {politicianInfo !== null ? (
                <PoliticianCard
                  className="ml-auto mr-auto"
                  data={politicianInfo}
                  stateName={
                    stateInfo !== null
                      ? stateInfo.name
                      : politicianInfo.state_id
                  }
                />
              ) : null}
            </CardDeck>
          </div>
          <h1 className={pageStyles.subtitle}>Learn More About this Bill</h1>
          <br />
          <Row>
            <Col>
              <h3 className={"mb-2 " + pageStyles.bigText}>
                This Bill's State
              </h3>
              {stateInfo !== null ? (
                <StateCard className="ml-auto mr-auto" data={stateInfo} />
              ) : null}
            </Col>
            {stateInfo !== null ? (
              <Col>
                <img
                  src={stateInfo.picture_path}
                  alt="Not available"
                  className={stateStyles.picture}
                />
              </Col>
            ) : null}
          </Row>
        </Container>
      </div>
    );
  }
}

export default BillInstance;
