import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { Splash } from "./pages/Home/Splash";
import About from "./pages/About/About";
import Politicians from "./pages/Politicians/Politicians";
import PoliticianInstance from "./pages/Politicians/PoliticianInstance";
import PoliticianSearch from "./pages/Politicians/PoliticianSearch";
import BillInstance from "./pages/Bills/BillInstance";
import StateInstance from "./pages/States/StateInstance";
import StateSearch from "./pages/States/StateSearch";
import DistrictInstance from "./pages/Districts/DistrictInstance";
import Bills from "./pages/Bills/Bills";
import BillSearch from "./pages/Bills/BillSearch";
import States from "./pages/States/States";
import Districts from "./pages/Districts/Districts";
import DistrictSearch from "./pages/Districts/DistrictSearch";
import Search from "./pages/Search/Search";
import DDBNavbar from "./components/DDBNavbar";
import Visualizations from "./pages/Visualizations/Visualizations";

function App() {
  return (
    <Router>
      <DDBNavbar />
      <Switch>
        <Route path="/" exact component={Splash} />
        <Route path="/about" exact component={About} />
        <Route path="/search" exact component={Search} />
        <Route path="/politicians" exact component={Politicians} />
        <Route path="/politicians/:id" component={PoliticianInstance} />
        <Route path="/politicianSearch" component={PoliticianSearch} />
        <Route path="/bills/:id" component={BillInstance} />
        <Route path="/district/:id" component={DistrictInstance} />
        <Route path="/state/:id" component={StateInstance} />
        <Route path="/stateSearch" component={StateSearch} />
        <Route path="/bills" exact component={Bills} />
        <Route path="/billSearch" component={BillSearch} />
        <Route path="/districts" exact component={Districts} />
        <Route path="/districtSearch" component={DistrictSearch} />
        <Route path="/states" exact component={States} />
        <Route path="/visualizations" exact component={Visualizations} />
      </Switch>
    </Router>
  );
}

export default App;
