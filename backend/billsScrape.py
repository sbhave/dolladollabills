import requests
import json



def json_extract(obj, key):
    """Recursively fetch values from nested JSON."""
    arr = []

    def extract(obj, arr, key):
        """Recursively search for values of key in JSON tree."""
        if isinstance(obj, dict):
            for k, v in obj.items():
                if isinstance(v, (dict, list)):
                    extract(v, arr, key)
                elif k == key:
                    arr.append(v)
        elif isinstance(obj, list):
            for item in obj:
                extract(item, arr, key)
        return arr

    values = extract(obj, arr, key)
    return values

url = 'https://api.propublica.org/congress/v1/bills/search.json?offset=100000'
url2 = 'https://api.propublica.org/congress/v1/117/both/bills/introduced.json?offset=20'

API_KEY = 'O0VyIVQEKiY59Ucs51KdQ1VHvtU0TmGSn88EM5jU'


x = []

for i in range(2):
    temp = 20 * i;
    res = requests.get(
        'https://api.propublica.org/congress/v1/bills/search.json?offset=' + str(temp),
        headers={'X-API-Key': 'O0VyIVQEKiY59Ucs51KdQ1VHvtU0TmGSn88EM5jU'},
    )
    x.append(json_extract(res.json(), 'bill_id'))

print(x)

# res = requests.get(
#     url,
#     headers={'X-API-Key': 'O0VyIVQEKiY59Ucs51KdQ1VHvtU0TmGSn88EM5jU'},
# )

print(res)

# x = json_extract(res.json(), 'bill_id')
# print(x)

