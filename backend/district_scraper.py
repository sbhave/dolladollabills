from models import db, District

import requests
import csv
from collections import defaultdict
from typing import Dict, DefaultDict

import pandas as pd

CENSUS_API_KEY = "f7f6b16b3106b3af548dd8a9ce66da86dd7caad8"

STATE_ABBRS = {
    "AL",
    "AK",
    "AZ",
    "AR",
    "CA",
    "CO",
    "CT",
    "DE",
    "FL",
    "GA",
    "HI",
    "ID",
    "IL",
    "IN",
    "IA",
    "KS",
    "KY",
    "LA",
    "ME",
    "MD",
    "MA",
    "MI",
    "MN",
    "MS",
    "MO",
    "MT",
    "NE",
    "NV",
    "NH",
    "NJ",
    "NM",
    "NY",
    "NC",
    "ND",
    "OH",
    "OK",
    "OR",
    "PA",
    "RI",
    "SC",
    "SD",
    "TN",
    "TX",
    "UT",
    "VT",
    "VA",
    "WA",
    "WV",
    "WI",
    "WY",
}

district_data: DefaultDict[tuple, Dict] = defaultdict(
    lambda: {
        "state_id": 0,
        "name": "",
        "zip_codes": set(),
        "population": 0,
        "age": 0.0,
        "income": 0,
    }
)

zip_code_data: DefaultDict[str, Dict] = defaultdict(
    lambda: {
        "cities": set(),
        "counties": set(),
    }
)

# for each zip code, assign it to a district and get its city/county
# along the way, get district data as each district is seen
df_zip = pd.read_csv("data/Districts/us_zip_codes.csv").set_index("zip")
with open("data/Districts/zip_code_districts.csv") as zip_district_csv:
    zip_district_reader = csv.DictReader(zip_district_csv)

    # iterate over all zip codes
    for row in zip_district_reader:
        state_id = row["state_fips"]
        state_abbr = row["state_abbr"]
        zip_code = row["zcta"]
        district_num = row["cd"]

        # make sure state is valid
        if state_abbr not in STATE_ABBRS:
            continue

        # define district
        if len(district_num) == 1:
            # pad with leading zero
            district_num = "0" + district_num
        district = (state_id, district_num)
        print(
            f"Processing zip {zip_code} in district {state_abbr}-{district_num}",
            end="\r",
        )

        # get one-time data for district if needed
        if district not in district_data:
            district_data[district]["state_id"] = int(state_id)

            # fetch name and demographic data
            url = f"\
https://api.census.gov/data/2019/acs/acs1?\
get=NAME,B01003_001E,B01002_001E,B06011_001E&\
for=congressional%20district:{district_num}&\
in=state:{state_id}&\
key={CENSUS_API_KEY}"
            response = requests.get(url)
            if response:
                content = response.json()
                name, population, age, income = content[1][:4]
                district_data[district]["name"] = name
                district_data[district]["population"] = int(population)
                district_data[district]["age"] = float(age)
                district_data[district]["income"] = int(income)
            else:
                print(f"\nGET of {url} failed with code {response.status_code}")
                exit(1)

        # make sure zip code is real first
        if int(zip_code) not in df_zip.index:
            continue

        # get geographic data for zip code if needed
        if zip_code not in zip_code_data:
            city = df_zip["city"][int(zip_code)]
            county = df_zip["county_name"][int(zip_code)]
            zip_code_data[zip_code]["cities"].add(city)
            zip_code_data[zip_code]["counties"].add(county)

        # add zip code to district
        district_data[district]["zip_codes"].add(zip_code)

    # flush progress message
    print()

# finalize district data, insert into database
for district_dict in district_data.values():
    # union all cities and counties of zip codes in this district
    cities = set()
    counties = set()
    zip_codes = district_dict["zip_codes"]
    for zip_code in zip_codes:
        cities.update(zip_code_data[zip_code]["cities"])
        counties.update(zip_code_data[zip_code]["counties"])

    # add to database
    cities_str = "," + ",".join(cities) + ","
    counties_str = "," + ",".join(counties) + ","
    zip_codes_str = "," + ",".join(zip_codes) + ","
    db_district = District(
        state_id=district_dict["state_id"],
        name=district_dict["name"],
        cities=cities_str,
        counties=counties_str,
        zip_codes=zip_codes_str,
        population=district_dict["population"],
        age=district_dict["age"],
        income=district_dict["income"],
    )
    db.session.add(db_district)
db.session.commit()
