import ReactDOM from "react-dom";
import PoliticianInstance from "../../pages/Politicians/PoliticianInstance";
import { act } from "react-dom/test-utils";
import { unmountComponentAtNode } from "react-dom";
import "@testing-library/jest-dom/extend-expect";
import { BrowserRouter as Router } from "react-router-dom";

it("Test Rendering for politician instance", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <Router>
      <PoliticianInstance></PoliticianInstance>
    </Router>,
    div
  );
});
