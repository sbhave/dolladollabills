import requests
import json

vaccines = requests.get('https://vaxtracker.me/api/vaccines').text
vaccineArray = []
countryArray = []
vaccines_dict = json.loads(vaccines)
for vaccines in vaccines_dict:
    vaccines_dict = {}
    if(int(vaccines['num_approved_countries']) != 0):
        vaccineArray.append(vaccines['name'])
        countryArray.append(vaccines['num_approved_countries'])
with open('vaccinesPie.json', 'w') as fp:
    json.dump(vaccineArray, fp=fp)
    json.dump(countryArray, fp=fp)