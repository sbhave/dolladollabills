import "../../App.css";
import React, { useState, useEffect } from "react";
import { Container, Jumbotron, CardDeck } from "react-bootstrap";
import { Tabs, Tab } from "react-bootstrap-tabs";
import StateCard from "../../components/StateCard";
import StateResult from "../../components/StateResult";
import ModelResultsControls from "../../components/ModelResultsControls";
import PaginationControls from "../../components/PaginationControls";
import LoadingSpinner from "../../components/LoadingSpinner";
import {
  StateSortingOptions,
  StateFilteringOptions,
} from "../../data/sortingFilteringOptions";
import { makeAPIRequest } from "../../utilities/APIRequests";
import "bootstrap/dist/css/bootstrap.min.css";
import pageStyles from "../../styles/Pages.module.css";
import cardStyles from "../../styles/Cards.module.css";
import "../../styles/Tabs.css";

function States() {
  const [pageNumber, setPageNumber] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [currNumStates, setCurrNumStates] = useState(0);
  const [searchParam, setSearchParam] = useState("");
  const [sortOption, setSortOption] = useState("");
  const [filterParams, setFilterParams] = useState(
    Object.fromEntries(
      StateFilteringOptions.map((optionSet) => [optionSet.param, ""])
    )
  );
  const [stateData, setStateData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [onSearchTab, setOnSearchTab] = useState(false);

  useEffect(() => {
    async function getStateData() {
      console.log("search param " + searchParam);
      const requestParams = {
        page: pageNumber,
        "per-page": perPage,
        search: searchParam,
        sort: sortOption,
        ...filterParams,
      };
      setLoading(true);
      const { count, data } = await makeAPIRequest("states", requestParams);
      setStateData(data);
      setCurrNumStates(count);
      setLoading(false);
    }
    getStateData();
  }, [pageNumber, perPage, searchParam, filterParams, sortOption]);

  let numPages =
    currNumStates % perPage === 0
      ? currNumStates / perPage
      : Math.floor(currNumStates / perPage) + 1;
  let pageEnd = Math.min((pageNumber - 1) * perPage + perPage, currNumStates);

  return (
    <div className={pageStyles.leftJustifyText}>
      <Jumbotron fluid={true} className={pageStyles.jumbotronBanner}>
        <h1 className={pageStyles.title}>These 50 United States</h1>
      </Jumbotron>
      <Container className={pageStyles.core}>
        <br />
        <Tabs
          onSelect={(index, label) => {
            if (label === "View All") {
              setSearchParam("");
              setOnSearchTab(false);
            } else if (label === "Search") {
              setOnSearchTab(true);
            }
          }}
        >
          <Tab label="View All" id="view_all" />
          <Tab label="Search" id="search" />
        </Tabs>
        <br />
        <p className={pageStyles.pageText}>
          Below, you'll find cards for states {(pageNumber - 1) * perPage + 1}{" "}
          through {pageEnd} of the {currNumStates} states that match your
          current filters. Click on a card to learn more about that state!
        </p>
        <ModelResultsControls
          sortingOptions={StateSortingOptions}
          setSortingOption={setSortOption}
          filteringOptions={StateFilteringOptions}
          updateFilteringOptions={(param, selectedFilterOptions) => {
            let newFilterParams = { ...filterParams };
            newFilterParams[param] = selectedFilterOptions;
            setFilterParams(newFilterParams);
          }}
          showSearchBar={onSearchTab}
          setSearchParam={setSearchParam}
        />
        <br />
        {loading ? (
          <LoadingSpinner />
        ) : onSearchTab ? (
          <React.Fragment>
            <Container>
              {stateData?.map((data) => {
                return (
                  <StateResult key={data.id} data={data} query={searchParam} />
                );
              })}
            </Container>
            <PaginationControls
              pageNumber={pageNumber}
              numPages={numPages}
              perPage={perPage}
              onSetPage={setPageNumber}
              onSetPerPage={setPerPage}
            />
          </React.Fragment>
        ) : (
          <React.Fragment>
            <CardDeck className={cardStyles.cardDeck}>
              {stateData?.map((data) => (
                <StateCard key={data.id} data={data} />
              ))}
            </CardDeck>
            <PaginationControls
              pageNumber={pageNumber}
              numPages={numPages}
              perPage={perPage}
              onSetPage={setPageNumber}
              onSetPerPage={setPerPage}
            />
          </React.Fragment>
        )}
        <br />
      </Container>
    </div>
  );
}

export default States;
