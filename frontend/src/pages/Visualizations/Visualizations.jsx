import React, { useState, useEffect } from "react";
import billData from "./BubbleChartData.json";
import BubbleChart from "@weknow/react-bubble-chart-d3";
import pageStyles from "../../styles/Pages.module.css";
import "../../styles/Tabs.css";
import { Tabs, Tab } from "react-bootstrap-tabs";
import { Bar } from "react-chartjs-2";
import { Doughnut } from "react-chartjs-2";
import { Pie } from "react-chartjs-2";
import { MDBContainer } from "mdbreact";
import LoadingSpinner from "../../components/LoadingSpinner";
import { makeAPIRequest } from "../../utilities/APIRequests";
import barData from "./BarChartData.json";
import ageData from "./PolarChartData.json";
import beliefData from "./beliefs.json";
import countrybData from "./countriesBubbleData.json";
import vaccinepData from "./vaccinesPie.json";

import {
  Container,
  Jumbotron,
} from "react-bootstrap";

function PoliticianChart() {
  const [data, setData] = useState([]);
  const [stateData, setStateData] = useState([]);

  useEffect(() => {
    console.log("Getting politician data from API!");
    async function getStateData() {
      const requestParams = { page: 1, "per-page": 50 };
      const response = await makeAPIRequest("states", requestParams);
      setStateData(response);
    }
    getStateData();
  }, []);

  var states = [];
  // var numBills = [];
  for (let i = 0; i < stateData.length; i++) {
    if (stateData[i]["bills"] !== undefined) {
      states.push(stateData[i]["name"]);
    }
  }
  console.log("states!");
  console.log(states);

  useEffect(() => {
    console.log("Getting politician data from API!");
    async function getPoliticianData() {
      const requestParams = { page: 1, "per-page": 535 };
      const response = await makeAPIRequest("politicians", requestParams);
      setData(response);
    }
    getPoliticianData();
  }, []);

  // sort politicians in order of number of bills
  // function GetSortOrder(prop) {
  //   return function (a, b) {
  //     if (a[prop] != undefined && b[prop] != undefined) {
  //       if (a[prop].length < b[prop].length) {
  //         return 1;
  //       } else if (a[prop].length > b[prop].length) {
  //         return -1;
  //       }
  //       return 0;
  //     }
  //   };
  // }
  // data.sort(GetSortOrder("bills"));
  var orderedList = [];
  var ageRanges = {};
  ageRanges[2] = 0;
  ageRanges[3] = 0;
  ageRanges[4] = 0;
  ageRanges[5] = 0;
  ageRanges[6] = 0;
  ageRanges[7] = 0;
  ageRanges[8] = 0;
  var total = 0;
  for (let i = 0; i < data.length - 1; i++) {
    if (data[i] !== undefined) {
      if (data[i["age"] < 30]) {
        ageRanges[2] += 1;
      } else if (data[i]["age"] >= 30 && data[i]["age"] <= 39) {
        ageRanges[3] += 1;
      } else if (data[i]["age"] >= 40 && data[i]["age"] <= 49) {
        ageRanges[4] += 1;
      } else if (data[i]["age"] >= 50 && data[i]["age"] <= 59) {
        ageRanges[5] += 1;
      } else if (data[i]["age"] >= 60 && data[i]["age"] <= 69) {
        ageRanges[6] += 1;
      } else if (data[i]["age"] >= 70 && data[i]["age"] <= 79) {
        ageRanges[7] += 1;
      } else {
        ageRanges[8] += 1;
      }

      orderedList.push({
        label: data[i]["first_name"] + " " + data[i]["last_name"],
        value: data[i]["bills"].length,
      });
      total = total + 1;
    }
  }
  console.log(ageRanges);

  return (
    <div>
      <Jumbotron fluid={true} className={pageStyles.jumbotronBanner}>
        <h1 className={pageStyles.title}>Data Visualizations</h1>
      </Jumbotron>
      <Container className={pageStyles.core}>
        <div className="centered-tabs">
          <br />
          <Tabs>
            <Tab label="DollaDollaBills Visuals">
              <br />
              {data === [] || stateData === [] ? (
                <LoadingSpinner />
              ) : (
                <React.Fragment>
                  <div>
                    <h1 className={pageStyles.subtitle}>
                      Politicians With Highest Contributions to Bills
                    </h1>
                  </div>
                  <BubbleChart
                    graph={{
                      zoom: 0.7,
                      offsetX: 0.2,
                      offsetY: 0.0,
                    }}
                    showLegend={false}
                    width={1000}
                    height={800}
                    valueFont={{
                      family: "Arial",
                      size: 12,
                      color: "#fff",
                      weight: "bold",
                    }}
                    labelFont={{
                      family: "Arial",
                      size: 14,
                      color: "#fff",
                      weight: "bold",
                    }}
                    data={billData}
                  />
                  <br />
                  <div>
                    <h1 className={pageStyles.subtitle}>
                      Number of Bills Per State
                    </h1>
                  </div>
                  <MDBContainer>
                    <Bar data={barData} />
                  </MDBContainer>
                  <br />
                  <div>
                    <h1 className={pageStyles.subtitle}>Politician Ages</h1>
                  </div>
                  <MDBContainer style={{ width: "800px" }}>
                    <Doughnut
                      data={ageData}
                      options={{
                        responsive: true,
                        height: 100,
                        width: 100,
                      }}
                    />
                  </MDBContainer>
                </React.Fragment>
              )}
              <br />
            </Tab>
            <Tab label="VaxTracker Visuals">
              <br />
              <div>
                <h1 className={pageStyles.subtitle}>Our Provider</h1>
              </div>
              <p className={pageStyles.pageText + " ml-5 mr-5"}>
                Our provider team was VaxTracker. They created a website with
                all sorts of information about Covid-19 vaccines, including
                vaccine facts, information on different clinical trials,
                pandemic-related statistics for different countries, and data on
                beliefs surrounding Covid-19 in different countries! You can
                check their website out{" "}
                <a href="https://www.vaxtracker.me">here</a>!
              </p>
              <br />
              <div>
                <h1 className={pageStyles.subtitle}>
                  Percentage of Population Vaccinated in Each Country
                </h1>
              </div>
              <BubbleChart
                graph={{
                  zoom: 0.7,
                  offsetX: 0.2,
                  offsetY: 0.0,
                }}
                showLegend={false}
                width={1000}
                height={800}
                valueFont={{
                  family: "Arial",
                  size: 12,
                  color: "#fff",
                  weight: "bold",
                }}
                labelFont={{
                  family: "Arial",
                  size: 14,
                  color: "#fff",
                  weight: "bold",
                }}
                data={countrybData}
              />
              <br />
              <div>
                <h1 className={pageStyles.subtitle}>
                  Percentage of People Likely to Take Vaccine Per Country
                </h1>
              </div>
              <MDBContainer>
                <Bar
                  data={beliefData}
                  options={{
                    responsive: true,
                    height: 100,
                    width: 300,
                  }}
                />
              </MDBContainer>
              <br />
              <div>
                <h1 className={pageStyles.subtitle}>
                  Number of Countries that Approved Each Vaccine
                </h1>
              </div>
              <MDBContainer style={{ width: "800px" }}>
                <Pie
                  data={vaccinepData}
                  options={{
                    responsive: true,
                    height: 100,
                    width: 100,
                  }}
                />
              </MDBContainer>
              <br />
            </Tab>
          </Tabs>
        </div>
      </Container>
    </div>
  );
}

export default PoliticianChart;

// function Visuals() {
//   console.log(React.version);
//   return (
//     <>
//       <div>
//         Hello
//         <BubbleChart
//           graph={{
//             zoom: 0.7,
//             offsetX: 0.0,
//             offsetY: 0.0
//           }}
//           showLegend={false}
//           width={1000}
//           height={800}
//           valueFont={{
//             family: "Arial",
//             size: 12,
//             color: "#fff",
//             weight: "bold"
//           }}
//           labelFont={{
//             family: "Arial",
//             size: 16,
//             color: "#fff",
//             weight: "bold"
//           }}
//           data={billData}
//         />
//       </div>
//     </>
//   );
// }

// export default Visuals;
