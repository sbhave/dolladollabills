import argparse
from splinter import Browser
from selenium.webdriver import ChromeOptions
from unittest import main, TestCase

parser = argparse.ArgumentParser()
parser.add_argument("path")
args = parser.parse_args()
browser_executable = args.path

POLITICIANS_URL = "https://www.dolladollabills.me/politicians"
BILLS_URL = "https://www.dolladollabills.me/bills"
DISTRICTS_URL = "https://www.dolladollabills.me/districts"
STATES_URL = "https://www.dolladollabills.me/states"
DEFAULT_WAIT = 20

class TestFiltering(TestCase):

  # Construct a browser to use in the tests
  @classmethod
  def setUpClass(self):
    command_line_options = ChromeOptions()
    command_line_options.add_argument("--headless")
    command_line_options.add_argument("--no-sandbox")
    command_line_options.add_argument("--disable-dev-shm-usage")
    self.browser = Browser("chrome", executable_path=browser_executable, options=command_line_options)

  # Class up the browser after running the tests
  @classmethod
  def tearDownClass(self):
    self.browser.quit()

  def test_politicians_state_alaska(self):
    self.browser.visit(POLITICIANS_URL)
    self.assertTrue(self.browser.is_text_present("of the 535", wait_time=DEFAULT_WAIT))
    state_filter = self.browser.find_by_id("state")
    state_filter.click()
    self.assertTrue(self.browser.is_text_present("Alaska", wait_time=DEFAULT_WAIT))
    alaska_option = self.browser.find_by_text("Alaska")
    alaska_option.click()
    self.assertTrue(self.browser.is_text_present("1 through 3 of the 3", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Lisa", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Murkowski", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Dan", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Sullivan", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Don", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Young", wait_time=DEFAULT_WAIT))

  def test_politicians_multi_attribute(self):
    self.browser.visit(POLITICIANS_URL)
    self.assertTrue(self.browser.is_text_present("of the 535", wait_time=DEFAULT_WAIT))
    state_filter = self.browser.find_by_id("state")
    state_filter.click()
    self.assertTrue(self.browser.is_text_present("Alaska", wait_time=DEFAULT_WAIT))
    alaska_option = self.browser.find_by_text("Alaska")
    alaska_option.click()
    age_range_filter = self.browser.find_by_id("age")
    age_range_filter.click()
    self.assertTrue(self.browser.is_text_present("Over 80", wait_time=DEFAULT_WAIT))
    over_eighty_option = self.browser.find_by_text("Over 80")
    over_eighty_option.click()
    self.assertTrue(self.browser.is_text_present("1 through 1 of the 1", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Don", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Young", wait_time=DEFAULT_WAIT))

  def test_politicians_no_result_filters(self):
    self.browser.visit(POLITICIANS_URL)
    self.assertTrue(self.browser.is_text_present("of the 535", wait_time=DEFAULT_WAIT))
    party_filter = self.browser.find_by_id("party")
    party_filter.click()
    self.assertTrue(self.browser.is_text_present("Democrat", wait_time=DEFAULT_WAIT))
    democrat_option = self.browser.find_by_text("Democrat")
    democrat_option.click()
    type_filter = self.browser.find_by_id("is_senator")
    type_filter.click()
    self.assertTrue(self.browser.is_text_present("Senator", wait_time=DEFAULT_WAIT))
    senator_option = self.browser.find_by_text("Senator")
    senator_option.click()
    state_filter = self.browser.find_by_id("state")
    state_filter.click()
    self.assertTrue(self.browser.is_text_present("Arkansas", wait_time=DEFAULT_WAIT))
    alaska_option = self.browser.find_by_text("Arkansas")
    alaska_option.click()
    self.assertTrue(self.browser.is_text_present("1 through 0 of the 0", wait_time=DEFAULT_WAIT))

  def test_bills_type_senate_bill(self):
    self.browser.visit(BILLS_URL)
    self.assertTrue(self.browser.is_text_present("of the 1042", wait_time=DEFAULT_WAIT))
    type_filter = self.browser.find_by_id("bill_type")
    type_filter.click()
    self.assertTrue(self.browser.is_text_present("Senate Bill", wait_time=DEFAULT_WAIT))
    senate_bill_option = self.browser.find_by_text("Senate Bill")
    senate_bill_option.click()
    self.assertTrue(self.browser.is_text_present("1 through 20 of the 337", wait_time=DEFAULT_WAIT))

  def test_bills_multi_attribute(self):
    self.browser.visit(BILLS_URL)
    self.assertTrue(self.browser.is_text_present("of the 1042", wait_time=DEFAULT_WAIT))
    type_filter = self.browser.find_by_id("bill_type")
    type_filter.click()
    self.assertTrue(self.browser.is_text_present("Senate Bill", wait_time=DEFAULT_WAIT))
    senate_bill_option = self.browser.find_by_text("Senate Bill")
    senate_bill_option.click()
    state_filter = self.browser.find_by_id("state")
    state_filter.click()
    self.assertTrue(self.browser.is_text_present("Texas", wait_time=DEFAULT_WAIT))
    senate_bill_option = self.browser.find_by_text("Texas")
    senate_bill_option.click()
    self.assertTrue(self.browser.is_text_present("of the 20", wait_time=DEFAULT_WAIT))

  def test_bills_no_result_filters(self):
    self.browser.visit(BILLS_URL)
    self.assertTrue(self.browser.is_text_present("of the 1042", wait_time=DEFAULT_WAIT))
    type_filter = self.browser.find_by_id("bill_type")
    type_filter.click()
    self.assertTrue(self.browser.is_text_present("Senate Bill", wait_time=DEFAULT_WAIT))
    senate_bill_option = self.browser.find_by_text("Senate Bill")
    senate_bill_option.click()
    state_filter = self.browser.find_by_id("state")
    state_filter.click()
    self.assertTrue(self.browser.is_text_present("Texas", wait_time=DEFAULT_WAIT))
    senate_bill_option = self.browser.find_by_text("Texas")
    senate_bill_option.click()
    type_filter = self.browser.find_by_id("year")
    type_filter.click()
    self.assertTrue(self.browser.is_text_present("2012", wait_time=DEFAULT_WAIT))
    senate_bill_option = self.browser.find_by_text("2012")
    senate_bill_option.click()
    self.assertTrue(self.browser.is_text_present("of the 0", wait_time=DEFAULT_WAIT))

  def test_districts_age_twenties(self):
    self.browser.visit(DISTRICTS_URL)
    self.assertTrue(self.browser.is_text_present("of the 435", wait_time=DEFAULT_WAIT))
    age_filter = self.browser.find_by_id("age")
    age_filter.click()
    self.assertTrue(self.browser.is_text_present("20-29", wait_time=DEFAULT_WAIT))
    twenties_option = self.browser.find_by_text("20-29")
    twenties_option.click()
    self.assertTrue(self.browser.is_text_present("of the 1", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Utah", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Congressional District 3", wait_time=DEFAULT_WAIT))

  def test_districts_multi_attribute(self):
    self.browser.visit(DISTRICTS_URL)
    self.assertTrue(self.browser.is_text_present("of the 435", wait_time=DEFAULT_WAIT))
    age_filter = self.browser.find_by_id("age")
    age_filter.click()
    self.assertTrue(self.browser.is_text_present("30-39", wait_time=DEFAULT_WAIT))
    twenties_option = self.browser.find_by_text("30-39")
    twenties_option.click()
    population_filter = self.browser.find_by_id("population")
    population_filter.click()
    self.assertTrue(self.browser.is_text_present("700,000-749,999", wait_time=DEFAULT_WAIT))
    twenties_option = self.browser.find_by_text("700,000-749,999")
    twenties_option.click()
    self.assertTrue(self.browser.is_text_present("of the 75", wait_time=DEFAULT_WAIT))

  def test_districts_no_result_filters(self):
    self.browser.visit(DISTRICTS_URL)
    self.assertTrue(self.browser.is_text_present("of the 435", wait_time=DEFAULT_WAIT))
    population_filter = self.browser.find_by_id("population")
    population_filter.click()
    self.assertTrue(self.browser.is_text_present("700,000-749,999", wait_time=DEFAULT_WAIT))
    twenties_option = self.browser.find_by_text("700,000-749,999")
    twenties_option.click()
    income_filter = self.browser.find_by_id("income")
    income_filter.click()
    self.assertTrue(self.browser.is_text_present("Over $200,000", wait_time=DEFAULT_WAIT))
    big_option = self.browser.find_by_text("Over $200,000")
    big_option.click()
    self.assertTrue(self.browser.is_text_present("of the 0", wait_time=DEFAULT_WAIT))

  def test_states_region_new_england(self):
    self.browser.visit(STATES_URL)
    self.assertTrue(self.browser.is_text_present("of the 50", wait_time=DEFAULT_WAIT))
    region_filter = self.browser.find_by_id("region")
    region_filter.click()
    self.assertTrue(self.browser.is_text_present("New England", wait_time=DEFAULT_WAIT))
    new_england_option = self.browser.find_by_text("New England")
    new_england_option.click()
    self.assertTrue(self.browser.is_text_present("1 through 6 of the 6", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Connecticut", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Maine", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Massachusetts", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("New Hampshire", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Rhode Island", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Vermont", wait_time=DEFAULT_WAIT))

  def test_states_multi_attribute(self):
    self.browser.visit(STATES_URL)
    self.assertTrue(self.browser.is_text_present("of the 50", wait_time=DEFAULT_WAIT))
    house_rep_filter = self.browser.find_by_id("num_reps")
    house_rep_filter.click()
    self.assertTrue(self.browser.is_text_present("20-29", wait_time=DEFAULT_WAIT))
    twenties_option = self.browser.find_by_text("20-29")
    twenties_option.click()
    self.assertTrue(self.browser.is_text_present("1 through 2 of the 2", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Florida", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("New York", wait_time=DEFAULT_WAIT))
    region_filter = self.browser.find_by_id("region")
    region_filter.click()
    self.assertTrue(self.browser.is_text_present("South Atlantic", wait_time=DEFAULT_WAIT))
    south_atlantic_option = self.browser.find_by_text("South Atlantic")
    south_atlantic_option.click()
    self.assertTrue(self.browser.is_text_present("1 through 1 of the 1", wait_time=DEFAULT_WAIT))
    self.assertTrue(self.browser.is_text_present("Florida", wait_time=DEFAULT_WAIT))

  def test_states_no_result_filters(self):
    self.browser.visit(STATES_URL)
    self.assertTrue(self.browser.is_text_present("of the 50", wait_time=DEFAULT_WAIT))
    party_filter = self.browser.find_by_id("party")
    party_filter.click()
    self.assertTrue(self.browser.is_text_present("Republican", wait_time=DEFAULT_WAIT))
    republican_option = self.browser.find_by_text("Republican")
    republican_option.click()
    population_filter = self.browser.find_by_id("population")
    population_filter.click()
    self.assertTrue(self.browser.is_text_present("Over 35,000,000", wait_time=DEFAULT_WAIT))
    big_option = self.browser.find_by_text("Over 35,000,000")
    big_option.click()
    self.assertTrue(self.browser.is_text_present("1 through 0 of the 0", wait_time=DEFAULT_WAIT))

if __name__ == "__main__":
  main(argv=['first-arg-is-ignored'])