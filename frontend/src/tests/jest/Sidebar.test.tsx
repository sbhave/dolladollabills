import ReactDOM from "react-dom";
import Sidebar from "../../components/Sidebar";
import { act } from "react-dom/test-utils";
import { unmountComponentAtNode } from "react-dom";
import "@testing-library/jest-dom/extend-expect";
import { BrowserRouter as Router } from "react-router-dom";

it("Test Rendering for sidebar", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <Router>
      <Sidebar></Sidebar>
    </Router>,
    div
  );
});
