from unittest import main, TestCase
from time import sleep
import requests

API_BASE = "https://www.dolladollabills.me/api"
MAX_RETRIES = 5
RETRY_INTERVAL = 1.0


def get_with_retries(url):
    retries = 0
    while True:
        try:
            return requests.get(url)
        except:
            if retries == MAX_RETRIES:
                raise
            retries += 1
            sleep(RETRY_INTERVAL)


class StateTests(TestCase):
    def test_num_states_per_page(self):
        res = get_with_retries(f"{API_BASE}/states")
        self.assertEqual(res.status_code, 200)
        res_json = res.json()
        self.assertEqual(len(res_json["data"]), 10)

    def test_num_all_states(self):
        res = get_with_retries(f"{API_BASE}/states?page=1&per-page=50")
        self.assertEqual(res.status_code, 200)
        res_json = res.json()
        self.assertEqual(len(res_json["data"]), 50)

    def test_states_count(self):
        res = get_with_retries(f"{API_BASE}/states/count")
        self.assertEqual(res.status_code, 200)
        res_json = res.json()
        self.assertEqual(res_json, "50")

    def test_name_from_state_id(self):
        res = get_with_retries(f"{API_BASE}/state?id=56")
        self.assertEqual(res.status_code, 200)
        res_json = res.json()
        self.assertEqual(res_json["name"], "Wyoming")

    def test_governor_from_state_id(self):
        res = get_with_retries(f"{API_BASE}/state?id=2")
        self.assertEqual(res.status_code, 200)
        res_json = res.json()
        self.assertEqual(res_json["governor"], "Mike Dunleavy")


class PoliticianTests(TestCase):
    def test_num_politicians_per_page(self):
        res = get_with_retries(f"{API_BASE}/politicians")
        self.assertEqual(res.status_code, 200)
        res_json = res.json()
        self.assertEqual(len(res_json["data"]), 10)

    def test_num_all_politicians(self):
        res = get_with_retries(f"{API_BASE}/politicians?page=1&per-page=535")
        self.assertEqual(res.status_code, 200)
        res_json = res.json()
        self.assertEqual(len(res_json["data"]), 535)

    def test_politician_count(self):
        res = get_with_retries(f"{API_BASE}/politicians/count")
        self.assertEqual(res.status_code, 200)
        res_json = res.json()
        self.assertEqual(res_json, "535")

    def test_district_from_politician_id(self):
        res = get_with_retries(f"{API_BASE}/politician?id=A000374")
        self.assertEqual(res.status_code, 200)
        res_json = res.json()
        self.assertEqual(res_json["district_id"], 2205)

    def test_twitter_from_politician_id(self):
        res = get_with_retries(f"{API_BASE}/politician?id=B001243")
        self.assertEqual(res.status_code, 200)
        res_json = res.json()
        self.assertEqual(res_json["twitter"], "MarshaBlackburn")


class DistrictTests(TestCase):
    def test_num_districs_per_page(self):
        res = get_with_retries(f"{API_BASE}/districts")
        self.assertEqual(res.status_code, 200)
        res_json = res.json()
        self.assertEqual(len(res_json["data"]), 10)

    def test_num_all_districts(self):
        res = get_with_retries(f"{API_BASE}/districts?page=1&per-page=435")
        self.assertEqual(res.status_code, 200)
        res_json = res.json()
        self.assertEqual(len(res_json["data"]), 435)

    def test_districts_count(self):
        res = get_with_retries(f"{API_BASE}/districts/count")
        self.assertEqual(res.status_code, 200)
        res_json = res.json()
        self.assertEqual(res_json, "435")

    def test_state_from_district_id(self):
        res = get_with_retries(f"{API_BASE}/district?id=5600")
        self.assertEqual(res.status_code, 200)
        res_json = res.json()
        self.assertEqual(res_json["state_id"], 56)

    def test_age_from_district_id(self):
        res = get_with_retries(f"{API_BASE}/district?id=5508")
        self.assertEqual(res.status_code, 200)
        res_json = res.json()
        self.assertEqual(res_json["age"], 40.8)


class BillTests(TestCase):
    def test_num_bills_per_page(self):
        res = get_with_retries(f"{API_BASE}/bills")
        self.assertEqual(res.status_code, 200)
        res_json = res.json()
        self.assertEqual(len(res_json["data"]), 10)

    def test_num_all_bills(self):
        res = get_with_retries(f"{API_BASE}/bills?page=1&per-page=1042")
        self.assertEqual(res.status_code, 200)
        res_json = res.json()
        self.assertEqual(len(res_json["data"]), 1042)

    def test_bills_count(self):
        res = get_with_retries(f"{API_BASE}/bills/count")
        self.assertEqual(res.status_code, 200)
        res_json = res.json()
        self.assertEqual(res_json, "1042")

    def test_bills_politician_by_id(self):
        res = get_with_retries(f"{API_BASE}/bill?id=s3029-115")
        self.assertEqual(res.status_code, 200)
        res_json = res.json()
        self.assertEqual(res_json["politician_id"], "A000360")

    def test_bills_party_by_id(self):
        res = get_with_retries(f"{API_BASE}/bill?id=hr276-116")
        self.assertEqual(res.status_code, 200)
        res_json = res.json()
        self.assertEqual(res_json["party"], "D")


if __name__ == "__main__":
    main()
