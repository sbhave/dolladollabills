import requests
import json

countries = requests.get('https://vaxtracker.me/api/countries').text
result_array = []
countries_dict = json.loads(countries)
for countries in countries_dict:
    result_dict = {}
    result_dict['label'] = countries['name']
    result_dict['value'] = round(float(countries['percent_pop_vacc']),2)
    result_array.append(result_dict)
with open('countriesBubbleData.json', 'w') as fp:
    json.dump(result_array, indent=2, ensure_ascii=False, fp=fp)
