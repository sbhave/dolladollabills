import ReactDOM from "react-dom";
import StateSearch from "../../pages/States/StateSearch";
import { act } from "react-dom/test-utils";
import { unmountComponentAtNode } from "react-dom";
import "@testing-library/jest-dom/extend-expect";
import { BrowserRouter as Router } from "react-router-dom";

it("Test Rendering for state search page", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <Router>
      <StateSearch></StateSearch>
    </Router>,
    div
  );
});
