import React, { useState, useEffect } from "react";
import { Container, Jumbotron } from "react-bootstrap";
import { Tabs, Tab } from "react-bootstrap-tabs";
import BillTable from "../../components/BillTable";
import BillResult from "../../components/BillResult";
import ModelResultsControls from "../../components/ModelResultsControls";
import PaginationControls from "../../components/PaginationControls";
import LoadingSpinner from "../../components/LoadingSpinner";
import {
  BillSortingOptions,
  BillFilteringOptions,
} from "../../data/sortingFilteringOptions";
import { makeAPIRequest } from "../../utilities/APIRequests";
import pageStyles from "../../styles/Pages.module.css";
import "../../styles/Tabs.css";

const Bills = () => {
  const [pageNumber, setPageNumber] = useState(1);
  const [perPage, setPerPage] = useState(20);
  const [currNumBills, setCurrNumBills] = useState(0);
  const [searchParam, setSearchParam] = useState("");
  const [sortOption, setSortOption] = useState("");
  const [filterParams, setFilterParams] = useState(
    Object.fromEntries(
      BillFilteringOptions.map((optionSet) => [optionSet.param, ""])
    )
  );
  const [billData, setBillData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [onSearchTab, setOnSearchTab] = useState(false);

  useEffect(() => {
    async function getBillData() {
      const requestParams = {
        page: pageNumber,
        "per-page": perPage,
        search: searchParam,
        sort: sortOption,
        ...filterParams,
      };
      setLoading(true);
      const { count, data } = await makeAPIRequest("bills", requestParams);
      setBillData(data);
      setCurrNumBills(count);
      setLoading(false);
    }
    getBillData();
  }, [pageNumber, perPage, searchParam, filterParams, sortOption]);

  let numPages =
    currNumBills % perPage === 0
      ? currNumBills / perPage
      : Math.floor(currNumBills / perPage) + 1;
  let pageEnd = Math.min((pageNumber - 1) * perPage + perPage, currNumBills);

  return (
    <div className={pageStyles.leftJustifyText}>
      <Jumbotron fluid={true} className={pageStyles.jumbotronBanner}>
        <h1 className={pageStyles.title}>Congressional Bills</h1>
      </Jumbotron>
      <Container className={pageStyles.core}>
        <br />
        <Tabs
          onSelect={(index, label) => {
            if (label === "View All") {
              setSearchParam("");
              setOnSearchTab(false);
            } else if (label === "Search") {
              setOnSearchTab(true);
            }
          }}
        >
          <Tab label="View All" id="view_all" />
          <Tab label="Search" id="search" />
        </Tabs>
        <br />
        <p className={pageStyles.pageText}>
          Below, you'll find a table of congressional bills in the United
          States. You're currently looking at bills{" "}
          {(pageNumber - 1) * perPage + 1} through {pageEnd} of the{" "}
          {currNumBills} bills in the country that match your current filters.
          Click a table row to learn more about that bill!
        </p>
        <ModelResultsControls
          sortingOptions={BillSortingOptions}
          setSortingOption={setSortOption}
          filteringOptions={BillFilteringOptions}
          updateFilteringOptions={(param, selectedFilterOptions) => {
            let newFilterParams = { ...filterParams };
            newFilterParams[param] = selectedFilterOptions;
            setFilterParams(newFilterParams);
          }}
          showSearchBar={onSearchTab}
          setSearchParam={setSearchParam}
        />
        <br />
        {loading ? (
          <LoadingSpinner />
        ) : onSearchTab ? (
          <React.Fragment>
            <Container>
              {billData?.map((data) => (
                <BillResult key={data.id} data={data} query={searchParam} />
              ))}
            </Container>
            <PaginationControls
              pageNumber={pageNumber}
              numPages={numPages}
              perPage={perPage}
              onSetPage={setPageNumber}
              onSetPerPage={setPerPage}
            />
          </React.Fragment>
        ) : (
          <React.Fragment>
            <BillTable billData={billData} showState={true} />
            <PaginationControls
              pageNumber={pageNumber}
              numPages={numPages}
              perPage={perPage}
              onSetPage={setPageNumber}
              onSetPerPage={setPerPage}
            />
          </React.Fragment>
        )}
        <br />
      </Container>
    </div>
  );
};

export default Bills;
