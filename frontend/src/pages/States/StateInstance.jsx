import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Container, Jumbotron, Row, Col, CardDeck } from "react-bootstrap";
import PoliticianCard from "../../components/PoliticianCard";
import DistrictTable from "../../components/DistrictTable";
import BillTable from "../../components/BillTable";
import LoadingSpinner from "../../components/LoadingSpinner";
import { makeAPIRequest } from "../../utilities/APIRequests";
import pageStyles from "../../styles/Pages.module.css";
import cardStyles from "../../styles/Cards.module.css";
import stateStyles from "../../styles/States.module.css";

function StateInstance() {
  const [stateInfo, setStateInfo] = useState(null);
  const { id } = useParams();

  useEffect(() => {
    async function getStateInfo() {
      const requestParams = { id: id };
      const response = await makeAPIRequest("state", requestParams);
      setStateInfo(response);
    }
    getStateInfo();
  }, [id]);

  function FlagAndPolitics() {
    return (
      <React.Fragment>
        <img
          className={stateStyles.picture}
          src={stateInfo.flag_path}
          alt="not available"
        />
        <div className={"pl-3 pr-3 " + pageStyles.pageText}>
          <p className="mb-2">
            {stateInfo.name} is a primarily{" "}
            {stateInfo.party === "D"
              ? "democratic"
              : stateInfo.party === "R"
              ? "republican"
              : "independent"}{" "}
            state.
          </p>
          <p className="mb-0">
            The governor of {stateInfo.name} is {stateInfo.governor}.
          </p>
        </div>
      </React.Fragment>
    );
  }

  function MapAndPopulation() {
    return (
      <React.Fragment>
        <img
          className={stateStyles.picture}
          src={stateInfo.picture_path}
          alt="not available"
        />
        <div className={"pl-3 pr-3 " + pageStyles.pageText}>
          <p className="mb-2">
            Located in the {stateInfo.region} region of the United States,{" "}
            {stateInfo.name} has a total land area of{" "}
            {stateInfo.land_area.toLocaleString()} square miles.
          </p>
          <p className="mb-0">
            {stateInfo.name} is home to {stateInfo.population.toLocaleString()}{" "}
            people.
          </p>
        </div>
      </React.Fragment>
    );
  }

  function StateBird() {
    const { name, path } = JSON.parse(stateInfo.bird);
    return (
      <React.Fragment>
        <h3 className={"mb-2 " + pageStyles.bigText}>{name}</h3>
        <img className={stateStyles.picture} src={path} alt="not available" />
      </React.Fragment>
    );
  }

  function StateFlower() {
    const { name, path } = JSON.parse(stateInfo.flower);
    return (
      <React.Fragment>
        <h3 className={"mb-2 " + pageStyles.bigText}>{name}</h3>
        <img className={stateStyles.picture} src={path} alt="not available" />
      </React.Fragment>
    );
  }

  if (stateInfo === null) {
    return (
      <div className={pageStyles.centerAll}>
        <Jumbotron fluid={true} className={pageStyles.jumbotronBanner}>
          <h1 className={pageStyles.title}>Loading...</h1>
        </Jumbotron>
        <Container className={pageStyles.core}>
          <br />
          <LoadingSpinner />
          <br />
        </Container>
      </div>
    );
  } else {
    return (
      <div className={pageStyles.centerAll}>
        <Jumbotron fluid={true} className={pageStyles.jumbotronBanner}>
          <h1 className={"mb-0 " + pageStyles.title}>
            All about {stateInfo.name}
          </h1>
          <h3 className={"mb-0 " + pageStyles.bigText}>
            (a.k.a: {stateInfo.abbreviation})
          </h3>
          <h3 className={"mb-0 " + pageStyles.note}>"{stateInfo.motto}"</h3>
        </Jumbotron>
        <Container className={pageStyles.core}>
          <br />
          <h1 className={pageStyles.subtitle}>Facts about {stateInfo.name}</h1>
          <br />
          <div>
            <Row>
              <Col>
                <FlagAndPolitics />
              </Col>
              <Col>
                <MapAndPopulation />
              </Col>
            </Row>
          </div>
          <br />
          <div>
            <h1 className={pageStyles.subtitle}>
              {stateInfo.name}'s State Bird and Flower
            </h1>
            <br />
            <Row>
              <Col>
                <StateBird />
              </Col>
              <Col>
                <StateFlower />
              </Col>
            </Row>
          </div>
          <br />
          <div>
            <h1 className={pageStyles.subtitle}>{stateInfo.name}'s Senators</h1>
            <br />
            <CardDeck className={cardStyles.cardDeck}>
              {stateInfo.politicians
                .filter((politician) => politician.is_senator === true)
                .map((politician) => (
                  <PoliticianCard
                    key={politician.id}
                    data={politician}
                    stateName={stateInfo.name}
                  />
                ))}
            </CardDeck>
          </div>
          <br />
          <div>
            <h1 className={pageStyles.subtitle}>
              {stateInfo.name}'s House Representatives
            </h1>
            <br />
            <CardDeck className={cardStyles.cardDeck}>
              {stateInfo.politicians
                .filter((politician) => politician.is_senator === false)
                .map((politician) => (
                  <PoliticianCard
                    key={politician.id}
                    data={politician}
                    stateName={stateInfo.name}
                  />
                ))}
            </CardDeck>
          </div>
          <br />
          <div>
            <h1 className={pageStyles.subtitle}>
              Bills Associated with {stateInfo.name}
            </h1>
            <br />
            <div>
              <BillTable billData={stateInfo.bills} showState={false} />
            </div>
          </div>
          <br />
          <div>
            <h1 className={pageStyles.subtitle}>
              {stateInfo.name}'s Congressional Districts
            </h1>
            <br />
            <DistrictTable data={stateInfo.districts} showState={false} />
          </div>
          <br />
        </Container>
      </div>
    );
  }
}

export default StateInstance;
