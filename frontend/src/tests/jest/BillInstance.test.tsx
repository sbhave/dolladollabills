import ReactDOM from "react-dom";
import BillInstance from "../../pages/Bills/BillInstance";
import { act } from "react-dom/test-utils";
import { unmountComponentAtNode } from "react-dom";
import "@testing-library/jest-dom/extend-expect";
import { BrowserRouter as Router } from "react-router-dom";

it("Test Rendering for bill instance", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <Router>
      <BillInstance></BillInstance>
    </Router>,
    div
  );
});
