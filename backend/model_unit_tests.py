import os

os.environ["SQLALCHEMY_DATABASE_URI"] = "sqlite:///mock.db"

import make_mock_db
from app import (
    app,
    db,
    State,
    District,
    Politician,
    Bill,
    model_id,
    model_count,
    model_amount_per_page,
)

from unittest import main, TestCase

import json


class ModelIdTests(TestCase):
    def test_state(self):
        res = model_id(State, "1").json
        self.assertEqual(res["id"], 1)
        self.assertEqual(res["name"], "State One")

    def test_district(self):
        res = model_id(District, "11").json
        self.assertEqual(res["id"], 11)
        self.assertEqual(res["name"], "District One S1")
        self.assertEqual(res["cities"][1:-1].split(","), ["City1S1", "City2S1"])

    def test_politician(self):
        res = model_id(Politician, "polit12").json
        self.assertEqual(res["id"], "polit12")
        self.assertEqual(res["state_id"], 2)
        self.assertEqual(res["district_id"], 12)
        self.assertEqual(res["leadership"], "None")

    def test_bill(self):
        res = model_id(Bill, "bill1").json
        self.assertEqual(res["id"], "bill1")
        self.assertEqual(res["state_id"], 1)
        self.assertEqual(res["politician_id"], "polit11")

    def test_invalid_id(self):
        res = model_id(State, 100)[0].json
        self.assertEqual(res, "Could Not Find Entry in Model for Given ID 100")


class ModelAmountPerPageTest(TestCase):
    politicians = db.session.query(Politician).order_by(Politician.id).all()
    districts = db.session.query(District).order_by(District.id).all()
    states = db.session.query(State).order_by(State.id).all()

    def test_first_page(self):
        res = model_amount_per_page(Politician, "1", "2", self.politicians).json
        self.assertEqual(res["count"], 5)
        ids = {r["id"] for r in res["data"]}
        self.assertEqual(ids, {"polit01", "polit02"})

    def test_general_page(self):
        res = model_amount_per_page(Politician, "2", "2", self.politicians).json
        self.assertEqual(res["count"], 5)
        ids = {r["id"] for r in res["data"]}
        self.assertEqual(ids, {"polit11", "polit12"})

    def test_large_page(self):
        res = model_amount_per_page(State, "1", "10", self.states).json
        self.assertEqual(res["count"], 2)

    def test_out_of_bounds(self):
        res = model_amount_per_page(District, "10", "10", self.districts)[0].json
        self.assertEqual(res, "Request Out of Bounds for Model's Entries")


if __name__ == "__main__":
    with app.app_context():
        main()
