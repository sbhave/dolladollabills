import React from "react";
import { withRouter } from "react-router-dom";
import { Table } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/Tables.css";

const billTypeToString = {
  hr: "House Bill",
  hjres: "House Joint Resolution",
  hconres: "House Concurrent Resolution",
  s: "Senate Bill",
  sjres: "Senate Joint Resolution",
  sconres: "Senate Concurrent Resolution",
};

class BillTable extends React.Component {
  render() {
    const widths = {
      billNumber: this.props.showState ? "15%" : "20%",
      party: this.props.showState ? "15%" : "20%",
      state: this.props.showState ? "15%" : "0%",
      chamber: this.props.showState ? "20%" : "20%",
      type: this.props.showState ? "25%" : "30%",
      year: this.props.showState ? "10%" : "10%",
    };

    return (
      <React.Fragment>
        <Table striped="true" bordered="true" hover="true">
          <thead>
            <tr>
              <th style={{ width: widths.billNumber }}>Bill Number</th>
              <th style={{ width: widths.party }}>Party</th>
              {this.props.showState ? (
                <th style={{ width: widths.state }}>State</th>
              ) : null}
              <th style={{ width: widths.chamber }}>Chamber of Congress</th>
              <th style={{ width: widths.type }}>Bill Type</th>
              <th style={{ width: widths.year }}>Year</th>
            </tr>
          </thead>
          <tbody>
            {this.props.billData.map((bill) => {
              return (
                <tr
                  key={bill.id}
                  onClick={() => {
                    this.props.history.push("/bills/" + bill.id);
                  }}
                >
                  <td>{bill.number}</td>
                  <td>
                    {bill.party === "D"
                      ? "Democrat"
                      : bill.party === "R"
                      ? "Republican"
                      : "Independent"}
                  </td>
                  {this.props.showState ? <td>{bill.state_name}</td> : null}
                  <td>{bill.bill_type[0] === "h" ? "House" : "Senate"}</td>
                  <td>{billTypeToString[bill.bill_type]}</td>
                  <td>{bill.year}</td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </React.Fragment>
    );
  }
}

export default withRouter(BillTable);
