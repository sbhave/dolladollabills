export const PoliticianSortingOptions = [
  {
    name: "Politician",
    param: "politician",
    options: [
      { value: "alpha-inc", label: "Name (a-z)" },
      { value: "alpha-dec", label: "Name (z-a)" },
      { value: "age-inc", label: "Age (increasing)" },
      { value: "age-dec", label: "Age (decreasing)" },
      { value: "f", label: "Gender (female first)" },
      { value: "m", label: "Gender (male first)" },
      { value: "d", label: "Party (democrat first)" },
      { value: "r", label: "Party (republican first)" },
      { value: "", label: "Default" },
    ],
  },
];

export const BillSortingOptions = [
  {
    name: "Bill",
    param: "bill",
    options: [
      { value: "state-inc", label: "State (increasing)" },
      { value: "state-dec", label: "State (decreasing)" },
      { value: "year-inc", label: "Year (increasing)" },
      { value: "year-dec", label: "Year (decreasing)" },
      { value: "", label: "Default" },
    ],
  },
  // { name: "Bill Subject", param: "subject" },
];

export const DistrictSortingOptions = [
  {
    name: "District",
    param: "district",
    options: [
      { value: "state-inc", label: "State (increasing)" },
      { value: "state-dec", label: "State (decreasing)" },
      { value: "age-inc", label: "Age (increasing)" },
      { value: "age-dec", label: "Age (decreasing)" },
      { value: "income-inc", label: "Income (increasing)" },
      { value: "income-dec", label: "Income (decreasing)" },
      { value: "pop-inc", label: "Population (increasing)" },
      { value: "pop-dec", label: "Population decreasing)" },
      { value: "", label: "Default" },
    ],
  },
];

export const StateSortingOptions = [
  {
    name: "State",
    param: "state",
    options: [
      { value: "alpha-inc", label: "Name (a-z)" },
      { value: "alpha-dec", label: "Name (z-a)" },
      { value: "pop-inc", label: "Population (increasing)" },
      { value: "pop-dec", label: "Population (decreasing)" },
      { value: "land-inc", label: "Land Area (increasing)" },
      { value: "land-dec", label: "Land Area (decreasing)" },
      {
        value: "rep-inc",
        label: "Number of Representatives (increasing)",
      },
      {
        value: "rep-dec",
        label: "Number of Representatives (decreasing)",
      },
      { value: "", label: "Default" },
    ],
  },
];

const StateOptions = {
  name: "State",
  param: "state",
  options: [
    { value: "Alabama", label: "Alabama" },
    { value: "Alaska", label: "Alaska" },
    { value: "Arizona", label: "Arizona" },
    { value: "Arkansas", label: "Arkansas" },
    { value: "California", label: "California" },
    { value: "Colorado", label: "Colorado" },
    { value: "Connecticut", label: "Connecticut" },
    { value: "Delaware", label: "Delaware" },
    { value: "Florida", label: "Florida" },
    { value: "Georgia", label: "Georgia" },
    { value: "Hawaii", label: "Hawaii" },
    { value: "Idaho", label: "Idaho" },
    { value: "Illinois", label: "Illinois" },
    { value: "Indiana", label: "Indiana" },
    { value: "Iowa", label: "Iowa" },
    { value: "Kansas", label: "Kansas" },
    { value: "Kentucky", label: "Kentucky" },
    { value: "Louisiana", label: "Louisiana" },
    { value: "Maine", label: "Maine" },
    { value: "Maryland", label: "Maryland" },
    { value: "Massachusetts", label: "Massachusetts" },
    { value: "Michigan", label: "Michigan" },
    { value: "Minnesota", label: "Minnesota" },
    { value: "Mississippi", label: "Mississippi" },
    { value: "Missouri", label: "Missouri" },
    { value: "Montana", label: "Montana" },
    { value: "Nebraska", label: "Nebraska" },
    { value: "Nevada", label: "Nevada" },
    { value: "New Hampshire", label: "Hampshire" },
    { value: "New Jersey", label: "New Jersey" },
    { value: "New Mexico", label: "New Mexico" },
    { value: "New York", label: "New York" },
    { value: "North Carolina", label: "North Carolina" },
    { value: "North Dakota", label: "North Dakota" },
    { value: "Ohio", label: "Ohio" },
    { value: "Oklahoma", label: "Oklahoma" },
    { value: "Oregon", label: "Oregon" },
    { value: "Pennsylvania", label: "Pennsylvania" },
    { value: "Rhode Island", label: "Rhode Island" },
    { value: "South Carolina", label: "South Carolina" },
    { value: "South Dakota", label: "South Dakota" },
    { value: "Tennessee", label: "Tennessee" },
    { value: "Texas", label: "Texas" },
    { value: "Utah", label: "Utah" },
    { value: "Vermont", label: "Vermont" },
    { value: "Virginia", label: "Virginia" },
    { value: "Washington", label: "Washington" },
    { value: "West Virginia", label: "West Virginia" },
    { value: "Wisconsin", label: "Wisconsin" },
    { value: "Wyoming", label: "Wyoming" },
  ],
};

const PartyOptions = {
  name: "Political Party",
  param: "party",
  options: [
    { value: "D", label: "Democrat" },
    { value: "R", label: "Republican" },
    { value: "I", label: "Independent" },
  ],
};

export const PoliticianFilteringOptions = [
  { ...PartyOptions },
  {
    name: "Chamber",
    param: "is_senator",
    options: [
      { value: "False", label: "House Representative" },
      { value: "True", label: "Senator" },
    ],
  },
  { ...StateOptions },
  {
    name: "Age Range",
    param: "age",
    options: [
      { value: "0,29", label: "Under 30" },
      { value: "30,39", label: "30-39" },
      { value: "40,49", label: "40-49" },
      { value: "50,59", label: "50-59" },
      { value: "60,69", label: "60-69" },
      { value: "70,79", label: "70-79" },
      { value: "80,100", label: "Over 80" },
    ],
  },
  {
    name: "Gender",
    param: "gender",
    options: [
      { value: "M", label: "Male" },
      { value: "F", label: "Female" },
    ],
  },
];

export const BillFilteringOptions = [
  { ...PartyOptions },
  { ...StateOptions },
  {
    name: "Chamber of Congress",
    param: "chamber",
    options: [
      { value: "house", label: "House" },
      { value: "senate", label: "Senate" },
    ],
  },
  {
    name: "Bill Type",
    param: "bill_type",
    options: [
      { value: "hr", label: "House Bill" },
      { value: "hjres", label: "House Joint Resolution" },
      { value: "hconres", label: "House Concurrent Resolution" },
      { value: "s", label: "Senate Bill" },
      { value: "sjres", label: "Senate Joint Resolution" },
      { value: "sconres", label: "Senate Concurrent Resolution" },
    ],
  },
  {
    name: "Year",
    param: "year",
    options: [
      { value: "2009", label: "2009" },
      { value: "2010", label: "2010" },
      { value: "2011", label: "2011" },
      { value: "2012", label: "2012" },
      { value: "2013", label: "2013" },
      { value: "2014", label: "2014" },
      { value: "2015", label: "2015" },
      { value: "2016", label: "2016" },
      { value: "2017", label: "2017" },
      { value: "2018", label: "2018" },
      { value: "2019", label: "2019" },
      { value: "2020", label: "2020" },
      { value: "2021", label: "2021" },
    ],
  },
  // { name: "Bill Subject", param: "subject" },
];

export const DistrictFilteringOptions = [
  { ...StateOptions },
  { ...PartyOptions },
  {
    name: "Population",
    param: "population",
    options: [
      { value: "0,499999", label: "Under 500,000" },
      { value: "500000,549999", label: "500,000-549,999" },
      { value: "550000,599999", label: "550,000-599,999" },
      { value: "600000,649999", label: "600,000-649,999" },
      { value: "650000,699999", label: "650,000-699,999" },
      { value: "700000,749999", label: "700,000-749,999" },
      { value: "750000,799999", label: "750,000-799,999" },
      { value: "800000,849999", label: "800,000-849,999" },
      { value: "850000,899999", label: "850,000-899,999" },
      { value: "900000,949999", label: "900,000-949,999" },
      { value: "950000,999999", label: "950,000-999,999" },
      { value: "1000000,5000000", label: "Over 1,000,000" },
    ],
  },
  {
    name: "Average Age",
    param: "age",
    options: [
      { value: "0,9", label: "Under 10" },
      { value: "10,19", label: "10-19" },
      { value: "20,29", label: "20-29" },
      { value: "30,39", label: "30-39" },
      { value: "40,49", label: "40-49" },
      { value: "50,59", label: "50-59" },
      { value: "60,69", label: "60-69" },
      { value: "70,79", label: "70-79" },
      { value: "80,100", label: "Over 80" },
    ],
  },
  {
    name: "Median HH Income",
    param: "income",
    options: [
      { value: "0,9999", label: "Under $10,000" },
      { value: "10000,19999", label: "$10,000-$19,999" },
      { value: "20000,29999", label: "$20,000-$29,999" },
      { value: "30000,39999", label: "$30,000-$39,999" },
      { value: "40000,49999", label: "$40,000-$49,999" },
      { value: "50000,59999", label: "$50,000-$59,999" },
      { value: "60000,69999", label: "$60,000-$69,999" },
      { value: "70000,79999", label: "$70,000-$79,999" },
      { value: "80000,89999", label: "$80,000-$89,999" },
      { value: "90000,99999", label: "$90,000-$99,999" },
      { value: "100000,109999", label: "$100,000-$109,999" },
      { value: "110000,119999", label: "$110,000-$119,999" },
      { value: "120000,129999", label: "$120,000-$129,999" },
      { value: "130000,139999", label: "$130,000-$139,999" },
      { value: "140000,149999", label: "$140,000-$149,999" },
      { value: "150000,159999", label: "$150,000-$159,999" },
      { value: "160000,169999", label: "$160,000-$169,999" },
      { value: "170000,179999", label: "$170,000-$179,999" },
      { value: "180000,189999", label: "$180,000-$189,999" },
      { value: "190000,199999", label: "$190,000-$199,999" },
      { value: "200000,1000000", label: "Over $200,000" },
    ],
  },
];

export const StateFilteringOptions = [
  { ...PartyOptions },
  {
    name: "House Reps.",
    param: "num_reps",
    options: [
      { value: "0,9", label: "Under 10" },
      { value: "10,19", label: "10-19" },
      { value: "20,29", label: "20-29" },
      { value: "30,39", label: "30-39" },
      { value: "40,49", label: "40-49" },
      { value: "50,100", label: "Over 50" },
    ],
  },
  {
    name: "Population",
    param: "population",
    options: [
      { value: "0,599999", label: "Under 600,000" },
      { value: "600000,649999", label: "600,000-649,999" },
      { value: "650000,699999", label: "650,000-699,999" },
      { value: "700000,749999", label: "700,000-749,999" },
      { value: "750000,799999", label: "750,000-799,999" },
      { value: "800000,849999", label: "800,000-849,999" },
      { value: "850000,899999", label: "850,000-899,999" },
      { value: "900000,949999", label: "900,000-949,999" },
      { value: "950000,999999", label: "950,000-999,999" },
      { value: "1000000,1999999", label: "1,000,000-1,999,999" },
      { value: "2000000,2999999", label: "2,000,000-2,999,999" },
      { value: "3000000,3999999", label: "3,000,000-3,999,999" },
      { value: "4000000,4999999", label: "4,000,000-4,999,999" },
      { value: "5000000,9999999", label: "5,000,000-9,999,999" },
      { value: "10000000,14999999", label: "10,000,000-14,999,999" },
      { value: "15000000,19999999", label: "15,000,000-19,999,999" },
      { value: "20000000,24999999", label: "20,000,000-24,999,999" },
      { value: "25000000,29999999", label: "25,000,000-29,999,999" },
      { value: "30000000,34999999", label: "30,000,000-34,999,999" },
      { value: "35000000,50000000", label: "Over 35,000,000" },
    ],
  },
  {
    name: "Land Area",
    param: "land_area",
    options: [
      { value: "0,49999", label: "Under 50,000 sq mi" },
      { value: "50000,99999", label: "50,000-99,999 sq mi" },
      { value: "100000,149999", label: "100,000-149,999 sq mi" },
      { value: "150000,199999", label: "150,000-199,999 sq mi" },
      { value: "200000,1000000", label: "Over 200,000 sq mi" },
    ],
  },
  {
    name: "Region",
    param: "region",
    options: [
      { value: "New England", label: "New England" },
      { value: "Mid-Atlantic", label: "Mid-Atlantic" },
      { value: "East North Central", label: "East North Central" },
      { value: "West North Central", label: "West North Central" },
      { value: "South Atlantic", label: "South Atlantic" },
      { value: "East South Central", label: "East South Central" },
      { value: "West South Central", label: "West South Central" },
      { value: "Mountain", label: "Mountain" },
      { value: "Pacific", label: "Pacific" },
    ],
  },
];

export const CustomSelectStyles = {
  menu: (provided, state) => ({
    ...provided,
    // a z-index of 4, specifically will make sure the pagination component
    // isn't on top of the Select component's dropdown menu
    zIndex: 4,
  }),
  option: (provided, state) => ({
    ...provided,
    borderBottom: "2px dotted green",
    color: state.isSelected ? "white" : "black",
    backgroundColor: state.isSelected
      ? "green"
      : state.isFocused
      ? "lightGray"
      : "white",
  }),
  control: (provided) => ({
    ...provided,
    marginTop: "2%",
    marginLeft: "auto",
    marginRight: "auto",
  }),
  multiValue: (provided) => ({
    ...provided,
  }),
};
