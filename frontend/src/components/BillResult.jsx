import React from "react";
import { withRouter } from "react-router-dom";
import { Card } from "react-bootstrap";
import cardStyles from "../styles/Cards.module.css";
import Highlighter from "react-highlight-words";

class BillResult extends React.Component {
  MAX_SUMMARY_LENGTH = 500;

  render() {
    let {
      name,
      number,
      subject,
      summary,
      year,
      sponsor,
      bill_type_str,
      chamber,
      party_str,
      state_name,
    } = this.props.data;
    if (summary.length > this.MAX_SUMMARY_LENGTH) {
      summary = summary.substr(0, this.MAX_SUMMARY_LENGTH) + "...";
    }
    const searchWords = this.props.query.split(" ");

    return (
      <React.Fragment>
        <Card
          className="mb-3"
          onClick={() => {
            this.props.history.push("/bills/" + this.props.data.id);
          }}
        >
          <Card.Body>
            <Card.Title>
              <Highlighter
                unhighlightClassName={cardStyles.cardTitle}
                highlightClassName={cardStyles.cardTitleHighlight}
                searchWords={searchWords}
                textToHighlight={number}
              />
            </Card.Title>
            <Card.Text>
              <b>Name: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={name}
              />
            </Card.Text>
            <Card.Text>
              <b>Chamber: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={chamber}
              />
            </Card.Text>
            <Card.Text>
              <b>Type: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={bill_type_str}
              />
            </Card.Text>
            <Card.Text>
              <b>Subject: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={subject}
              />
            </Card.Text>
            <Card.Text>
              <b>Sponsor: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={sponsor}
              />
            </Card.Text>
            <Card.Text>
              <b>Party: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={party_str}
              />
            </Card.Text>
            <Card.Text>
              <b>State: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={state_name}
              />
            </Card.Text>
            <Card.Text>
              <b>Year: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={String(year)}
              />
            </Card.Text>
            <Card.Text>
              <b>Summary: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={summary}
              />
            </Card.Text>
          </Card.Body>
        </Card>
      </React.Fragment>
    );
  }
}

export default withRouter(BillResult);
