import argparse
import os

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  os_group = parser.add_mutually_exclusive_group()
  os_group.add_argument("-m", "--mac", help="Mac", action="store_true")
  os_group.add_argument("-w", "--windows", help="Windows", action="store_true")
  os_group.add_argument("-l", "--linux", help="Linux", action="store_true")
  args = parser.parse_args()

  CURRENT_DIR = os.path.dirname(__file__)
  MAC_EXE = os.path.join(CURRENT_DIR, "chromedriver_mac")
  WINDOWS_EXE = os.path.join(CURRENT_DIR, "chromedriver_windows.exe")
  LINUX_EXE = os.path.join(CURRENT_DIR, "chromedriver_linux")

  SPLASH_TESTS = os.path.join(CURRENT_DIR, "splash_tests.py")
  ABOUT_TESTS = os.path.join(CURRENT_DIR, "about_tests.py")
  PAGINATION_TESTS = os.path.join(CURRENT_DIR, "pagination_tests.py")
  FILTERING_TESTS = os.path.join(CURRENT_DIR, "filtering_tests.py")
  SORTING_TESTS = os.path.join(CURRENT_DIR, "sorting_tests.py")

  browser_executable = MAC_EXE if args.mac else WINDOWS_EXE if args.windows else LINUX_EXE
  print("Using", browser_executable)
  print()

  # run all GUI tests
  print("====SPLASH TESTS====")
  os.system("python3 " + SPLASH_TESTS + " " + browser_executable)
  print()
  print("====ABOUT TESTS====")
  os.system("python3 " + ABOUT_TESTS + " " + browser_executable)
  print()
  print("====PAGINATION TESTS====")
  os.system("python3 " + PAGINATION_TESTS + " " + browser_executable)
  print()
  print("====FILTERING TESTS====")
  os.system("python3 " + FILTERING_TESTS + " " + browser_executable)
  print()
  print("====SORTING TESTS====")
  os.system("python3 " + SORTING_TESTS + " " + browser_executable)
  print()
