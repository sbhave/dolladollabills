import ReactDOM from "react-dom";
import About from "../../pages/About/About";
import { act } from "react-dom/test-utils";
import { unmountComponentAtNode } from "react-dom";
import "@testing-library/jest-dom/extend-expect";

it("Test Rendering for about page", () => {
  const div = document.createElement("div");
  ReactDOM.render(<About></About>, div);
});
