import React from "react";
import FadeLoader from "react-spinners/FadeLoader";
import pageStyles from "../styles/Pages.module.css";

const songLyrics = [
  {
    quote: '"Money trees is the perfect place for shade"',
    artist: "Kendrick Lamar",
  },
  { quote: '"Cash rules everything around me"', artist: "Wu-Tang Clan" },
  { quote: '"Dollar dollar bill, y\'all"', artist: "Wu-Tang Clan" },
  { quote: "\"Mo' money, mo' problems\"", artist: "Biggie Smalls" },
];

function LoadingSpinner() {
  const lyricIndex = Math.floor(Math.random() * songLyrics.length);
  return (
    <div>
      <FadeLoader
        color="#153615"
        loading={true}
        size={150}
        css={{ display: "block", marginLeft: "auto", marginRight: "auto" }}
      />
      <br />
      <h3 className={pageStyles.bigText + " ml-auto mr-auto"}>
        {songLyrics[lyricIndex].quote}
      </h3>
      <h3 className={pageStyles.bigText + " ml-auto mr-auto"}>
        - {songLyrics[lyricIndex].artist}
      </h3>
    </div>
  );
}

export default LoadingSpinner;
