from models import app, db, Politician, Bill, District, State
from Politician import query_politician
from Bill import query_bill
from District import query_district
from State import query_state
from flask import request, jsonify


# ---------- Policitians ----------

# This endpoint returns a set of Politicians given the page number
# the amount per page needed
# The default page number is 1
# The default amount per page is 10
# FORMAT("..../api/politicians?page=<Int>&per-page=<Int>")
# FORMAT for sorting age :  ("...api/politicians?sort=<age-inc or age-dec>")
#        last name alpha :  ("...api/politicians?sort=<alpha-inc or alpha-dec>")
#                 gender :  ("...api/politicians?sort=<f or m>")
#                 party  :  ("...api/politicians?sort=<r or d>")
# note : sorting is for 1 attribute at a time

# Filtering Parameters for Politicians:
#   party       Example: api/politicians?party=D
# 	is_senator  Example: api/politicians?is_senator=True
# 	gender      Example: api/politicians?gender=F
# 	age         Example: api/politicians?age=40,50
# 	state       Example: api/politicians?state=Texas

# FORMAT for search      : ("...api/politicians?search=<query>")
@app.route("/api/politicians", methods=["GET"])
def politicians():
    queries = request.args.to_dict(flat=False)
    politician_list = query_politician(queries)

    page_param = request.args.get("page")
    amount_param = request.args.get("per-page")
    return model_amount_per_page(Politician, page_param, amount_param, politician_list)


# This endpoint returns the amount of individual politicians in the database collection
# FORMAT("..../api/politicians/count")
@app.route("/api/politicians/count", methods=["GET"])
def politicians_count():
    return model_count(Politician)


# This endpoint returns a single Politician Entry given a correct ID
# FORMAT("..../api/politician?id=<String id within database collection>")
@app.route("/api/politician", methods=["GET"])
def politician_id():
    query = request.args.get("id")
    return model_id(Politician, query)


# ---------- Bills ----------

# This endpoint returns a set of Bills given the page number
# the amount per page needed
# The default page number is 1
# The default amount per page is 10
# FORMAT("..../api/bills?page=<Int>&per-page=<Int>")
# FORMAT for sorting      year :  ("...api/bills?sort=<year-inc or year-dec>")
#                 senate/house :  ("...api/bills?sort=<senate-inc or house>")
# note : sorting is for 1 attribute at a time

# Filtering Parameters for Bills:
#   party       Example: api/bills?party=D
# 	chamber     Example: api/bills?chamber=house
# 	bill_tpe    Example: api/bills?bill_type=hr
# 	year        Example: api/bills?age=2019
# 	state       Example: api/bills?state=Texas
# 	subject     Example: api/bills?subject=Law


@app.route("/api/bills", methods=["GET"])
def bills():
    queries = request.args.to_dict(flat=False)
    bill_list = query_bill(queries)

    page_param = request.args.get("page")
    amount_param = request.args.get("per-page")
    return model_amount_per_page(Bill, page_param, amount_param, bill_list)


# This endpoint returns the amount of individual bills in the database collection
# FORMAT("..../api/bills/count")
@app.route("/api/bills/count", methods=["GET"])
def bills_count():
    return model_count(Bill)


# This endpoint returns a single Bill Entry given a correct ID
# FORMAT("..../api/bill?id=<String id within database collection>")
@app.route("/api/bill", methods=["GET"])
def bill_id():
    query = request.args.get("id")
    return model_id(Bill, query)


# ---------- Districts ----------

# This endpoint returns a set of Districts given the page number
# the amount per page needed
# The default page number is 1
# The default amount per page is 10
# FORMAT("..../api/districts?page=<Int>&per-page=<Int>")
# FORMAT for sorting population :  ("...api/districts?sort=<pop-inc or pop-dec>")
#                           age :  ("...api/districts?sort=<age-inc or age-dec>")
#                        income :  ("...api/districts?sort=<income_inc or income-dec>")
# note : sorting is for 1 attribute at a time

# Filtering Parameters for Districts:
#   party       Example: api/districts?party=D
# 	population  Example: api/districts?population=500000,800000
# 	income      Example: api/districts?income=30000,40000
# 	age         Example: api/districts?age=50,60
# 	state       Example: api/districts?state=Texas


@app.route("/api/districts", methods=["GET"])
def districts():
    queries = request.args.to_dict(flat=False)
    district_list = query_district(queries)

    page_param = request.args.get("page")
    amount_param = request.args.get("per-page")
    return model_amount_per_page(District, page_param, amount_param, district_list)


# This endpoint returns the amount of individual districts in the database collection
# FORMAT("..../api/districts/count")
@app.route("/api/districts/count", methods=["GET"])
def districts_count():
    return model_count(District)


# This endpoint returns a single District Entry given a correct ID
# FORMAT("..../api/district?id=<Int id within database collection>")
@app.route("/api/district", methods=["GET"])
def district_id():
    query = request.args.get("id")
    return model_id(District, query)


# ---------- States ----------

# This endpoint returns a set of States given the page number the amount per page needed
# The default page number is 1
# The default amount per page is 10
# FORMAT("..../api/states?page=<Int>&per-page=<Int>")
# FORMAT for sorting      alpha :  ("...api/states?sort=<alpha-inc or alpha-dec>")
#                    population :  ("...api/states?sort=<pop-inc or pop-dec>")
#                     land area :  ("...api/states?sort=<land_inc or land-dec>")
#                     num reps  :  ("...api/states?sort=<rep-inc or rep-dec>")
# note : sorting is for 1 attribute at a time

# Filtering Parameters for States:
#   party       Example: api/states?party=D
# 	population  Example: api/states?population=600000,650000
# 	region      Example: api/states?region=Pacific
# 	land_area   Example: api/states?land_area=70000,85000
# 	num_reps    Example: api/states?num_reps=10,20


@app.route("/api/states", methods=["GET"])
def states():
    queries = request.args.to_dict(flat=False)
    state_list = query_state(queries)

    page_param = request.args.get("page")
    amount_param = request.args.get("per-page")
    return model_amount_per_page(State, page_param, amount_param, state_list)


# This endpoint returns the amount of individual states in the database collection
# FORMAT("..../api/states/count")
@app.route("/api/states/count", methods=["GET"])
def states_count():
    return model_count(State)


# This endpoint returns a single State Entry given a correct ID
# FORMAT("..../api/state?id=<Int id within database collection>")
@app.route("/api/state", methods=["GET"])
def state_id():
    query = request.args.get("id")
    return model_id(State, query)


# ----------Helper Functions ----------

MAX_RETRIES = 3
INT_KEY_MODELS = {State, District}


def model_id(model, query):
    retries = 0
    while True:
        try:
            if query != None:
                res = ""
                if model in INT_KEY_MODELS:
                    res = str(db.session.query(model).get(int(query)))
                else:
                    res = str(db.session.query(model).get(query))
                if res != "None":
                    d = eval(res)
                    return jsonify(d)
                else:
                    return (
                        jsonify(
                            "Could Not Find Entry in Model for Given ID {}".format(
                                query
                            )
                        ),
                        251,
                    )
            else:
                return jsonify("Please use a valid Id for this model"), 250
        except Exception as e:
            if retries == MAX_RETRIES:
                return jsonify("model_id threw " + repr(e)), 252
            db.session.rollback()
            retries += 1


def model_count(model):
    return jsonify(str(db.session.query(model).count()))


def model_amount_per_page(model, page_param, amount_param, model_list):
    # Sets default values to parameters if they were not given
    page = 1
    per_page = 10

    retries = 0
    while True:
        try:
            # conditionals to parse given parameters into integers
            if page_param != None:
                page = int(page_param)
                page = abs(page)
            if amount_param != None:
                per_page = int(amount_param)
                per_page = abs(per_page)

            if page == 0:
                page += 1
            if per_page == 0:
                per_page += 1

            # variables to grab correct range of values inside database collection
            count = 0
            end = page * per_page
            start = end - per_page
            result = []

            # check page out of bounds
            num_models = len(model_list)
            if num_models > 0 and start >= num_models:
                return jsonify("Request Out of Bounds for Model's Entries"), 250

            for x in model_list:
                if start <= count < end:
                    s = str(x)
                    d = eval(s)
                    result.append(d)
                count += 1
            # result.append({"count": len(model_list)})
            complete = {"count": len(model_list), "data": result}
            return jsonify(complete)
        except Exception as e:
            if retries == MAX_RETRIES:
                return jsonify("model_amount_per_page threw " + repr(e)), 252
            db.session.rollback()
            retries += 1


if __name__ == "__main__":
    app.run(port=8080)
