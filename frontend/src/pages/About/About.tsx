import { useState } from "react";
import { memberData } from "../../data/MembersData";
import { toolData } from "../../data/ToolData";
import { apiData } from "../../data/APIData";
import axios from "axios";
import { CardDeck, Card, Container, Jumbotron } from "react-bootstrap";
import ReactPlayer from "react-player";
import MemberCard, { MemberData } from "../../components/MemberCard";
import ResourceCard from "../../components/ResourceCard";
import pageStyles from "../../styles/Pages.module.css";
import cardStyles from "../../styles/Cards.module.css";
import "bootstrap/dist/css/bootstrap.css";

export default function About() {
  const [members, setMembers] = useState(memberData);
  const [commits, setCommits] = useState(0);
  const [issues, setIssues] = useState(0);
  // const [customerIssues, setCustomerIssues] = useState(0);

  // get commits for the whole team and per-member
  axios
    .get(
      "https://gitlab.com/api/v4/projects/24627227/repository/commits?page=1&per_page=1000"
    )
    .then((response: any) => {
      let numCommits: number = response.data.length;
      let membersCopy: MemberData[] = members;
      membersCopy.forEach((member: MemberData) => {
        let memberCommits = response.data.filter((commit: CommitData) =>
          member.gitlabNames.includes(commit.author_name)
        );
        member.commits = memberCommits.length;
      });
      let ourCommits: number = 0;
      membersCopy.forEach(
        (member: MemberData) => (ourCommits += member.commits)
      );
      console.assert(
        numCommits === ourCommits,
        "Each member's commits should total to the repo's total number of commits."
      );
      setMembers(membersCopy);
      setCommits(numCommits);
    });

  // get issues for the whole team and per-member
  axios
    .get("https://gitlab.com/api/v4/projects/24627227/issues?per_page=1000")
    .then((response: any) => {
      let numIssues: number = response.data.length;
      let membersCopy: MemberData[] = members;
      membersCopy.forEach((member: MemberData) => {
        let memberIssues = response.data.filter((issue: IssueData) =>
          member.gitlabNames.includes(issue.author.name)
        );
        member.issues = memberIssues.length;
      });
      let ourIssues: number = 0;
      membersCopy.forEach((member: MemberData) => (ourIssues += member.issues));
      setMembers(membersCopy);
      setIssues(numIssues);
      // setCustomerIssues(numIssues - ourIssues);
    });

  return (
    <div className={pageStyles.centerAll}>
      <div>
        <Jumbotron fluid={true} className={pageStyles.jumbotronBanner}>
          <h1 className={pageStyles.title}>About DollaDollaBills</h1>
        </Jumbotron>
        <Container className={pageStyles.core}>
          <br />
          <p className={pageStyles.pageText + " " + pageStyles.sidePadding}>
            DollaDollaBills was created to bring more awareness among the
            younger generation about who their representatives are and what
            kinds of legislation they support. Today, it's easy to learn about
            congressional activity at a high level, but very difficult to
            understand the activities of locally elected officials.
            DollaDollaBills bridges this gap by providing location-based lookup
            of representatives and by connecting legislation with supporting
            representatives.
          </p>
          <br />
        </Container>
      </div>
      <div>
        <Jumbotron fluid={true} className={pageStyles.jumbotronBanner}>
          <h1 className={pageStyles.title}>Our Team</h1>
        </Jumbotron>
        <Container className={pageStyles.core}>
          <br />
          <CardDeck className={cardStyles.cardDeck}>
            {memberData.map((data) => {
              return <MemberCard key={data.name} info={data} />;
            })}
          </CardDeck>
          <br />
          <h1 className={pageStyles.subtitle}>Cumulative Stats</h1>
          <CardDeck className={cardStyles.cardDeck}>
            <Card
              key="commits"
              className={"bg-white rounded " + cardStyles.smallCard}
            >
              <Card.Text className={"mt-2 mb-0 " + cardStyles.cardTextBig}>
                {commits}
              </Card.Text>
              <Card.Text className={"mb-2 " + cardStyles.cardTitle}>
                Total Commits
              </Card.Text>
            </Card>
            <Card
              key="issues"
              className={"bg-white rounded " + cardStyles.smallCard}
            >
              <Card.Text className={"mt-2 mb-0 " + cardStyles.cardTextBig}>
                {issues}
              </Card.Text>
              <Card.Text className={"mb-2 " + cardStyles.cardTitle}>
                Total Issues
              </Card.Text>
            </Card>
            <Card
              key="tests"
              className={"bg-white rounded " + cardStyles.smallCard}
            >
              <Card.Text className={"mt-2 mb-0 " + cardStyles.cardTextBig}>
                80
              </Card.Text>
              <Card.Text className={"mb-2 " + cardStyles.cardTitle}>
                Total Tests
              </Card.Text>
            </Card>
          </CardDeck>
          <br />
        </Container>
      </div>
      <div>
        <Jumbotron fluid={true} className={pageStyles.jumbotronBanner}>
          <h1 className={pageStyles.title}>Our Resources</h1>
        </Jumbotron>
        <Container className={pageStyles.core}>
          <br />
          <h1 className={pageStyles.subtitle}>Tools</h1>
          <CardDeck className={cardStyles.cardDeck}>
            {toolData.map((tool: ResourceData) => {
              return (
                <ResourceCard
                  key={tool.name}
                  imgUrl={tool.imgUrl}
                  name={tool.name}
                  bodyText={tool.bodyText}
                  link={tool.link}
                />
              );
            })}
          </CardDeck>
          <h1 className={pageStyles.subtitle}>APIs</h1>
          <CardDeck className={cardStyles.cardDeck}>
            {apiData.map((api: ResourceData) => {
              return (
                <ResourceCard
                  key={api.name}
                  imgUrl={api.imgUrl}
                  name={api.name}
                  bodyText={api.bodyText}
                  link={api.link}
                />
              );
            })}
          </CardDeck>
          <h1 className={pageStyles.subtitle}>
            Our Gitlab Repo and Postman API
          </h1>
          <CardDeck className={cardStyles.cardDeck}>
            <ResourceCard
              key="gitlab"
              imgUrl="./images/Logos/gitlab.png"
              name="Our GitLab Repo"
              link="https://gitlab.com/sbhave/dolladollabills"
              id="gitlab"
            />
            <ResourceCard
              key="postman"
              imgUrl="./images/Logos/postman.png"
              name="Our Postman API"
              link="https://documenter.getpostman.com/view/7971575/TzRNFqMq"
              id="postman"
            />
          </CardDeck>
          <br />
        </Container>
      </div>
      <div>
        <Jumbotron fluid={true} className={pageStyles.jumbotronBanner}>
          <h1 className={pageStyles.title}>Our Presentation</h1>
        </Jumbotron>
        <Container className={pageStyles.core}>
          <br />
          <ReactPlayer
            className="react-player ml-auto mr-auto"
            url="https://www.youtube.com/watch?v=Gd1p9PqEArM"
            playing={false}
            controls={true}
            muted={false}
            loop={false}
            width="90%"
            height="562px"
          />
          <br />
          <br />
        </Container>
      </div>
    </div>
  );
}

//------
// types
//------

interface CommitData {
  author_email: string;
  author_name: string;
  authored_date: Date;
  committed_date: Date;
  committer_email: string;
  committer_name: string;
  created_at: Date;
  id: string;
  message: string;
  parent_ids: string[];
  short_id: string;
  title: string;
  web_url: string;
}

interface IssueData {
  _links: Links;
  assignee: Assignee;
  assignees: Assignee[];
  author: Assignee;
  blocking_issues_count: number;
  closed_at: null;
  closed_by: null;
  confidential: boolean;
  created_at: Date;
  description: string;
  discussion_locked: null;
  downvotes: number;
  due_date: null;
  has_tasks: boolean;
  health_status: null;
  id: number;
  iid: number;
  labels: string[];
  merge_requests_count: number;
  milestone: null;
  moved_to_id: null;
  project_id: number;
  references: References;
  service_desk_reply_to: null;
  state: string;
  task_completion_status: TaskCompletionStatus;
  time_stats: TimeStats;
  title: string;
  updated_at: Date;
  upvotes: number;
  user_notes_count: number;
  web_url: string;
  weight: null;
}

interface Links {
  award_emoji: string;
  notes: string;
  project: string;
  self: string;
}

interface Assignee {
  avatar_url: string;
  id: number;
  name: string;
  state: string;
  username: string;
  web_url: string;
}

interface References {
  full: string;
  relative: string;
  short: string;
}

interface TaskCompletionStatus {
  completed_count: number;
  count: number;
}

interface TimeStats {
  human_time_estimate: null;
  human_total_time_spent: null;
  time_estimate: number;
  total_time_spent: number;
}

interface ResourceData {
  imgUrl: string;
  name: string;
  bodyText?: string; // nullable
  link?: string; // nullable
}
