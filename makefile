front-dev-setup:
	cd frontend/ && yarn
front-dev:
	cd frontend/ && yarn start
python-unit-tests:
	echo "Running python unit tests..."
	python3 backend/model_unit_tests.py -v
gui-tests:
	echo "Running splinter GUI tests..."
	python3 frontend/gui_tests/splash_tests.py frontend/gui_tests/chromedriver_linux
