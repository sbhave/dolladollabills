from models import db, Politician, Bill, District, State

from sqlalchemy import or_


def query_district(queries):
    q = (
        db.session.query(District)
        .join(State, State.id == District.state_id)
        .join(Politician, Politician.district_id == District.id)
    )

    # search

    if "search" in queries:
        token = queries["search"][0]
        token_conds = []
        token_conds.append(District.name.ilike(f"%{token}%"))
        token_conds.append(District.cities.ilike(f",%{token}%,"))
        token_conds.append(District.counties.ilike(f",%{token}%,"))
        token_conds.append(District.zip_codes.ilike(f",%{token}%,"))
        token_conds.append(District.representative.ilike(f"%{token}%"))
        token_conds.append(District.party_str.ilike(f"%{token}%"))
        token_conds.append(District.state_name.ilike(f"%{token}%"))
        q = q.filter(or_(*token_conds))

    # filter

    if "party" in queries:
        party_conds = []
        for party in queries["party"]:
            party_conds.append(Politician.party == party)
        q = q.filter(or_(*party_conds))

    if "age" in queries:
        age_conds = []
        for age in queries["age"]:
            age_min, age_max = tuple(map(float, age.split(",")))
            age_conds.append(District.age.between(age_min, age_max))
        q = q.filter(or_(*age_conds))

    if "population" in queries:
        population_conds = []
        for population in queries["population"]:
            population_min, population_max = tuple(map(int, population.split(",")))
            population_conds.append(
                District.population.between(population_min, population_max)
            )
        q = q.filter(or_(*population_conds))

    if "income" in queries:
        income_conds = []
        for income in queries["income"]:
            income_min, income_max = tuple(map(int, income.split(",")))
            income_conds.append(District.income.between(income_min, income_max))
        q = q.filter(or_(*income_conds))

    if "state" in queries:
        state_conds = []
        for state in queries["state"]:
            state_conds.append(State.name == state)
        q = q.filter(or_(*state_conds))

    # sort

    if "sort" not in queries:
        q = q.order_by(District.name)
    elif queries["sort"][0] == "age-inc":
        q = q.order_by(District.age)
    elif queries["sort"][0] == "age-dec":
        q = q.order_by(District.age.desc())
    elif queries["sort"][0] == "income-inc":
        q = q.order_by(District.income)
    elif queries["sort"][0] == "income-dec":
        q = q.order_by(District.income.desc())
    elif queries["sort"][0] == "pop-inc":
        q = q.order_by(District.population)
    elif queries["sort"][0] == "pop-dec":
        q = q.order_by(District.population.desc())
    elif queries["sort"][0] == "state-inc":
        q = q.order_by(State.name)
    elif queries["sort"][0] == "state-dec":
        q = q.order_by(State.name.desc())

    return q.all()
