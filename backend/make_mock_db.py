import os

os.environ["SQLALCHEMY_DATABASE_URI"] = "sqlite:///mock.db"

from app import (
    app,
    db,
    State,
    District,
    Politician,
    Bill,
    model_id,
)

state1 = State(
    id=1,
    name="State One",
    motto="State Motto One",
    abbreviation="S1",
    governor="State Governor One",
    region="State Region One",
    land_area=10,
    population=100,
    party="A",
    picture_path="/path/to/picture/state1.jpg",
    flag_path="/path/to/flag/state1.jpg",
    flower="State Flower One",
    bird="State Bird One",
    senator1_name="State One Senator One",
    senator2_name="State One Senator Two",
    num_reps=2,
)
state2 = State(
    id=2,
    name="State Two",
    motto="State Motto Two",
    abbreviation="S2",
    governor="State Governor Two",
    region="State Region Two",
    land_area=20,
    population=200,
    party="B",
    picture_path="/path/to/picture/state2.jpg",
    flag_path="/path/to/flag/state2.jpg",
    flower="State Flower Two",
    bird="State Bird Two",
    senator1_name="State Two Senator One",
    senator2_name="State Two Senator Two",
    num_reps=1,
)
db.session.add(state2)

district11 = District(
    id=11,
    state_id=1,
    name="District One S1",
    cities=",City1S1,City2S1,",
    counties=",County1S1,",
    zip_codes=",10001,10002,10003,",
    population=110,
    income=1100,
    age=11000,
    latitude=11.11,
    longitude=-11.11,
)
district21 = District(
    id=21,
    state_id=1,
    name="District Two S1",
    cities=",City2S1,",
    counties=",County2S1,",
    zip_codes=",10002,10003,10004,",
    population=210,
    income=2100,
    age=21000,
    latitude=21.21,
    longitude=-21.21,
)
district12 = District(
    id=12,
    state_id=2,
    name="District One S2",
    cities=",City1S2,",
    counties=",County1S2,County2S2,",
    zip_codes=",20001,",
    population=120,
    income=1200,
    age=12000,
    latitude=12.12,
    longitude=-12.12,
)

politician11 = Politician(
    id="polit11",
    state_id=1,
    district_id=11,
    first_name="Politician 11 First",
    last_name="Politician 11 Last",
    leadership_role="Politician 11 Role",
    twitter="Polit11Twitter",
    office="Politician 11 Office",
    party="A",
    is_senator=False,
    gender="M",
    age=11,
    image_url="/image/url/politician11.jpg",
)
politician21 = Politician(
    id="polit21",
    state_id=1,
    district_id=21,
    first_name="Politician 21 First",
    last_name="Politician 21 Last",
    leadership_role="None",
    twitter="Polit21Twitter",
    office="Politician 21 Office",
    party="B",
    is_senator=False,
    gender="M",
    age=21,
    image_url="/image/url/politician21.jpg",
)
politician12 = Politician(
    id="polit12",
    state_id=2,
    district_id=12,
    first_name="Politician 12 First",
    last_name="Politician 12 Last",
    leadership_role="None",
    twitter="Polit12Twitter",
    office="Politician 12 Office",
    party="A",
    is_senator=False,
    gender="F",
    age=12,
    image_url="/image/url/politician12.jpg",
)
politician01 = Politician(
    id="polit01",
    state_id=1,
    district_id=None,
    first_name="Politician 01 First",
    last_name="Politician 01 Last",
    leadership_role="Politician 01 Role",
    twitter="Polit01Twitter",
    office="Politician 01 Office",
    party="A",
    is_senator=True,
    gender="M",
    age=101,
    image_url="/image/url/politician01.jpg",
)
politician02 = Politician(
    id="polit02",
    state_id=2,
    district_id=None,
    first_name="Politician 02 First",
    last_name="Politician 02 Last",
    leadership_role="None",
    twitter="Polit02Twitter",
    office="Politician 02 Office",
    party="B",
    is_senator=True,
    gender="F",
    age=102,
    image_url="/image/url/politician02.jpg",
)

bill1 = Bill(
    id="bill1",
    state_id=1,
    politician_id="polit11",
    name="House Bill One",
    number="HB1",
    subject="House Bill One Subject",
    summary="House Bill One Summary",
    bill_type="HB",
    is_senate=False,
    party="A",
    year=2001,
    bill_url="/bill/url/bill1",
    image_url="/image/url/bill1.jpg",
)
bill2 = Bill(
    id="bill2",
    state_id=2,
    politician_id="polit02",
    name="Senate Bill Two",
    number="S2",
    subject="Senate Bill Two Subject",
    summary="Senate Bill Two Summary",
    bill_type="S",
    is_senate=True,
    party="B",
    year=2002,
    bill_url="/bill/url/bill2",
    image_url="/image/url/bill2.jpg",
)

db.create_all()
db.session.add(state1)
db.session.add(state2)
db.session.add(district11)
db.session.add(district21)
db.session.add(district12)
db.session.add(politician11)
db.session.add(politician21)
db.session.add(politician12)
db.session.add(politician01)
db.session.add(politician02)
db.session.add(bill1)
db.session.add(bill2)
db.session.commit()
