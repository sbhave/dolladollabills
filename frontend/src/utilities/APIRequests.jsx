import axios from "axios";

/*
 * This creates an axios instance that we can call get() on, which already has
 * the API address prepended to the address it will query, so we only have to
 * add the endpoint to the get call. If an API_URL environment variable exists,
 * it will use that address instead of our hosted API, which could help with
 * testing.
 */
const axiosClient = axios.create({
  baseURL: process.env.API_URL
    ? process.env.API_URL
    : "https://www.dolladollabills.me/api",
});

/*
 * Expects a JavaScript object whose keys are query parameters and whose values
 * are either the associated argument or an array of arguments to each use once
 * for that query parameter.
 * Returns a URLSearchParams object containing the query parameters laid out in
 * the passed-in JavaScript object.
 */
const compileRequestParams = (params) => {
  let requestParams = new URLSearchParams();
  for (const paramName in params) {
    if (Array.isArray(params[paramName])) {
      for (const paramValue of params[paramName]) {
        if (paramValue !== "") {
          requestParams.append(paramName, paramValue);
        }
      }
    } else {
      if (params[paramName] !== "") {
        requestParams.append(paramName, params[paramName]);
      }
    }
  }
  // console.log(requestParams.toString());
  return requestParams;
};

/*
 * Given an endpoint string to append to the API address and a JavaScript object
 * describing any query parameters to that endpoint, makes an API call to that
 * endpoint with the passed-in parameters.
 */
export const makeAPIRequest = async (endpoint, params) => {
  const result = await axiosClient.get(endpoint, {
    params: compileRequestParams(params),
  });
  return result.data;
};
