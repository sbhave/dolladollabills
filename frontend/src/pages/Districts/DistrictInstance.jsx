import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Container, Jumbotron, Row, Col } from "react-bootstrap";
import { DistrictMap } from "../../components/DistrictMap";
import StateCard from "../../components/StateCard";
import PoliticianCard from "../../components/PoliticianCard";
import LoadingSpinner from "../../components/LoadingSpinner";
import { makeAPIRequest } from "../../utilities/APIRequests";
import pageStyles from "../../styles/Pages.module.css";
import districtStyles from "../../styles/Districts.module.css";

function DistrictInstance() {
  const [districtInfo, setDistrictInfo] = useState(null);
  const [stateInfo, setStateInfo] = useState(null);
  const { id } = useParams();

  useEffect(() => {
    async function getDistrictInfo() {
      const requestParams = { id: id };
      const response = await makeAPIRequest("district", requestParams);
      setDistrictInfo(response);
    }
    getDistrictInfo();
  }, [id]);

  useEffect(() => {
    async function getStateInfo() {
      const requestParams = { id: districtInfo.state_id };
      const response = await makeAPIRequest("state", requestParams);
      setStateInfo(response);
    }
    if (districtInfo !== null) {
      getStateInfo();
    }
  }, [districtInfo]);

  function Grid(props) {
    const { width, items } = props;

    let rows = [];
    let i = -1;
    let cols = items.map((item) => {
      i++;
      return (
        <Col key={i} className={districtStyles.gridMember}>
          {item}
        </Col>
      );
    });

    // if needed, add empty cols to make sure the last row has width columns
    while (cols.length % width !== 0) {
      i++;
      cols = cols.concat(
        <Col key={i} className={districtStyles.gridMember}></Col>
      );
    }

    // add rows composed of width columns to the rows array, we can be
    // guaranteed that i * width + width won't go out of range of cols, since
    // we added extra empty cols above
    for (let i = 0; i < Math.ceil(items.length / width); i++) {
      rows = rows.concat(<Row>{cols.slice(i * width, i * width + width)}</Row>);
    }

    return rows;
  }

  if (districtInfo === null) {
    return (
      <div className={pageStyles.centerAll}>
        <Jumbotron fluid={true} className={pageStyles.jumbotronBanner}>
          <h1 className={"mb-0 " + pageStyles.title}>Loading...</h1>
        </Jumbotron>
        <Container className={pageStyles.core}>
          <br />
          <LoadingSpinner />
          <br />
        </Container>
      </div>
    );
  } else {
    const [districtName, stateName] = districtInfo.name.split(", ");
    const atLargeMarker = " (at Large)";
    const atLarge = districtInfo.name.includes(atLargeMarker);
    const mediumGridLength = 100;
    const bigGridLength = 150;
    const districtSize = 0.75;
    const wholeStateSize = 5;
    const cityNames = districtInfo.cities
      .split(",")
      .filter((city) => city !== "")
      .sort();
    const countyNames = districtInfo.counties
      .split(",")
      .filter((city) => city !== "")
      .sort();
    const zipCodeNumbers = districtInfo.zip_codes
      .split(",")
      .filter((city) => city !== "")
      .sort();
    return (
      <div className={pageStyles.centerAll}>
        <Jumbotron fluid={true} className={pageStyles.jumbotronBanner}>
          <h1 className={"mb-0 " + pageStyles.title}>
            {stateName}'s{" "}
            {atLarge ? "only Congressional District" : districtName}
          </h1>
        </Jumbotron>
        <Container className={pageStyles.core}>
          <br />
          <h1 className={pageStyles.subtitle}>Demographics</h1>
          <br />
          <div className={districtStyles.arrangeHorizontally}>
            {/* <img
              className={districtStyles.picture}
              src="/images/Logos/ut.png"
            /> */}
            {/* HERE */}
            <div style={{ marginBottom: "4rem" }}>
              <h3 className={"mb-2 " + pageStyles.bigText}>
                A Map of this District
              </h3>
              <DistrictMap
                latitude={districtInfo.latitude}
                longitude={districtInfo.longitude}
                bBoxSize={atLarge ? wholeStateSize : districtSize}
              />
            </div>
            <div
              className={pageStyles.pageText + " " + districtStyles.sideText}
            >
              <p className="mb-3">
                This congressional district has a population of{" "}
                {districtInfo.population.toLocaleString()} people.
              </p>
              <p className="mb-3">
                The average age of its citizens is {districtInfo.age} years old.
              </p>
              <p className={atLarge ? "mb-3" : "mb-0"}>
                The average household income in this district is $
                {districtInfo.income.toLocaleString()}.00.
              </p>
              {atLarge ? (
                <p className="mb-0">
                  Since {stateName} has a relatively low total population--at
                  only {districtInfo.population.toLocaleString()}--and states
                  get seats in the house of representatives based on their
                  population, this is the only congressional district in{" "}
                  {stateName}.
                </p>
              ) : null}
            </div>
          </div>
          <br />
          <div>
            <h1 className={pageStyles.subtitle}>
              Learn More About this District
            </h1>
            <br />
            <Row>
              <Col>
                <h3 className={"mb-2 " + pageStyles.bigText}>
                  This District's State
                </h3>
                {stateInfo !== null ? (
                  <StateCard className="ml-auto mr-auto" data={stateInfo} />
                ) : null}
              </Col>
              <Col>
                <h3 className={"mb-2 " + pageStyles.bigText}>
                  This District's House Rep
                </h3>
                <PoliticianCard
                  className="ml-auto mr-auto"
                  data={districtInfo.politician}
                  stateName={
                    stateInfo !== null ? stateInfo.name : districtInfo.state_id
                  }
                />
              </Col>
            </Row>
          </div>
          <br />
          <p className={pageStyles.pageText}>
            <b>Note:</b> Below are the cities, counties, and zip codes inside
            this district. Because political districts are divided{" "}
            <i>interestingly</i> they can contain multiple of each, which is why
            you'll likely see a lot of them.
          </p>
          <div>
            <h1 className={pageStyles.subtitle}>Cities in this District</h1>
            <br />
            <Grid
              width={
                cityNames.length >= mediumGridLength
                  ? cityNames.length >= bigGridLength
                    ? 5
                    : 4
                  : 3
              }
              items={cityNames}
            />
          </div>
          <br />
          <div>
            <h1 className={pageStyles.subtitle}>Counties in this District</h1>
            <br />
            <Grid
              width={
                countyNames.length >= mediumGridLength
                  ? countyNames.length >= bigGridLength
                    ? 5
                    : 4
                  : 3
              }
              items={countyNames}
            />
          </div>
          <br />
          <div>
            <h1 className={pageStyles.subtitle}>Zip Codes in this District</h1>
            <br />
            <Grid
              width={
                zipCodeNumbers.length >= mediumGridLength
                  ? zipCodeNumbers.length >= bigGridLength
                    ? 5
                    : 4
                  : 3
              }
              items={zipCodeNumbers}
            />
          </div>
          <br />
        </Container>
      </div>
    );
  }
}

export default DistrictInstance;
