import React from "react";
import { Dropdown, Badge } from "react-bootstrap";

class FilterControls extends React.Component {
  state = {
    selectedItems: [],
  };

  ItemIcons = React.forwardRef(({ onClick, items }, ref) => {
    return (
      <span ref={ref} onClick={onClick}>
        {items.length == 0
          ? "Nothing Selected..."
          : items.map((item) => <Badge>{item}</Badge>)}
      </span>
    );
  });

  render() {
    return (
      <Dropdown>
        <Dropdown.Toggle as={this.ItemIcons} items={this.state.selectedItems}>
          {/* {this.state.selectedItems.length == 0
            ? "Nothing Selected..."
            : this.state.selectedItems.join(" ")} */}
        </Dropdown.Toggle>
        <Dropdown.Menu>
          {this.props.listItems.map((item) => (
            <Dropdown.Item
              key={item.name}
              active={this.state.selectedItems.includes(item.name)}
              onClick={() => {
                if (this.state.selectedItems.includes(item.name)) {
                  const indexOfItem = this.state.selectedItems.indexOf(
                    item.name
                  );
                  let newSelectedItems = [];
                  for (let i = 0; i < this.state.selectedItems.length; i++) {
                    if (i != indexOfItem) {
                      newSelectedItems = [
                        ...newSelectedItems,
                        this.state.selectedItems[i],
                      ];
                    }
                  }
                  this.setState({
                    ...this.state,
                    selectedItems: newSelectedItems,
                  });
                } else {
                  const newSelectedItems = [
                    ...this.state.selectedItems,
                    item.name,
                  ];
                  this.setState({
                    ...this.state,
                    selectedItems: newSelectedItems,
                  });
                }
              }}
            >
              {item.name}
            </Dropdown.Item>
          ))}
        </Dropdown.Menu>
      </Dropdown>
    );
  }
}

export default FilterControls;
