import React from "react";
import { withRouter } from "react-router-dom";
import { Card } from "react-bootstrap";
import cardStyles from "../styles/Cards.module.css";
import Highlighter from "react-highlight-words";

class StateResult extends React.Component {
  MAX_FIELD_LENGTH = 500;

  render() {
    let {
      name,
      abbreviation,
      motto,
      governor,
      senator1_name,
      senator2_name,
      party_str,
      region,
      land_area,
      population,
      num_reps,
    } = this.props.data;
    const searchWords = this.props.query.split(" ");
    return (
      <React.Fragment>
        <Card
          className="mb-3"
          onClick={() => {
            this.props.history.push("/state/" + this.props.data.id);
          }}
        >
          <Card.Body>
            <Card.Title>
              <Highlighter
                unhighlightClassName={cardStyles.cardTitle}
                highlightClassName={cardStyles.cardTitleHighlight}
                searchWords={searchWords}
                textToHighlight={name}
              />
            </Card.Title>
            <Card.Text>
              <b>Abbreviation: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={abbreviation}
              />
            </Card.Text>
            <Card.Text>
              <b>Motto: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={motto}
              />
            </Card.Text>
            <Card.Text>
              <b>Governor: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={governor}
              />
            </Card.Text>
            <Card.Text>
              <b>Senior Senator: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={senator1_name}
              />
            </Card.Text>
            <Card.Text>
              <b>Junior Senator: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={senator2_name}
              />
            </Card.Text>
            <Card.Text>
              <b>Party: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={party_str}
              />
            </Card.Text>
            <Card.Text>
              <b>Number of House Representatives: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={String(num_reps)}
              />
            </Card.Text>
            <Card.Text>
              <b>Population: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={String(population)}
              />
            </Card.Text>
            <Card.Text>
              <b>Land Area: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={String(land_area)}
              />
            </Card.Text>
            <Card.Text>
              <b>Region: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={region}
              />
            </Card.Text>
          </Card.Body>
        </Card>
      </React.Fragment>
    );
  }
}

export default withRouter(StateResult);
