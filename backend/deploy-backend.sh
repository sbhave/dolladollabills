IMAGE_NAME=dolladollabills-backend
HOST_NAME=292247796879.dkr.ecr.us-east-2.amazonaws.com

echo "Deploying Backend..."
aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin $HOST_NAME
docker build -t $IMAGE_NAME .
docker tag $IMAGE_NAME:latest $HOST_NAME/$IMAGE_NAME
docker push $HOST_NAME/$IMAGE_NAME
cd aws_deploy
eb init
eb deploy
