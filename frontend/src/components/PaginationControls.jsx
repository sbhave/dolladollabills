import React from "react";
import { Pagination, Row, Dropdown, DropdownButton } from "react-bootstrap";
import "../styles/Pagination.css";

class PaginationControls extends React.Component {
  PageNumber = () => {
    if (
      this.props.pageNumber !== 1 &&
      this.props.pageNumber !== this.props.numPages
    ) {
      return (
        <Pagination.Item
          active
          onClick={() => this.props.onSetPage(this.props.pageNumber)}
        >
          {this.props.pageNumber}
        </Pagination.Item>
      );
    } else {
      return null;
    }
  };

  LeftOfPageNumber = () => {
    if (this.props.pageNumber - 2 > 1) {
      return (
        <React.Fragment>
          <Pagination.Ellipsis disabled={true} />
          <Pagination.Item
            onClick={() => this.props.onSetPage(this.props.pageNumber - 1)}
          >
            {this.props.pageNumber - 1}
          </Pagination.Item>
        </React.Fragment>
      );
    } else if (this.props.pageNumber - 1 > 1) {
      return (
        <Pagination.Item
          onClick={() => this.props.onSetPage(this.props.pageNumber - 1)}
        >
          {this.props.pageNumber - 1}
        </Pagination.Item>
      );
    } else {
      return null;
    }
  };

  RightOfPageNumber = () => {
    if (this.props.pageNumber + 2 < this.props.numPages) {
      return (
        <React.Fragment>
          <Pagination.Item
            onClick={() => this.props.onSetPage(this.props.pageNumber + 1)}
          >
            {this.props.pageNumber + 1}
          </Pagination.Item>
          <Pagination.Ellipsis disabled={true} />
        </React.Fragment>
      );
    } else if (this.props.pageNumber + 1 < this.props.numPages) {
      return (
        <Pagination.Item
          onClick={() => this.props.onSetPage(this.props.pageNumber + 1)}
        >
          {this.props.pageNumber + 1}
        </Pagination.Item>
      );
    } else {
      return null;
    }
  };

  PrevButton = () => {
    return (
      <Pagination.Prev
        onClick={() => this.props.onSetPage(this.props.pageNumber - 1)}
        disabled={this.props.pageNumber - 1 >= 1 ? false : true}
        id="pagination_prev"
      />
    );
  };

  NextButton = () => {
    return (
      <Pagination.Next
        onClick={() => this.props.onSetPage(this.props.pageNumber + 1)}
        disabled={
          this.props.pageNumber + 1 <= this.props.numPages ? false : true
        }
        id="pagination_next"
      />
    );
  };

  render() {
    return (
      <div>
        <Pagination>
          <Row className="ml-auto mr-auto">
            <this.PrevButton />
            <Pagination.Item
              active={this.props.pageNumber === 1}
              onClick={() => this.props.onSetPage(1)}
            >
              1
            </Pagination.Item>

            <this.LeftOfPageNumber />
            <this.PageNumber />
            <this.RightOfPageNumber />

            {/* don't render the last page number if there is only one page
                since that will result in the Pagination.Item above showing a 1
                and this Pagination.Item showing another 1 */}
            {/* don't render it if there are 0 pages either, since that will
                result in the Pagination.Item above showing a 1 and this one
                showing a 0 */}
            {this.props.numPages > 1 ? (
              <Pagination.Item
                active={this.props.pageNumber === this.props.numPages}
                onClick={() => this.props.onSetPage(this.props.numPages)}
              >
                {this.props.numPages}
              </Pagination.Item>
            ) : null}
            <this.NextButton />
          </Row>
        </Pagination>
        <DropdownButton
          drop="up"
          variant="secondary"
          title={this.props.perPage + " Per Page"}
          style={{ float: "right" }}
          id="pagination_dropup"
        >
          <Dropdown.Item
            onSelect={() => {
              this.props.onSetPerPage(10);
              this.props.onSetPage(1);
            }}
            id="pagination_10"
          >
            Show 10
          </Dropdown.Item>
          <Dropdown.Item
            onSelect={() => {
              this.props.onSetPerPage(20);
              this.props.onSetPage(1);
            }}
            id="pagination_20"
          >
            Show 20
          </Dropdown.Item>
          <Dropdown.Item
            onSelect={() => {
              this.props.onSetPerPage(30);
              this.props.onSetPage(1);
            }}
            id="pagination_30"
          >
            Show 30
          </Dropdown.Item>
        </DropdownButton>
        <br />
      </div>
    );
  }
}
export default PaginationControls;
