echo "Deploying Frontend..."
export REACT_APP_API_URL=/api
rm -f tsconfig.json
yarn
yarn build
aws s3 sync build/ s3://dolladollabills-frontend --acl public-read --exclude '*/images/*'
