import React from "react";
import { withRouter } from "react-router-dom";
import { Card } from "react-bootstrap";
import cardStyles from "../styles/Cards.module.css";
import Highlighter from "react-highlight-words";

class PoliticianResult extends React.Component {
  render() {
    let name = this.props.data.first_name + " " + this.props.data.last_name;
    const {
      leadership,
      twitter,
      office,
      party_str,
      chamber,
      gender_str,
      age,
      state_name,
    } = this.props.data;
    const searchWords = this.props.query.split(" ");
    return (
      <React.Fragment>
        <Card
          className="mb-3"
          onClick={() => {
            this.props.history.push("/politicians/" + this.props.data.id);
          }}
        >
          <Card.Body>
            <Card.Title>
              <Highlighter
                unhighlightClassName={cardStyles.cardTitle}
                highlightClassName={cardStyles.cardTitleHighlight}
                searchWords={searchWords}
                textToHighlight={name}
              />
            </Card.Title>
            <Card.Text>
              <b>Party: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={party_str}
              />
            </Card.Text>
            <Card.Text>
              <b>State: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={state_name}
              />
            </Card.Text>
            <Card.Text>
              <b>Chamber: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={chamber}
              />
            </Card.Text>
            <Card.Text>
              <b>Leadership Role: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={leadership}
              />
            </Card.Text>
            <Card.Text>
              <b>Gender: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={gender_str}
              />
            </Card.Text>
            <Card.Text>
              <b>Age: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={String(age)}
              />
            </Card.Text>
            <Card.Text>
              <b>Twitter: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={"@" + twitter}
              />
            </Card.Text>
            <Card.Text>
              <b>Office: </b>
              <Highlighter
                unhighlightClassName={cardStyles.leftCardText}
                highlightClassName={cardStyles.leftCardTextHighlight}
                searchWords={searchWords}
                textToHighlight={office}
              />
            </Card.Text>
          </Card.Body>
        </Card>
      </React.Fragment>
    );
  }
}

export default withRouter(PoliticianResult);
