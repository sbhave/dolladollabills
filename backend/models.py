from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from sqlalchemy import case
from sqlalchemy.ext.hybrid import hybrid_property

import os

app = Flask(__name__)
CORS(app)

app.config["SQLALCHEMY_DATABASE_URI"] = os.environ["SQLALCHEMY_DATABASE_URI"]
# app.config[
#     "SQLALCHEMY_DATABASE_URI"
# ] = "mysql+pymysql://admin:dolla-dolla-bills1@ddb-db.ctz7bj3mpbtg.us-east-2.rds.amazonaws.com/prod"

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)


class Politician(db.Model):
    __tablename__ = "politician"
    id = db.Column(db.String(7), primary_key=True)
    state_id = db.Column(db.Integer, db.ForeignKey("state.id"))
    district_id = db.Column(db.Integer, db.ForeignKey("district.id"), nullable=True)
    bills = db.relationship("Bill", backref="politician")

    # searchable
    first_name = db.Column(db.String(50))
    last_name = db.Column(db.String(50))
    leadership_role = db.Column(db.String(100))
    twitter = db.Column(db.String(50))
    office = db.Column(db.String(100))

    # filterable
    party = db.Column(db.String(1))
    is_senator = db.Column(db.Boolean)
    gender = db.Column(db.String(1))
    age = db.Column(db.Integer)

    # CHANGES Party Abreviation to Party Representation
    @hybrid_property
    def party_str(self):
        if self.party == "D":
            return "Democrat"
        if self.party == "I":
            return "Independent"
        if self.party == "R":
            return "Republican"
        return "Unknown"

    # CHANGES Party Abreviation to Party Representation
    @party_str.expression
    def party_str(cls):
        return case(
            [
                (cls.party == "D", "Democrat"),
                (cls.party == "I", "Independent"),
                (cls.party == "R", "Republican"),
            ],
            else_="Unknown",
        )

    # CHANGES Senator Boolean to String Representation of Politician Chamber Type
    @hybrid_property
    def chamber(self):
        return "Senator" if self.is_senator else "House Representative"

    # CHANGES Senator Boolean to String Representation of Politician Chamber Type
    @chamber.expression
    def chamber(cls):
        return case(
            [
                (cls.is_senator, "Senator"),
            ],
            else_="House Representative",
        )

    # CHANGES Gender Abreviation to Gender Representation
    @hybrid_property
    def gender_str(self):
        return "Male" if self.gender == "M" else "Female"

    # CHANGES Gender Abreviation to Gender Representation
    @gender_str.expression
    def gender_str(cls):
        return case(
            [
                (cls.gender == "M", "Male"),
            ],
            else_="Female",
        )

    # CREATES State Name Field from State ID
    @hybrid_property
    def state_name(self):
        return self.state.name

    # CREATES State Name Field from State ID
    @state_name.expression
    def state_name(cls):
        return State.name

    # media
    # picture, twitter feed
    image_url = db.Column(
        db.String(200),
        default="https://www.tenforums.com/geek/gars/images/2/types/thumb_15951118880user.png",
    )

    def __repr__(self):
        dictionary = {
            "id": self.id,
            "state_id": self.state_id,
            "district_id": self.district_id,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "leadership": self.leadership_role,
            "twitter": self.twitter,
            "office": self.office,
            "party": self.party,
            "party_str": self.party_str,
            "is_senator": self.is_senator,
            "chamber": self.chamber,
            "gender": self.gender,
            "gender_str": self.gender_str,
            "age": self.age,
            "image_url": self.image_url,
            "bills": self.bills,
            "state_name": self.state_name,
        }
        return str(dictionary)

    def __str__(self):
        return repr(self)


class Bill(db.Model):
    __tablename__ = "bill"
    id = db.Column(db.String(20), primary_key=True)
    state_id = db.Column(db.Integer, db.ForeignKey("state.id"))
    politician_id = db.Column(db.String(7), db.ForeignKey("politician.id"))

    # searchable
    name = db.Column(db.String(1000))
    number = db.Column(db.String(20))
    subject = db.Column(db.String(200))
    summary = db.Column(db.String(10000))

    # CREATES Sponsor Politcian Name Field from Politician ID
    @hybrid_property
    def sponsor(self):
        return self.politician.first_name + " " + self.politician.last_name

    # CREATES Sponsor Politcian Name Field from Politician ID
    @sponsor.expression
    def sponsor(cls):
        return Politician.first_name + " " + Politician.last_name

    # filterable
    bill_type = db.Column(db.String(20))
    is_senate = db.Column(db.Boolean)
    party = db.Column(db.String(1))
    year = db.Column(db.Integer)

    # CHANGES Bill Type Abreviation to Type Representation
    @hybrid_property
    def bill_type_str(self):
        bt = self.bill_type
        if bt == "hconres":
            return "House Concurrent Resolution"
        if bt == "hjres":
            return "House Joint Resolution"
        if bt == "hr":
            return "House Bill"
        if bt == "s":
            return "Senate Bill"
        if bt == "sconres":
            return "Senate Concurrent Resolution"
        if bt == "sjres":
            return "Senate Joint Resolution"
        return "Unknown"

    # CHANGES Bill Type Abreviation to Type Representation
    @bill_type_str.expression
    def bill_type_str(cls):
        return case(
            [
                (cls.bill_type == "hconres", "House Concurrent Resolution"),
                (cls.bill_type == "hjres", "House Joint Resolution"),
                (cls.bill_type == "hr", "House Bill"),
                (cls.bill_type == "s", "Senate Bill"),
                (cls.bill_type == "sconres", "Senate Concurrent Resolution"),
                (cls.bill_type == "sjres", "Senate Joint Resolution"),
            ],
            else_="Unknown",
        )

    # CHANGES Senate Boolean to String Representation of Origin of Bill
    @hybrid_property
    def chamber(self):
        return "Senate" if self.is_senate else "House"

    # CHANGES Senate Boolean to String Representation of Origin of Bill
    @chamber.expression
    def chamber(cls):
        return case(
            [
                (cls.is_senate, "Senate"),
            ],
            else_="House",
        )

    # CHANGES Party Abreviation to Bill Party Representation
    @hybrid_property
    def party_str(self):
        if self.party == "D":
            return "Democrat"
        if self.party == "I":
            return "Independent"
        if self.party == "R":
            return "Republican"
        return "Unknown"

    # CHANGES Party Abreviation to Bill Party Representation
    @party_str.expression
    def party_str(cls):
        return case(
            [
                (cls.party == "D", "Democrat"),
                (cls.party == "I", "Independent"),
                (cls.party == "R", "Republican"),
            ],
            else_="Unknown",
        )

    # CREATES State Name Field from State ID
    @hybrid_property
    def state_name(self):
        return self.state.name

    # CREATES State Name Field from State ID
    @state_name.expression
    def state_name(cls):
        return State.name

    # media
    # python word cloud, pic of state and/or politician
    bill_url = db.Column(db.String(200))
    image_url = db.Column(db.String(200))

    def __repr__(self):
        dictionary = {
            "id": self.id,
            "state_id": self.state_id,
            "politician_id": self.politician_id,
            "name": self.name,
            "number": self.number,
            "subject": self.subject,
            "summary": self.summary,
            "bill_type": self.bill_type,
            "bill_type_str": self.bill_type_str,
            "is_senate": self.is_senate,
            "chamber": self.chamber,
            "party": self.party,
            "party_str": self.party_str,
            "year": self.year,
            "bill_url": self.bill_url,
            "image_url": self.image_url,
            "sponsor": self.sponsor,
            "state_name": self.state_name,
        }
        return str(dictionary)

    def __str__(self):
        return repr(self)


# Create model for district
class District(db.Model):
    __tablename__ = "district"
    id = db.Column(db.Integer, primary_key=True)
    state_id = db.Column(db.Integer, db.ForeignKey("state.id"))
    politician = db.relationship("Politician", backref="district", uselist=False)

    # searchable
    name = db.Column(db.String(100))
    cities = db.Column(db.String(4000))
    counties = db.Column(db.String(1000))
    zip_codes = db.Column(db.String(2400))

    # CREATES Politcian Name Field from Politician ID
    @hybrid_property
    def representative(self):
        return self.politician.first_name + " " + self.politician.last_name

    # CREATES Politcian Name Field from Politician ID
    @representative.expression
    def representative(cls):
        return Politician.first_name + " " + Politician.last_name

    # filterable
    population = db.Column(db.Integer)
    income = db.Column(db.Integer)
    age = db.Column(db.Float)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    # party (from politician)
    # state (from state)

    # CHANGES Party Abreviation to Bill Party Representation
    @hybrid_property
    def party_str(self):
        if self.politician.party == "D":
            return "Democrat"
        if self.politician.party == "I":
            return "Independent"
        if self.politician.party == "R":
            return "Republican"
        return "Unknown"

    # CHANGES Party Abreviation to Bill Party Representation
    @party_str.expression
    def party_str(cls):
        return case(
            [
                (Politician.party == "D", "Democrat"),
                (Politician.party == "I", "Independent"),
                (Politician.party == "R", "Republican"),
            ],
            else_="Unknown",
        )

    # CREATES State Name Field from State ID
    @hybrid_property
    def state_name(self):
        return self.state.name

    # CREATES State Name Field from State ID
    @state_name.expression
    def state_name(cls):
        return State.name

    # media
    # district map, pic of flag

    def __repr__(self):
        dictionary = {
            "id": self.id,
            "state_id": self.state_id,
            "name": self.name,
            "cities": self.cities,
            "counties": self.counties,
            "zip_codes": self.zip_codes,
            "population": self.population,
            "income": self.income,
            "age": self.age,
            "latitude": self.latitude,
            "longitude": self.longitude,
            "politician": self.politician,
            "representative": self.representative,
            "party_str": self.party_str,
            "state_name": self.state.name,
        }
        return str(dictionary)

    def __str__(self):
        return repr(self)


class State(db.Model):
    __tablename__ = "state"
    id = db.Column(db.Integer, primary_key=True)
    districts = db.relationship("District", backref="state")
    politicians = db.relationship("Politician", backref="state")
    bills = db.relationship("Bill", backref="state")

    # searchable
    name = db.Column(db.String(20))
    motto = db.Column(db.String(60))
    abbreviation = db.Column(db.String(2))
    governor = db.Column(db.String(30))
    senator1_name = db.Column(db.String(100))
    senator2_name = db.Column(db.String(100))

    # filterable
    region = db.Column(db.String(20))
    land_area = db.Column(db.Integer)
    population = db.Column(db.Integer)
    party = db.Column(db.String(1))
    num_reps = db.Column(db.Integer)

    # CHANGES Party Abreviation to Bill Party Representation
    @hybrid_property
    def party_str(self):
        if self.party == "D":
            return "Democrat"
        if self.party == "I":
            return "Independent"
        if self.party == "R":
            return "Republican"
        return "Unknown"

    # CHANGES Party Abreviation to Bill Party Representation
    @party_str.expression
    def party_str(cls):
        return case(
            [
                (cls.party == "D", "Democrat"),
                (cls.party == "I", "Independent"),
                (cls.party == "R", "Republican"),
            ],
            else_="Unknown",
        )

    # media
    picture_path = db.Column(db.String(100))
    flag_path = db.Column(db.String(100))
    flower = db.Column(db.String(200))
    bird = db.Column(db.String(200))

    def __repr__(self):
        dictionary = {
            "id": self.id,
            "name": self.name,
            "motto": self.motto,
            "abbreviation": self.abbreviation,
            "governor": self.governor,
            "region": self.region,
            "land_area": self.land_area,
            "population": self.population,
            "party": self.party,
            "party_str": self.party_str,
            "picture_path": self.picture_path,
            "flag_path": self.flag_path,
            "flower": self.flower,
            "bird": self.bird,
            "senator1_name": self.senator1_name,
            "senator2_name": self.senator2_name,
            "num_reps": self.num_reps,
            "districts": self.districts,
            "politicians": self.politicians,
            "bills": self.bills,
        }
        return str(dictionary)

    def __str__(self):
        return repr(self)
