from app import db, District

import numpy as np
import pandas as pd

df = pd.read_csv(
    "data/Districts/zip_code_latlong.csv", delimiter=";", dtype=object
).set_index("Zip")

districts = db.session.query(District).all()
for district in districts:
    lats = []
    longs = []
    for zip_code in district.zip_codes[1:-1].split(","):
        if zip_code not in df.index:
            continue
        lats.append(float(df["Latitude"][zip_code]))
        longs.append(float(df["Longitude"][zip_code]))
    district.latitude = np.mean(np.array(lats))
    district.longitude = np.mean(np.array(longs))
    print(district.name, district.latitude, district.longitude)
db.session.commit()
