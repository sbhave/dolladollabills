import ReactDOM from "react-dom";
import PaginationControls from "../../components/PaginationControls";
import { act } from "react-dom/test-utils";
import { unmountComponentAtNode } from "react-dom";
import "@testing-library/jest-dom/extend-expect";
import { BrowserRouter as Router } from "react-router-dom";

it("Test Rendering for pagination", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <Router>
      <PaginationControls></PaginationControls>
    </Router>,
    div
  );
});
