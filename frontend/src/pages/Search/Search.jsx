import "../../App.css";
import React, { useState, useEffect } from "react";
import axios from "axios";
import { Container, Jumbotron, Row } from "react-bootstrap";
import PoliticianResult from "../../components/PoliticianResult";
import BillResult from "../../components/BillResult";
import DistrictResult from "../../components/DistrictResult";
import StateResult from "../../components/StateResult";
import "bootstrap/dist/css/bootstrap.min.css";
import pageStyles from "../../styles/Pages.module.css";

function Search() {
  const [politicianData, setPoliticianData] = useState([]);
  const [billData, setBillData] = useState([]);
  const [districtData, setDistrictData] = useState([]);
  const [stateData, setStateData] = useState([]);

  const params = new URLSearchParams(window.location.search);
  let queryParam = "";
  if (params.has("q")) {
    queryParam = params.get("q");
  }

  useEffect(() => {
    async function getPoliticianData() {
      const address = "https://www.dolladollabills.me/api/politicians?";
      const pageNumberParamString = "page=1";
      const perPageParamString = "&per-page=100";
      let searchString = "";
      if (queryParam) {
        searchString = "&search=" + queryParam;
      }

      axios.get(
        address + pageNumberParamString + perPageParamString + searchString
      ).then((response) => {
        setPoliticianData(response.data["data"]);
      });
    }
    getPoliticianData();
  }, [queryParam]);

  useEffect(() => {
    async function getBillData() {
      const address = "https://www.dolladollabills.me/api/bills?";
      const pageNumberParamString = "page=1";
      const perPageParamString = "&per-page=100";
      let searchString = "";
      if (queryParam) {
        searchString = "&search=" + queryParam;
      }

      axios.get(
        address + pageNumberParamString + perPageParamString + searchString
      ).then((response) => {
        setBillData(response.data["data"]);
      });
    }
    getBillData();
  }, [queryParam]);

  useEffect(() => {
    async function getDistrictData() {
      const address = "https://www.dolladollabills.me/api/districts?";
      const pageNumberParamString = "page=1";
      const perPageParamString = "&per-page=100";
      let searchString = "";
      if (queryParam) {
        searchString = "&search=" + queryParam;
      }

      axios.get(
        address + pageNumberParamString + perPageParamString + searchString
      ).then((response) => {
        setDistrictData(response.data["data"]);
      });
    }
    getDistrictData();
  }, [queryParam]);

  useEffect(() => {
    async function getStateData() {
      const address = "https://www.dolladollabills.me/api/states?";
      const pageNumberParamString = "page=1";
      const perPageParamString = "&per-page=100";
      let searchString = "";
      if (queryParam) {
        searchString = "&search=" + queryParam;
      }

      axios.get(
        address + pageNumberParamString + perPageParamString + searchString
      ).then((response) => {
        setStateData(response.data["data"]);
      });
    }
    getStateData();
  }, [queryParam]);

  return (
    <div className={pageStyles.leftJustifyText}>
      <Jumbotron fluid={true} className={pageStyles.jumbotronBanner}>
        <h1 className={pageStyles.title}>Site Search</h1>
      </Jumbotron>
      <Container className={pageStyles.core}>
        <br />
        <Row className="ml-1 mr-1">
          <h2 className={pageStyles.subtitle}>Politicians</h2>
        </Row>
        <br />
        <Row className="ml-1 mr-1">
          <Container>
            {politicianData?.map((data) => (
              <PoliticianResult key={data.id} data={data} query={queryParam} />
            ))}
          </Container>
        </Row>
        <br />
        <Row className="ml-1 mr-1">
          <h2 className={pageStyles.subtitle}>Bills</h2>
        </Row>
        <br />
        <Row className="ml-1 mr-1">
          <Container>
            {billData?.map((data) => (
              <BillResult key={data.id} data={data} query={queryParam} />
            ))}
          </Container>
        </Row>
        <br />
        <Row className="ml-1 mr-1">
          <h2 className={pageStyles.subtitle}>Districts</h2>
        </Row>
        <br />
        <Row className="ml-1 mr-1">
          <Container>
            {districtData?.map((data) => (
              <DistrictResult key={data.id} data={data} query={queryParam} />
            ))}
          </Container>
        </Row>
        <br />
        <Row className="ml-1 mr-1">
          <h2 className={pageStyles.subtitle}>States</h2>
        </Row>
        <br />
        <Row className="ml-1 mr-1">
          <Container>
            {stateData?.map((data) => (
              <StateResult key={data.id} data={data} query={queryParam} />
            ))}
          </Container>
        </Row>
        <br />
      </Container>
    </div>
  );
}

export default Search;
