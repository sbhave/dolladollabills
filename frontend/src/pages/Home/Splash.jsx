import React from "react";
import { Button, Container, CardDeck } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import splashStyles from "../../styles/Splash.module.css";
import pageStyles from "../../styles/Pages.module.css";
import ResourceCard from "../../components/ResourceCard";
import ReactPlayer from "react-player";

export function Splash() {
  const history = useHistory();
  return (
    <React.Fragment>
      <div className={splashStyles.Container}>
        <ReactPlayer
          style={{ overflowX: "hidden", overflowY: "hidden" }}
          className="react-player"
          url={"https://www.youtube.com/watch?v=llmaU4jBTqQ"}
          playing={true}
          controls={false}
          config={{
            youtube: {
              playerVars: {
                autoplay: 1,
                showinfo: 0,
                disablekb: 1,
                fs: 0,
                iv_load_policy: 3,
                loop: 1,
                modestbranding: 1,
                rel: 0,
                pip: 0,
                end: 1190,
              },
            },
          }}
          muted={true}
          loop={true}
          width="100vw"
          height="calc(100vw * .5625)"
        />

        <div className={splashStyles.Content}>
          <div className={splashStyles.SubContent}>
            <h1 className={splashStyles.title}>Dolla Dolla Bills</h1>
            <p className={"mb-2 " + splashStyles.bigText}>
              <b>
                We're a website driven to bring more awareness to US politics
                and the bills supported by our representatives.
              </b>
            </p>
            <p className={splashStyles.bigText}>
              <b>
                Have a look around, we hope you enjoy what we have to offer!
              </b>
            </p>

            <Button
              style={{
                color: "#587758",
                margin: "5%",
                // boxShadow: "5px 5px 3px rgba(46, 46, 46, 0.62)",
              }}
              onClick={() => history.push("./About")}
              className="btn btn-outline-dark"
            >
              Learn More About Us
            </Button>
          </div>
        </div>
      </div>
      <Container className={pageStyles.core + " " + pageStyles.centerAll}>
        <br />
        <h1 className={pageStyles.title}>Our Resources</h1>
        <CardDeck>
          <ResourceCard
            imgUrl="./images/Icons/billIcon.png"
            name="Bills"
            bodyText="A collection of political bills we were able to find information on"
            link="./bills"
          />
          <ResourceCard
            imgUrl="./images/Icons/politicianIcon.png"
            name="Politicians"
            bodyText="The Senators and House Representatives of 2020"
            link="./politicians"
          />
          <ResourceCard
            imgUrl="./images/Icons/districtIcon.png"
            name="Districts"
            bodyText="All 435 US congressional districts, as of the 2020 election"
            link="./districts"
          />
          <ResourceCard
            imgUrl="./images/Icons/stateIcon.png"
            name="States"
            bodyText="The 50 United States and various facts about them"
            link="./states"
          />
        </CardDeck>
      </Container>
    </React.Fragment>
  );
}
