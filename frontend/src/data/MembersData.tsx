export const memberData = [
  {
    name: "Siddhi Bhave",
    gitlabNames: ["sbhave", "Siddhi Bhave", "Siddhi"],
    commits: 0,
    issues: 0,
    tests: 10,
    job: "Full-Stack Developer",
    linkedin: "https://www.linkedin.com/in/siddhi-bhave",
    image: "./images/About/siddhi_bhave.jpeg",
    blurb:
      "Hi, I am a junior majoring in Computer Science and minoring in Entrepreneurship with a certificate in Applied Statistical Modeling.",
  },
  {
    name: "Saumyaa Krishnan",
    gitlabNames: ["saumyaa2000", "Saumyaa Krishnan", "Saumyaa"],
    commits: 0,
    issues: 0,
    tests: 20,
    job: "Frontend Developer",
    linkedin: "https://www.linkedin.com/in/saumyaa-krishnan-95109016b/",
    image: "./images/About/saumyaa-krishnan.jpeg",
    blurb:
      "Hi I'm Saumyaa, and I'm a junior majoring in Computer Science. In my free time, I enjoy watching movies, embroidering, and spending time with friends!",
  },
  {
    name: "Reagan Lasswell",
    gitlabNames: ["reagancl", "Reagan Lasswell", "Reagan"],
    commits: 0,
    issues: 0,
    tests: 64,
    job: "Backend Developer",
    linkedin: "http://www.linkedin.com/in/reagan-lasswell-5a2264161",
    image: "./images/About/reagan-lasswell.jpg",
    blurb:
      "I am a senior from Galveston, Texas. I enjoy the game of baseball, watching movies and spending time with my friends. I am very proud to be a longhorn and I am excited to graduate and enter the real world after this semester.",
  },
  {
    name: "Ian Thorne",
    gitlabNames: ["ian-thorne", "Ian Thorne", "Ian"],
    commits: 0,
    issues: 0,
    tests: 25,
    job: "Frontend Developer",
    linkedin: "https://www.linkedin.com/in/ian-thorne526/",
    image: "/images/About/ian-thorne.jpeg",
    blurb:
      "I'm Ian! I'm a senior in computer science pursuing a certificate in digital arts and media. After graduating, I'm hoping to go into game development. I'm also a big fan of Magic: The Gathering!",
  },
  {
    name: "William Wang",
    gitlabNames: ["wwang00", "William Wang", "William"],
    commits: 0,
    issues: 0,
    tests: 11,
    job: "Backend Developer",
    linkedin: "https://www.linkedin.com/in/wwang00",
    image: "/images/About/william-wang.png",
    blurb:
      "Hi, I'm William and I'm a senior studying computer science and math.",
  },
];
