import React from "react";
import { withRouter } from "react-router-dom";
import { Card } from "react-bootstrap";
import cardStyles from "../styles/Cards.module.css";
import politicianStyles from "../styles/Politicians.module.css";

class PoliticianCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hovering: false,
    };
  }

  GetCardClasses = () => {
    if (this.state.hovering === true) {
      return (
        this.props.className +
        " " +
        politicianStyles.politicianCard +
        " " +
        cardStyles.cardShadow
      );
    } else {
      return this.props.className + " " + politicianStyles.politicianCard;
    }
  };

  render() {
    return (
      <React.Fragment>
        <Card
          className={this.GetCardClasses()}
          onMouseEnter={() => this.setState({ hovering: true })}
          onMouseLeave={() => this.setState({ hovering: false })}
          onClick={() => {
            this.props.history.push("/politicians/" + this.props.data.id);
          }}
        >
          <Card.Header
            className={"text-center pt-2 pb-1 " + cardStyles.cardHeader}
          >
            <Card.Title className={"mb-0 " + cardStyles.cardTitle}>
              {this.props.data.first_name}
            </Card.Title>
            <Card.Title className={"mb-0 " + cardStyles.cardTitle}>
              {this.props.data.last_name}
            </Card.Title>
          </Card.Header>
          <div
            className={
              this.props.data.party === "D"
                ? politicianStyles.cornerBadgeDem
                : this.props.data.party === "R"
                ? politicianStyles.cornerBadgeRep
                : politicianStyles.cornerBadgeIndep
            }
          >
            {this.props.data.party}
          </div>
          <Card.Img
            variant="left"
            className={politicianStyles.politicianCardPicture}
            src={this.props.data.image_url}
          />
          <Card.Body
            className={"d-flex flex-column p-2 " + cardStyles.leftCardText}
          >
            <Card.Text className="mb-2">
              <b>
                {this.props.data.is_senator === true
                  ? "Senator"
                  : "House Representative"}
              </b>
            </Card.Text>
            <Card.Text className="mb-2">
              <b>State:</b> {this.props.data.state_name}
            </Card.Text>
            <Card.Text className="mb-2">
              <b>Age:</b> {this.props.data.age}
            </Card.Text>
            <Card.Text className="mb-2">
              <b>Gender:</b>{" "}
              {this.props.data.gender === "M" ? "Male" : "Female"}
            </Card.Text>
          </Card.Body>
        </Card>
      </React.Fragment>
    );
  }
}

export default withRouter(PoliticianCard);
