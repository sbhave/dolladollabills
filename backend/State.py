from models import db, Politician, Bill, District, State

from sqlalchemy import or_


def query_state(queries):
    q = db.session.query(State).join(Politician, Politician.state_id == State.id)

    # search

    if "search" in queries:
        token = queries["search"][0]
        token_conds = []
        token_conds.append(State.name.ilike(f"%{token}%"))
        token_conds.append(State.motto.ilike(f"%{token}%"))
        token_conds.append(State.abbreviation.ilike(f"%{token}%"))
        token_conds.append(State.governor.ilike(f"%{token}%"))
        token_conds.append(State.party_str.ilike(f"%{token}%"))
        token_conds.append(State.senator1_name.ilike(f"%{token}%"))
        token_conds.append(State.senator2_name.ilike(f"%{token}%"))
        token_conds.append(State.region.ilike(f"%{token}%"))
        q = q.filter(or_(*token_conds))

    # filter

    if "party" in queries:
        party_conds = []
        for party in queries["party"]:
            party_conds.append(State.party == party)
        q = q.filter(or_(*party_conds))

    if "region" in queries:
        region_conds = []
        for region in queries["region"]:
            region_conds.append(State.region == region)
        q = q.filter(or_(*region_conds))

    if "land_area" in queries:
        land_area_conds = []
        for land_area in queries["land_area"]:
            land_area_min, land_area_max = tuple(map(int, land_area.split(",")))
            land_area_conds.append(
                State.land_area.between(land_area_min, land_area_max)
            )
        q = q.filter(or_(*land_area_conds))

    if "population" in queries:
        population_conds = []
        for population in queries["population"]:
            population_min, population_max = tuple(map(int, population.split(",")))
            population_conds.append(
                State.population.between(population_min, population_max)
            )
        q = q.filter(or_(*population_conds))

    if "num_reps" in queries:
        num_reps_conds = []
        for num_reps in queries["num_reps"]:
            num_reps_min, num_reps_max = tuple(map(int, num_reps.split(",")))
            num_reps_conds.append(State.num_reps.between(num_reps_min, num_reps_max))
        q = q.filter(or_(*num_reps_conds))

    # sort

    if "sort" not in queries:
        q = q.order_by(State.name)
    elif queries["sort"][0] == "alpha-inc":
        q = q.order_by(State.name)
    elif queries["sort"][0] == "alpha-dec":
        q = q.order_by(State.name.desc())
    elif queries["sort"][0] == "land-inc":
        q = q.order_by(State.land_area)
    elif queries["sort"][0] == "land-dec":
        q = q.order_by(State.land_area.desc())
    elif queries["sort"][0] == "pop-inc":
        q = q.order_by(State.population)
    elif queries["sort"][0] == "pop-dec":
        q = q.order_by(State.population.desc())
    elif queries["sort"][0] == "rep-inc":
        q = q.order_by(State.num_reps)
    elif queries["sort"][0] == "rep-dec":
        q = q.order_by(State.num_reps.desc())

    return q.all()
