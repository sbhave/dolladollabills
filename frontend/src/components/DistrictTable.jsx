import React from "react";
import { withRouter } from "react-router-dom";
import { Table } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/Tables.css";

class DistrictTable extends React.Component {
  render() {
    const widths = {
      state: this.props.showState ? "12.5%" : "0%",
      name: this.props.showState ? "25%" : "27.5%",
      party: this.props.showState ? "15%" : "17.5%",
      population: this.props.showState ? "12.5%" : "15%",
      age: this.props.showState ? "12.5%" : "15%",
      income: this.props.showState ? "22.5%" : "25%",
    };

    return (
      <React.Fragment>
        <Table striped="true" bordered="true" hover="true">
          <thead>
            <tr>
              {this.props.showState ? (
                <th style={{ width: widths.state }}>State</th>
              ) : null}
              <th style={{ width: widths.name }}>Name</th>
              <th style={{ width: widths.party }}>Party</th>
              <th style={{ width: widths.population }}>Population</th>
              <th style={{ width: widths.age }}>Average Age</th>
              <th style={{ width: widths.income }}>Median Household Income</th>
            </tr>
          </thead>
          <tbody>
            {this.props.data.map((district) => {
              const [districtName, stateName] = district.name.split(", ");
              return (
                <tr
                  key={district.id}
                  onClick={() => {
                    this.props.history.push("/district/" + district.id);
                  }}
                >
                  {this.props.showState ? <td>{stateName}</td> : null}
                  <td>{districtName}</td>
                  <td>
                    {district.politician.party === "D"
                      ? "Democrat"
                      : district.politician.party === "R"
                      ? "Republican"
                      : "Independent"}
                  </td>
                  <td>{district.population.toLocaleString()}</td>
                  <td>{district.age} years</td>
                  <td>${district.income.toLocaleString()}</td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </React.Fragment>
    );
  }
}

// exporting with the withRouter higher-order component so it can use history to
// route the user to the correct district page according to the table row they
// click
export default withRouter(DistrictTable);
