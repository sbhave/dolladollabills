import React, { useState, useEffect } from "react";
import Select from "react-select";
import axios from "axios";
import {
  Container,
  Jumbotron,
  Row,
  FormControl,
  InputGroup,
  Button,
} from "react-bootstrap";
import BillResult from "../../components/BillResult";
import {
  BillFilteringOptions,
  CustomSelectStyles,
} from "../../data/sortingFilteringOptions";
import pageStyles from "../../styles/Pages.module.css";

let emptyFilterParams = {};
BillFilteringOptions.forEach((optionSet) => {
  emptyFilterParams[optionSet.param] = [];
});

const BillSearch = () => {
  const [filterParams, setFilterParams] = useState(emptyFilterParams);
  const [sortParams, setSortParams] = useState("");
  const [billData, setBillData] = useState([]);

  const params = new URLSearchParams(window.location.search);
  let queryParam = "";
  if (params.has("q")) {
    queryParam = params.get("q");
  }
  const query = React.useRef();
  function searchOnClick() {
    window.location.assign("/billSearch?q=" + query.current.value);
  }

  const options = [
    { value: "senate-inc", label: "Senate" },
    { value: "house", label: "House" },
    { value: "year-inc", label: "Year (increasing)" },
    { value: "year-dec", label: "Year (decreasing)" },
    { value: "", label: "Default" },
  ];

  useEffect(() => {
    async function getBillData() {
      const address = "https://www.dolladollabills.me/api/bills?";
      const pageNumberParamString = "page=1";
      const perPageParamString = "&per-page=50";
      let searchString = "";
      let filterParamsString = "";
      const sortParamsString = "&sort=" + sortParams;
      for (const paramName in filterParams) {
        for (const paramValue of filterParams[paramName]) {
          filterParamsString += "&" + paramName + "=" + paramValue;
        }
      }
      if (queryParam) {
        searchString = "&search=" + queryParam;
      }

      axios
        .get(
          address +
            pageNumberParamString +
            perPageParamString +
            searchString +
            filterParamsString +
            sortParamsString
        )
        .then((response) => {
          setBillData(response.data["data"]);
        });
    }
    getBillData();
  }, [filterParams, sortParams, queryParam]);

  return (
    <div className={pageStyles.leftJustifyText}>
      <Jumbotron fluid={true} className={pageStyles.jumbotronBanner}>
        <h1 className={pageStyles.title}>Bill Search</h1>
      </Jumbotron>
      <Container className={pageStyles.core}>
        <br />
        <Row className="ml-1 mr-1">
          <div style={{ width: "30%" }}>
            <h1 className={pageStyles.mediumText}>Sort By</h1>
            <Select
              styles={CustomSelectStyles}
              options={options}
              onChange={(selectedOptions) => {
                setSortParams(selectedOptions.value);
              }}
            ></Select>
          </div>
          <div style={{ width: "30%" }} className="ml-auto">
            <h1 className={pageStyles.mediumText}>Search</h1>
            <InputGroup className="ml-auto">
              <FormControl
                type="text"
                placeholder="Search..."
                className="ml-auto"
                ref={query}
                onKeyPress={(event) => {
                  if (event.key === "Enter") {
                    event.preventDefault();
                    searchOnClick();
                  }
                }}
              />
              <InputGroup.Append>
                <Button
                  variant="outline-success"
                  onClick={() => searchOnClick()}
                >
                  Go
                </Button>
              </InputGroup.Append>
            </InputGroup>
          </div>
        </Row>
        <br />
        <Row className="ml-1 mr-1">
          {BillFilteringOptions.map((optionSet) => (
            <div style={{ width: "20%" }}>
              <h1 className={pageStyles.mediumText}>{optionSet.name}</h1>
              <Select
                styles={CustomSelectStyles}
                options={optionSet.options}
                isMulti={true}
                onChange={(selectedOptions) => {
                  let newFilterParams = { ...filterParams };
                  newFilterParams[optionSet.param] = selectedOptions.map(
                    (option) => option.value
                  );
                  setFilterParams(newFilterParams);
                }}
                key={optionSet.name}
                id={optionSet.name}
              />
            </div>
          ))}
        </Row>
        <br />
        <Row className="ml-1 mr-1">
          <Container>
            {billData?.map((data) => {
              return (
                <BillResult key={data.id} data={data} query={queryParam} />
              );
            })}
          </Container>
        </Row>
        <br />
      </Container>
    </div>
  );
};

export default BillSearch;
